## 2019-09-08 updated by Aix

## DEMO
[link](http://aix.asuscomm.com)

## Initiate your DB & Config
```bash
cd [your_project]
source venv/bin/activate
cd src
flask init db
flask init config
```
## Set your development environment
```bash
cd [your_project]
source venv/bin/activate
cd src
FLASK_ENV=development flask run

## if you want to use gunicorn
flask gunicorn run dev

## prod
flask gunicorn run prod

## clean gunicorn realated files
flask gunicorn clean

## FLASK_ENV=development flask run -h XX.XX.XX.XX -p XXXX
## open a new terminal
flask ng build dev
## If you have finished your development stage, you have to compile your Angular project to the production edition.
flask ng build prod
```