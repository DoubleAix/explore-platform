import os
import pytest

from app.main import create_app, db
from app.main.utils.init_data import init_config


@pytest.fixture
def app_instance(tmp_path):
    
    app = create_app()
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(tmp_path, 'web_config.db')
    with app.app_context():
        db.create_all()
        init_config(app)

    return app


def test_example(app_instance):
    
    client = app_instance.test_client()
    response = client.get('api/config/menu_list')
    assert response.status_code == 200
    
    