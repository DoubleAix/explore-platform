import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { take, tap, shareReplay, filter, switchMap, share } from 'rxjs/operators';
import { ParentMenu } from '../../models/menu.model';
import { SettingsResourcePolicyMapping, SettingResourcePolicy } from '../../models/resource-policy-mapping.model';
import { Permission } from '../../models/permission.model';
import { Group, GroupPolicy } from '../../models/group.model';
import { Space, SpacePolicy } from '../../models/space.model';
import { ChildResource, ParentResource, ResourcePolicy } from '../../models/resource.model';
import { SettingsResourceOption } from '../../enums/settings-resource-option.enum';

import { SpaceService } from 'src/app/services/space/space.service';

// for test
import { menus } from 'src/app/tests/tests.menus';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  baseUrl = '/api/config/';

  private spaceMapping = new BehaviorSubject<Space[]>(null);
  readonly spaceMapping$ = this.spaceMapping.asObservable();
  private settingResourcePolicyMapping = new BehaviorSubject<SettingsResourcePolicyMapping[]>(null);
  readonly settingResourcePolicyMapping$ = this.settingResourcePolicyMapping.asObservable();
  private permissionMapping = new BehaviorSubject<object>(null);
  readonly permissionMapping$ = this.permissionMapping.asObservable();

  private dashboardTitle = new BehaviorSubject<string>('');
  readonly dashboardTitle$ = this.dashboardTitle.asObservable();
  private dashboardMenu = new BehaviorSubject<ParentMenu[]>(null);
  readonly dashboardMenu$ = this.dashboardMenu.asObservable();
  private dashboardResourcePolicyMapping = new BehaviorSubject<Map<number, number | null>>(null);
  readonly dashboardResourcePolicyMapping$ = this.dashboardResourcePolicyMapping.asObservable();

  constructor(
    private http: HttpClient,
    private spaceService: SpaceService,
  ) { }

  setDashboardTitle(dashboardTitle: string) {
    this.dashboardTitle.next(dashboardTitle);
  }

  initSettingsResourcePolicyMapping() {
    this.getSettingsResourcePolicyMapping().toPromise().then(data => this.settingResourcePolicyMapping.next(data));
  }
  initPermissionMapping() {
    this.getPermissions().toPromise().then(data => {
      const permissionMapping = {};
      data.forEach(item => permissionMapping[item.name] = item.id);
      this.permissionMapping.next(permissionMapping);
    });
  }

  getSpaceMapping(): Observable<Space[]> {
    return this.spaceService.getSpaces().pipe(
      take(1),
      tap(data => this.spaceMapping.next(data)),
      shareReplay(),
    );
  }

  getSettingsResourcePolicyMapping(): Observable<SettingsResourcePolicyMapping[]> {
    return this.http.get<SettingsResourcePolicyMapping[]>(this.baseUrl + 'settings_resource_policy_mapping');
  }
  // send data to BehaviorSubject dashboardMenu for dashboard menu display use
  // send & modify data to BehaviorSubject dashboardResourcePolicyMapping for route canActivate use  
  getMenu(spaceId: string): Observable<ParentMenu[]> {
    return this.http.get<ParentMenu[]>(this.baseUrl + 'menu_list/' + spaceId).pipe(
      take(1),
      tap(data => this.dashboardMenu.next(menus.concat(data))),
      tap(data => {
        let mapping: Map<number, number | null> = new Map();
        data.forEach(item => {
          item.sub.forEach(item => mapping.set(item.resource_id, item.authorized_to))
        });
        this.dashboardResourcePolicyMapping.next(mapping);
      }),
      shareReplay(),
    );
  }

  getAuthorizationOption(authorizationTypeId: number): Observable<Permission[]> {
    return this.http.get<Permission[]>(this.baseUrl + 'authorization_option_list/' + authorizationTypeId);
  }

  getPermissions(): Observable<Permission[]> {
    return this.http.get<Permission[]>(this.baseUrl + 'permission_list');
  }


  // settings resource policy
  getSettingsResourcePolicyId(resourceName: SettingsResourceOption, permissionName: string): number {
    let resourceObject: SettingsResourcePolicyMapping;
    let resourcePolicyObject: SettingResourcePolicy;

    this.settingResourcePolicyMapping$.subscribe(
      (data: SettingsResourcePolicyMapping[]) => {
        resourceObject = data.filter(obj => { return obj.name == resourceName })[0];
        resourcePolicyObject = resourceObject.resource_policies.filter(obj => { return obj.permission_id == this.permissionMapping.value[permissionName] })[0];
      }
    )
    // console.log(resourceName)
    // console.log(permissionName)
    // console.log(resourcePolicyObject);
    return resourcePolicyObject.resource_policies_id
  }

  // resource policy
  getResourcePolicyId(resource: ChildResource | ParentResource, permissionName: string): number {
    let resourcePolicy: ResourcePolicy = resource.resource_policies.filter(obj => { return obj.permission_id == this.permissionMapping.value[permissionName] })[0];
    return resourcePolicy.id
  }
  getAuthorizableResourcePolicyIds(resource: ChildResource | ParentResource): number[] {
    return resource.resource_policies.map(item => item.id);
  }
  // resource policy for canActivate use
  getResourcePolicyIdbyResourceId(spaceId: string, resourceId: number): Observable<number> {
    return this.dashboardResourcePolicyMapping$.pipe(
      tap(data => {
        if (!data) {
          this.getMenu(spaceId).pipe(take(1)).subscribe();
        }
      }),
      filter(data => data != null),
      take(1),
      switchMap(
        (data: Map<number, number | null>) => {
          return of(data.get(resourceId))
        }
      ),

    )
  }

  // group policy
  getGroupPolicyId(group: Group, permissionName: string): number {
    let groupPolicy: GroupPolicy = group.group_policies.filter(obj => { return obj.permission_id == this.permissionMapping.value[permissionName] })[0];
    // console.log(group)
    // console.log(permissionName)
    return groupPolicy.id
  }

  getAuthorizableGroupPolicyIds(group: Group): number[] {
    return group.group_policies.map(item => item.id);
  }

  // space policy
  getSpacePolicyId(space: Space, permissionName: string): number {
    let spacePolicy: SpacePolicy = space.space_policies.filter(obj => { return obj.permission_id == this.permissionMapping.value[permissionName] })[0];
    return spacePolicy.id
  }
  getAuthorizableSpacePolicyIds(space: Space): number[] {
    return space.space_policies.map(item => item.id);
  }
  // for CanActivate use (space guard)
  getSpacePolicyIdBySpaceId(spaceId: string, permissionName: string): Observable<number> {
    return this.spaceMapping$.pipe(
      tap(data => {
        if (!data) {
          this.getSpaceMapping().pipe(take(1)).subscribe();
        }
      }),
      filter(data => data != null),
      take(1),
      switchMap(
        (data: Space[]) => {
          let spaceObject: Space = data.filter(obj => { return obj.space_id == spaceId })[0];
          let spacePolicy: SpacePolicy = spaceObject.space_policies.filter(obj => { return obj.permission_id == this.permissionMapping.value[permissionName] })[0];
          return of(spacePolicy.id)
        }
      )
    )
  }

}