import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Member } from '../../models/member.model';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl: string = '/api/user/';


  constructor(private http: HttpClient) {

  }
  getUsers(): Observable<Member[]> {
    return this.http.get<Member[]>(this.baseUrl);
  }

  getUsersFiltered(searchString: string): Observable<Member[]> {
    const params = new HttpParams().set('search_string', searchString);

    return this.http.get<Member[]>(this.baseUrl + 'filtered_list', { params: params });
  }

  getUserById(id: number): Observable<Member> {
    return this.http.get<Member>(this.baseUrl + id);
  }


  createUser(user: Member): Observable<Member> {
    return this.http.post<Member>(this.baseUrl, user);
  }
  updateUser(user: Member, id: number): Observable<Member> {
    return this.http.put<Member>(this.baseUrl + id, user);
  } 
  deleteUser(id: number): Observable<Member> {
    return this.http.delete<Member>(this.baseUrl + id);
  }
}
