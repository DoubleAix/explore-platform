import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Member } from '../../models/member.model';

@Injectable({
  providedIn: 'root'
})
export class GroupPolicyService {
  baseUrl: string = '/api/group_policy/';

  constructor(private http: HttpClient) { }

  getMembers(id: number, permission: string): Observable<Member[]> {
    return this.http.get<Member[]>(this.baseUrl + id + '/' + permission + '/');
  }

  addMembers(id: number, permission: string, members: Member[]): Observable<Member[]> {
    return this.http.post<Member[]>(this.baseUrl + id + '/' + permission + '/', members);
  }

  deleteMember(id: number, permission: string, member_id: number): Observable<Member> {
    return this.http.delete<Member>(this.baseUrl + id + '/' + permission + '/' + member_id);
  }
}
