import { TestBed } from '@angular/core/testing';

import { GroupPolicyService } from './group-policy.service';

describe('GroupPolicyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupPolicyService = TestBed.get(GroupPolicyService);
    expect(service).toBeTruthy();
  });
});
