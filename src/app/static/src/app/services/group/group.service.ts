import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../../models/group.model';


@Injectable({
  providedIn: 'root'
})
export class GroupService {
  baseUrl: string = '/api/group/';



  constructor(private http: HttpClient) { }

  getGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.baseUrl);
  }

  getGroupById(id: number): Observable<Group> {
    return this.http.get<Group>(this.baseUrl + id);
  }

  getGroupsFiltered(searchString: string): Observable<Group[]> {
    const params = new HttpParams().set('search_string', searchString);

    return this.http.get<Group[]>(this.baseUrl + 'filtered_list', { params: params });
  }

  createGroup(group: Group): Observable<Group> {
    return this.http.post<Group>(this.baseUrl, group);
  }

  deleteGroup(id: number): Observable<Group> {
    return this.http.delete<Group>(this.baseUrl + id);
  }
 
}
