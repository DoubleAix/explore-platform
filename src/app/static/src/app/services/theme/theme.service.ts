import { Injectable } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private themeClass = new BehaviorSubject<string>('basic-theme');
  readonly themeClass$ = this.themeClass.asObservable();


  constructor(
    private overlayContainer: OverlayContainer,
  ) {
    overlayContainer.getContainerElement().classList.add(this.themeClass.value);
  }

  toggleTheme(themeClass: string): void {
    this.overlayContainer.getContainerElement().classList.remove(this.themeClass.value);
    this.themeClass.next(themeClass);
    this.overlayContainer.getContainerElement().classList.add(this.themeClass.value);
  }
}
