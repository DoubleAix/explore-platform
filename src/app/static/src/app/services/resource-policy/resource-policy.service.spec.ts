import { TestBed } from '@angular/core/testing';

import { ResourcePolicyService } from './resource-policy.service';

describe('ResourcePolicyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResourcePolicyService = TestBed.get(ResourcePolicyService);
    expect(service).toBeTruthy();
  });
});
