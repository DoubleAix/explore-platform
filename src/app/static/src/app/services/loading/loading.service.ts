import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor() { }

  private loading = new BehaviorSubject<boolean>(false);
  readonly loading$ = this.loading.asObservable();

  activate() {
    this.loading.next(true);
  }
  deactivate() {
    this.loading.next(false);
  }
}
