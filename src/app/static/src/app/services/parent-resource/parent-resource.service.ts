import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ParentResource } from '../../models/resource.model';
import { Member } from '../../models/member.model';
import { Group } from '../../models/group.model';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ParentResourceService {
  baseUrl: string = '/api/parent_resource/';

  constructor(private http: HttpClient) { }

  getParentResources(): Observable<ParentResource[]> {
    return this.http.get<ParentResource[]>(this.baseUrl);
  }

  getParentResourceById(id: number): Observable<ParentResource> {
    return this.http.get<ParentResource>(this.baseUrl + id);
  }

  createParentResource(parentResource: ParentResource): Observable<ParentResource> {
    return this.http.post<ParentResource>(this.baseUrl, parentResource);
  }
  updateParentResource(parentResource: ParentResource, id: number): Observable<ParentResource> {
    return this.http.put<ParentResource>(this.baseUrl + id, parentResource);
  }

  deleteParentResource(id: number): Observable<ParentResource> {
    return this.http.delete<ParentResource>(this.baseUrl + id);
  }

}
