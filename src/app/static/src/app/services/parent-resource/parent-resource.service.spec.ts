import { TestBed } from '@angular/core/testing';

import { ParentResourceService } from './parent-resource.service';

describe('ParentResourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParentResourceService = TestBed.get(ParentResourceService);
    expect(service).toBeTruthy();
  });
});
