import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap, shareReplay, catchError, take } from 'rxjs/operators';
import { BehaviorSubject, Observable, ReplaySubject, throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as moment from 'moment';
import { HttpHeaders } from '@angular/common/http';
import { LoginResult } from '../../models/login-result.model';
import { LoginInfo } from 'src/app/models/login-info.model';
import { RefreshInfo } from 'src/app/models/refresh-info.model';
import { Member } from "src/app/models/member.model";

// import { LoadingService } from 'src/app/services/loading/loading.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = '/api/auth/';
  jwthelper = new JwtHelperService();

  private userInfo = new BehaviorSubject<Member>(null);
  readonly userInfo$ = this.userInfo.asObservable();

  private resourcePolicies = new BehaviorSubject<number[]>(null);
  readonly resourcePolicies$ = this.resourcePolicies.asObservable();

  private groupPolicies = new BehaviorSubject<number[]>(null);
  readonly groupPolicies$ = this.groupPolicies.asObservable();

  private spacePolicies = new BehaviorSubject<number[]>(null);
  readonly spacePolicies$ = this.spacePolicies.asObservable();

  private authorizableResourcePolicies = new BehaviorSubject<number[]>(null);
  readonly authorizableResourcePolicies$ = this.authorizableResourcePolicies.asObservable();

  private authorizableGroupPolicies = new BehaviorSubject<number[]>(null);
  readonly authorizableGroupPolicies$ = this.authorizableGroupPolicies.asObservable();

  private authorizableSpacePolicies = new BehaviorSubject<number[]>(null);
  readonly authorizableSpacePolicies$ = this.authorizableSpacePolicies.asObservable();

  constructor(
    private router: Router,
    private http: HttpClient, 
    
  ) { }

  login(loginInfo: LoginInfo): Observable<LoginResult> {
    return this.http.post<LoginResult>(this.baseUrl + 'login', loginInfo)
      .pipe(
        tap(data => {
          this.setSession(data);

        }),
        shareReplay()
      );
  }

  logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    // localStorage.removeItem('expires_at');
    // localStorage.removeItem('sub');
  }

  partialRefresh(userPlocies: string[]): Observable<LoginResult> {
    const accessToken = localStorage.getItem('access_token');
    const refreshInfo: RefreshInfo = {
      user_policy_type: userPlocies,
      access_token: accessToken,
    };
    return this.refresh(refreshInfo);
  }

  refresh(refreshInfo: RefreshInfo): Observable<LoginResult> {
    const refreshToken = localStorage.getItem('refresh_token');
    const httpOptions = { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + refreshToken }) };
    return this.http.post<LoginResult>(this.baseUrl + 'refresh', refreshInfo, httpOptions)
      .pipe(
        tap(data => this.setSession(data)),
        shareReplay(),
      );
  }

  private setSession(loginResult: LoginResult): void {

    localStorage.setItem('access_token', loginResult.access_token);
    // const access_token_object = this.jwthelper.decodeToken(loginResult.access_token);
    // localStorage.setItem('expires_at', JSON.stringify(access_token_object.exp));
    // localStorage.setItem('sub', access_token_object.sub);
    if (loginResult.refresh_token) {
      localStorage.setItem('refresh_token', loginResult.refresh_token);
    }

    const userClaims = this.jwthelper.decodeToken(loginResult.access_token).user_claims;
    // console.log(userClaims.space_policies);
    // console.log(userClaims.authorizable_space_policies);
    if (userClaims && userClaims.user_info) {
      this.userInfo.next(userClaims.user_info);
    }
    if (userClaims && userClaims.resource_policies) {
      this.resourcePolicies.next(userClaims.resource_policies);
    }
    if (userClaims && userClaims.group_policies) {
      this.groupPolicies.next(userClaims.group_policies);
    }
    if (userClaims && userClaims.space_policies) {
      this.spacePolicies.next(userClaims.space_policies);
    }
    if (userClaims && userClaims.authorizable_resource_policies) {
      this.authorizableResourcePolicies.next(userClaims.authorizable_resource_policies);
    }
    if (userClaims && userClaims.authorizable_group_policies) {
      this.authorizableGroupPolicies.next(userClaims.authorizable_group_policies);
    }
    if (userClaims && userClaims.authorizable_space_policies) {
      this.authorizableSpacePolicies.next(userClaims.authorizable_space_policies);
    }

  }

  isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  private getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment.unix(expiresAt);
  }

}