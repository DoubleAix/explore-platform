import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Space } from '../../models/space.model';

@Injectable({
  providedIn: 'root'
})
export class SpaceService {
  baseUrl = '/api/space/';


  constructor(private http: HttpClient) { }

  getSpaces(): Observable<Space[]> {
    return this.http.get<Space[]>(this.baseUrl);
  }

  getSpaceById(id: number): Observable<Space> {
    return this.http.get<Space>(this.baseUrl + id);
  }
  getSpaceBySpaceId(space_id: string): Observable<Space> {
    return this.http.get<Space>(this.baseUrl + 'space_by_space_id/' + space_id);
  }
  getSpacesFiltered(searchString: string): Observable<Space[]> {
    const params = new HttpParams().set('search_string', searchString);

    return this.http.get<Space[]>(this.baseUrl + 'filtered_list', { params: params });
  }

  createSpace(space: Space): Observable<Space> {
    return this.http.post<Space>(this.baseUrl, space);
  }

  updateSpace(space: Space, id: number): Observable<Space> {
    return this.http.put<Space>(this.baseUrl + id, space);
  }

  deleteSpace(id: number): Observable<Space> {
    return this.http.delete<Space>(this.baseUrl + id);
  }
}
