import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { GeneralSnackbarComponent } from '../../utils/snackbars/general-snackbar/general-snackbar.component'
import { DashboardModule } from '../../dashboard/dashboard.module'
@Injectable({
  providedIn: DashboardModule
})
export class MessageService {

  constructor(
    private snackBar: MatSnackBar,
  ) { }

  openInfoSnackBar(message): void {
    this.snackBar.openFromComponent(GeneralSnackbarComponent,
      {
        panelClass: ['snackbar-info'],
        duration: 3000,
        horizontalPosition: 'right',
        data: {
          message: message,
        }
      });
  }
  openErrorSnackBar(message): void {
    this.snackBar.openFromComponent(GeneralSnackbarComponent,
      {
        panelClass: ['snackbar-error'],
        duration: 5000,
        horizontalPosition: 'right',
        data: {
          message: message,
        }
      });
  }
}
