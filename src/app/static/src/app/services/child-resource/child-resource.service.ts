import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChildResource, ParentResource } from '../../models/resource.model';
import { Member } from '../../models/member.model'
import { Group } from '../../models/group.model'



@Injectable({
  providedIn: 'root'
})
export class ChildResourceService {

  baseUrl: string = '/api/child_resource/';

  constructor(private http: HttpClient) { }

  getChildResources(parentResourceId: number): Observable<ChildResource[]> {
    const params = new HttpParams().set('parent_resource_id', parentResourceId.toString());
    return this.http.get<ChildResource[]>(this.baseUrl, { params: params });
  }

  getChildResourceById(id: number): Observable<ChildResource> {
    return this.http.get<ChildResource>(this.baseUrl + id);
  }

  createChildResource(childResource: ChildResource, parentResourceId: number): Observable<ChildResource> {
    const params = new HttpParams().set('parent_resource_id', parentResourceId.toString());
    return this.http.post<ChildResource>(this.baseUrl, childResource, { params: params });
  }

  updateChildResource(childResource: ChildResource, id: number): Observable<ChildResource> {
    return this.http.put<ChildResource>(this.baseUrl + id, childResource);
  }

  deleteChildResource(id: number): Observable<ChildResource> {
    return this.http.delete<ChildResource>(this.baseUrl + id);
  }
}
