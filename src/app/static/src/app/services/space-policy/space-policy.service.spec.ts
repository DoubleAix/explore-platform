import { TestBed } from '@angular/core/testing';

import { SpacePolicyService } from './space-policy.service';

describe('SpacePolicyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpacePolicyService = TestBed.get(SpacePolicyService);
    expect(service).toBeTruthy();
  });
});
