import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ParentResource } from '../../models/resource.model';
import { Member } from '../../models/member.model'
import { Group } from '../../models/group.model'

@Injectable({
  providedIn: 'root'
})
export class SpacePolicyService {
  baseUrl: string = '/api/space_policy/';

  constructor(private http: HttpClient) { }

  getMembers(id: number, permission: string, userType: string): Observable<Member[] | Group[]> {
    return this.http.get<Member[] | Group[]>(this.baseUrl + id + '/' + userType + '/' + permission + '/');
  }

  addMembers(id: number, permission: string, members: Member[] | Group[], userType: string): Observable<Member[] | Group[]> {
    return this.http.post<Member[] | Group[]>(this.baseUrl + id + '/' + userType + '/' + permission + '/', members);
  }

  deleteMember(id: number, permission: string, member_id: number, userType: string): Observable<Member | Group> {
    return this.http.delete<Member | Group>(this.baseUrl + id + '/' + userType + '/' + permission + '/' + member_id);
  }
}
