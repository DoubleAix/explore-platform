import { Injectable } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { take, filter, switchMap, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ConfigService } from '../config/config.service';
import { Group } from '../../models/group.model';
import { SettingsResourceOption } from '../../enums/settings-resource-option.enum';
import { ChildResource, ParentResource } from '../../models/resource.model';
import { Space } from 'src/app/models/space.model';
@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(
    private authService: AuthService,
    private configService: ConfigService,
  ) { }

  validatePolicyById(userPolicies$: Observable<number[]>, policyId: number): Observable<boolean> {
    return userPolicies$.pipe(
      filter(data => data != null),
      take(1),
      switchMap(
        data => {
          // the policyId of the test case equals to -1
          if (policyId === -1) {
            return of(true);
          }
          if (data && data.includes(policyId)) {
            return of(true);
          } else {
            return of(false);
          }
        }
      )
    );
  }
  validatePolicyByIds(userPolicies$: Observable<number[]>, policyIds: number[]): Observable<boolean> {
    return userPolicies$.pipe(
      filter(data => data != null),
      take(1),
      switchMap(
        data => {
          // the policyId of the test case equals to -1
          if (policyIds.includes(-1)) {
            return of(true);
          }
          if (data && data.some(userPolicy => policyIds.includes(userPolicy))) {
            return of(true);
          } else {
            return of(false);
          }
        }
      )
    );
  }

  // menu
  validateResourcePolicyById(resourcePolicyId: number): Observable<boolean> {
    return this.validatePolicyById(this.authService.resourcePolicies$, resourcePolicyId);
  }

  validateResourcePolicyByIds(resourcePolicyIds: number[]): Observable<boolean> {
    return this.validatePolicyByIds(this.authService.resourcePolicies$, resourcePolicyIds);
  }

  // resource management
  validateResourcePolicy(resource: ChildResource | ParentResource, permissionName: string): Observable<boolean> {
    const resourcePolicyId = this.configService.getResourcePolicyId(resource, permissionName);
    return this.validatePolicyById(this.authService.resourcePolicies$, resourcePolicyId);
  }

  // settings
  validateSettingsResourcePolicy(resourceName: SettingsResourceOption, permissionName: string): Observable<boolean> {
    const resourcePolicyId = this.configService.getSettingsResourcePolicyId(resourceName, permissionName);
    return this.validatePolicyById(this.authService.resourcePolicies$, resourcePolicyId);
  }

  // resource policy for canActivate use
  validateResourcePolicyByResourceId(spaceId: string, resourceId: number): Observable<boolean> {
    return this.configService.getResourcePolicyIdbyResourceId(spaceId, resourceId).pipe(
      switchMap(data => {
        return this.validatePolicyById(this.authService.resourcePolicies$, data)
      })
    )
  }

  // authorizable resource policy permission
  validateAuthorizableResourcePolicy(resource: ChildResource | ParentResource, permissionName: string): Observable<boolean> {
    const resourcePolicyId = this.configService.getResourcePolicyId(resource, permissionName);
    return this.validatePolicyById(this.authService.authorizableResourcePolicies$, resourcePolicyId);
  }

  validateAuthoriableResourcePolicyAny(resource: ChildResource | ParentResource): Observable<boolean> {
    const resourcePolicyIds = this.configService.getAuthorizableResourcePolicyIds(resource);
    return this.validatePolicyByIds(this.authService.authorizableResourcePolicies$, resourcePolicyIds);
  }
  // group management
  validateGroupPolicy(group: Group, permissionName: string): Observable<boolean> {
    const groupPolicyId = this.configService.getGroupPolicyId(group, permissionName);
    return this.validatePolicyById(this.authService.groupPolicies$, groupPolicyId);
  }
  // authorizable group permission
  validateAuthorizableGroupPolicy(group: Group, permissionName: string): Observable<boolean> {
    const groupPolicyId = this.configService.getGroupPolicyId(group, permissionName);
    return this.validatePolicyById(this.authService.authorizableGroupPolicies$, groupPolicyId);
  }

  validateAuthoriableGroupPolicyAny(group: Group): Observable<boolean> {
    const groupPolicyIds = this.configService.getAuthorizableGroupPolicyIds(group);
    return this.validatePolicyByIds(this.authService.authorizableGroupPolicies$, groupPolicyIds);
  }

  // space management
  validateSpacePolicy(space: Space, permissionName: string): Observable<boolean> {
    const spacePolicyId = this.configService.getSpacePolicyId(space, permissionName);
    return this.validatePolicyById(this.authService.spacePolicies$, spacePolicyId);
  }

  validateAuthoriableSpacePolicyAny(space: Space): Observable<boolean> {
    const spacePolicyIds = this.configService.getAuthorizableSpacePolicyIds(space);
    return this.validatePolicyByIds(this.authService.authorizableSpacePolicies$, spacePolicyIds);
  }
  // authorizable space permission
  validateAuthorizableSpacePolicy(space: Space, permissionName: string): Observable<boolean> {
    const spacePolicyId = this.configService.getSpacePolicyId(space, permissionName);
    return this.validatePolicyById(this.authService.authorizableSpacePolicies$, spacePolicyId);
  }

  validateSpacePolicybySpaceId(spaceId: string, permissionName: string): Observable<boolean> {
    return this.configService.getSpacePolicyIdBySpaceId(spaceId, permissionName).pipe(
      switchMap(data => { return this.validatePolicyById(this.authService.spacePolicies$, data) })
    );

  }

}
