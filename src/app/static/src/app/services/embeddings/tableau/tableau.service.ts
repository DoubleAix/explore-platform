import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TableauDashboard } from '../../../models/tableau.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableauService {

  constructor(private http: HttpClient) { }

  getDashboardUrl(id: number):Observable<TableauDashboard> {
    return this.http.get<TableauDashboard>('/api/tableau/' + id);
  }
}
