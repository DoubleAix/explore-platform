import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SpaceResources, SpaceResource } from "src/app/models/space-resource.model";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpaceResourceService {
  baseUrl = '/api/space_resource/';

  constructor(private http: HttpClient) {

  }

  getSpaceResources(id: number): Observable<SpaceResources> {
    return this.http.get<SpaceResources>(this.baseUrl + id);
  }

  updateSpaceResources(data: SpaceResource[], id: number): Observable<SpaceResources> {
    return this.http.post<SpaceResources>(this.baseUrl + id, data);
  }
}

