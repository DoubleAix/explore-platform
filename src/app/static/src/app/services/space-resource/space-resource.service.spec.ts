import { TestBed } from '@angular/core/testing';

import { SpaceResourceService } from './space-resource.service';

describe('SpaceResourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpaceResourceService = TestBed.get(SpaceResourceService);
    expect(service).toBeTruthy();
  });
});
