import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { animateChild, trigger, state, style, animate, transition, group, stagger, query, keyframes } from '@angular/animations';
import { Space } from 'src/app/models/space.model';
// import { SpaceService } from 'src/app/services/space/space.service';
import { PermissionService } from 'src/app/services/permission/permission.service';
import { ConfigService } from 'src/app/services/config/config.service';
import { ThemeService } from '../services/theme/theme.service';

@Component({
  selector: 'app-space-page',
  templateUrl: './space-page.component.html',
  styleUrls: ['./space-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('items', [
      transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }),  // initial
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({ transform: 'scale(1)', opacity: 1 }))  // final
      ]),
    ]),
    trigger('list', [
      transition(':enter', [
        query('@items', stagger(50, animateChild()), { optional: true })
      ]),
    ])
  ]
})
export class SpacePageComponent implements OnInit {
  title = '探索平台';
  logo = './assets/logo.takoyaki.png';
  spaceList: Observable<Space[]>;
  constructor(
    // private spaceService: SpaceService,
    private configService: ConfigService,
    private router: Router,
    private titleService: Title,
    private permissionService: PermissionService,
    private themeService: ThemeService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.themeService.toggleTheme('basic-theme');
    this.spaceList = this.configService.spaceMapping$;
    this.configService.getSpaceMapping().subscribe();
  }

  accessSpace(spaceId: string): void {
    this.router.navigate([`/dashboard/${spaceId}`]);
  }

  validateSpacePermission(space: Space, permissionName: string): Observable<boolean> {
    // console.log(space);
    return this.permissionService.validateSpacePolicy(space, permissionName);
  }
}
