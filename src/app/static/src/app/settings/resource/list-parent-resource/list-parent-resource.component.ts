import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Observable } from 'rxjs';

import { MessageService } from '../../../services/message/message.service';
import { ParentResourceService } from '../../../services/parent-resource/parent-resource.service';
import { ParentResource } from '../../../models/resource.model';
import { ConfirmDialogComponent } from '../../../utils/dialogs/confirm-dialog/confirm-dialog.component';
import { AuthService } from '../../../services/auth/auth.service';

import { AddParentResourceComponent } from '../add-parent-resource/add-parent-resource.component';
import { PermissionService } from '../../../services/permission/permission.service';
import { SettingsResourceOption } from '../../../enums/settings-resource-option.enum';
import { SettingDataSource } from '../../shared/setting.data-source';
import { RefreshInfo } from '../../../models/refresh-info.model';
import { UserPolicyType } from '../../../enums/user-policy-type.enum';

@Component({
  selector: 'app-list-parent-resource',
  templateUrl: './list-parent-resource.component.html',
  styleUrls: ['./list-parent-resource.component.scss']
})
export class ListParentResourceComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'parent_resource_name', 'icon_name','tags', 'reserved', 'actions'];
  parentResourcesDataSource = new SettingDataSource<ParentResource>();

  constructor(
    private parentResourceService: ParentResourceService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
    private permissionService: PermissionService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.parentResourcesDataSource.initData(this.parentResourceService.getParentResources());
    this.parentResourcesDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁資源數：';
    this.parentResourcesDataSource.sort = this.sort;
    this.refreshUserPolicies();
  }

  applyFilter(filterValue: string) {
    this.parentResourcesDataSource.filter = filterValue.trim().toLowerCase();
  }

  createParentResource(): void {
    const dialogRef = this.dialog.open(
      AddParentResourceComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          update: false,
        },
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.parentResourceService.createParentResource(data)
            .subscribe(
              (response: ParentResource) => {
                this.parentResourcesDataSource.create(response);
                this.messageService.openInfoSnackBar('已經成功建立 ' + data.parent_resource_name);
                this.refreshUserPolicies();


              },
              error => {
                this.messageService.openErrorSnackBar('群組ID ' + data.parent_resource_name + ' 已經存在');
              });
        }
      },
    );
  }

  editParentResource(parentResource: ParentResource): void {
    const dialogRef = this.dialog.open(
      AddParentResourceComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          parent_resource_obj: parentResource,
          update: true,
        },
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.parentResourceService.updateParentResource(data, parentResource.id)
            .subscribe(
              (response: ParentResource) => {
                this.parentResourcesDataSource.update(response);
                this.messageService.openInfoSnackBar('已經成功更新 ' + data.parent_resource_name);
                // this.refreshUserPolicies();
              },
              error => {
                this.messageService.openErrorSnackBar('主要資源 ' + data.parent_resource_name + ' 不存在');
              });
        }
      },
    );
  }



  deleteParentResource(parentResource: ParentResource): void {
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent,
      {
        autoFocus: false,
        data: {
          type: '主要資源',
          id: parentResource.id,
          name: parentResource.parent_resource_name,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.parentResourceService.deleteParentResource(parentResource.id)
            .subscribe((response: ParentResource) => {
              this.parentResourcesDataSource.delete(response);
              this.messageService.openInfoSnackBar('已經刪除 ' + parentResource.parent_resource_name);
            });
        }
      });
  }

  private refreshUserPolicies(): void {
    this.authService.partialRefresh([
      UserPolicyType.RESOURCE_POLICY,
      UserPolicyType.AUTHORIZABLE_RESOURCE_POLICY
    ]).subscribe();
  }

  validateResourceManagementPermission(permissionName: string): Observable<boolean> {
    return this.permissionService.validateSettingsResourcePolicy(SettingsResourceOption.RESOURCE_MANAGEMENT, permissionName);
  }

  validateParentResourcePermission(parentResource: ParentResource, permissionName: string): Observable<boolean> {
    return this.permissionService.validateResourcePolicy(parentResource, permissionName);
  }

  validateParentResourceAuthorizableAny(parentResource: ParentResource): Observable<boolean> {
    return this.permissionService.validateAuthoriableResourcePolicyAny(parentResource);
  }

  editParentResourceAuthorization(parentResource: ParentResource): void {
    this.router.navigate(['../edit_parent_resource_authorization', parentResource.id],
      {
        relativeTo: this.route,
      });
  }

  editParentResourceChildren(parentResource: ParentResource): void {
    this.router.navigate(['../list_child_resource', parentResource.id],
      {
        relativeTo: this.route,
      });
  }
}
