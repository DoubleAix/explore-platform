import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListParentResourceComponent } from './list-parent-resource.component';

describe('ListParentResourceComponent', () => {
  let component: ListParentResourceComponent;
  let fixture: ComponentFixture<ListParentResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListParentResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListParentResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
