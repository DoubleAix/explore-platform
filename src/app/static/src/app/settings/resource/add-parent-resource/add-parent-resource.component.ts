import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { validation_messages, ParentResourceValidationMessages } from './add-parent-resource.validation.messages';
import { iconList } from '../resource.icons';

@Component({
  selector: 'app-add-parent-resource',
  templateUrl: './add-parent-resource.component.html',
  styleUrls: ['./add-parent-resource.component.scss']
})
export class AddParentResourceComponent implements OnInit {
  parentResourceForm: FormGroup;
  validation_messages: ParentResourceValidationMessages = validation_messages;
  iconList: string[] = iconList;
  update: boolean;
  //chips
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;
  // Enter, comma
  separatorKeysCodes = [ENTER, COMMA];

  constructor(
    private dialogRef: MatDialogRef<AddParentResourceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) {
    this.update = data.update;
  }

  ngOnInit() {
    this.buildForm();

    if (this.update) {

      // this.parentResourceForm.patchValue(this.data.parent_resource_obj);
      this.parentResourceForm.patchValue({
        authorization_type_id: this.data.parent_resource_obj.authorization_type_id,
        icon_name: this.data.parent_resource_obj.icon_name,
        id: this.data.parent_resource_obj.id,
        label: this.data.parent_resource_obj.label,
        layer: this.data.parent_resource_obj.layer,
        parent_resource_name: this.data.parent_resource_obj.parent_resource_name,
        type: this.data.parent_resource_obj.type
      });
      // const tags = this.parentResourceForm.get('tags') as FormArray;
      // for (let tag of this.data.parent_resource_obj.tags) {
      //   tags.push(this.fb.control(tag.trim()));
      // }
      this.parentResourceForm.setControl('tags', this.fb.array(this.data.parent_resource_obj.tags || []));
      this.parentResourceForm.controls['parent_resource_name'].disable();
    }
  }

  buildForm() {
    this.parentResourceForm = this.fb.group({
      parent_resource_name: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]
      ],
      icon_name: ['dashboard', [
        Validators.required,
        Validators.maxLength(20)
      ]
      ],
      tags: this.fb.array([])
    });
  }

  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // Add our tags
    if ((value || '').trim()) {
      const tags = this.parentResourceForm.get('tags') as FormArray;
      tags.push(this.fb.control(value.trim()));
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(index: number): void {
    const requirements = this.parentResourceForm.get('tags') as FormArray;

    if (index >= 0) {
      requirements.removeAt(index);
    }
  }

  validateByControl(validation, controlName: string | string[]): boolean {
    return this.parentResourceForm.get(controlName).hasError(validation.type) && (this.parentResourceForm.get(controlName).dirty || this.parentResourceForm.get(controlName).touched);
  }

  public closeDialog() {
    this.dialogRef.close();
  }
  public confirm() {
    if (this.update) {
      this.parentResourceForm.controls['parent_resource_name'].enable();
    }
    this.dialogRef.close(this.parentResourceForm.value);
  }


}
