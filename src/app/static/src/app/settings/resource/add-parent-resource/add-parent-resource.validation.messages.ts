export const validation_messages = {
    'parent_resource_name': [
        { type: 'required', message: '主要資源名稱是必要欄位' },
        { type: 'maxlength', message: '主要資源名稱最多只能輸入10個字元' },
    ],
    'icon_name': [
        { type: 'required', message: 'Icon 樣式是必要欄位' },
        { type: 'maxlength', message: 'Icon 樣式最多只能輸入20個字元' },
    ],
};

export interface ParentResourceValidationMessages {
    parent_resource_name?: object[];
    icon_name?: object[];
};