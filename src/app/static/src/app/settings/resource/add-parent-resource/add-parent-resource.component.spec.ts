import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddParentResourceComponent } from './add-parent-resource.component';

describe('AddParentResourceComponent', () => {
  let component: AddParentResourceComponent;
  let fixture: ComponentFixture<AddParentResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddParentResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddParentResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
