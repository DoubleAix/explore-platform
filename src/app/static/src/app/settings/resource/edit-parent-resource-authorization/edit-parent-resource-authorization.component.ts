import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { take, filter } from 'rxjs/operators'
import { BehaviorSubject, Observable, of } from 'rxjs';

import { ConfigService } from '../../../services/config/config.service';
import { ParentResourceService } from '../../../services/parent-resource/parent-resource.service';
import { ParentResource, ChildResource } from '../../../models/resource.model';
import { Permission } from '../../../models/permission.model';
import { PermissionService } from 'src/app/services/permission/permission.service';


@Component({
  selector: 'app-edit-parent-resource-authorization',
  templateUrl: './edit-parent-resource-authorization.component.html',
  styleUrls: ['./edit-parent-resource-authorization.component.scss']
})
export class EditParentResourceAuthorizationComponent implements OnInit, OnDestroy {
  parentResource = new BehaviorSubject<ParentResource>(null);
  parentResourcePermission: Permission[];


  constructor(
    private parentResourceService: ParentResourceService,
    private router: Router,
    private route: ActivatedRoute,
    private configService: ConfigService,
    private permissionService: PermissionService,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.parentResourceService.getParentResourceById(params['id']).subscribe(
          data => {
            this.parentResource.next(data);
            this.configService.getAuthorizationOption(data.authorization_type_id).subscribe(
              data => {
                this.parentResourcePermission = data;
              },
              error => console.log(error),
            );
          }
        );

      });
  }
  ngOnDestroy() {
    this.parentResource.complete();
  }
  validateParentResourceAuthorizable(permission: string): Observable<boolean> {
    return this.permissionService.validateAuthorizableResourcePolicy(this.parentResource.value, permission)
  }

  returnResourceManagement(): void {
    this.router.navigate(['../../list_parent_resource'], { relativeTo: this.route });
  }
}
