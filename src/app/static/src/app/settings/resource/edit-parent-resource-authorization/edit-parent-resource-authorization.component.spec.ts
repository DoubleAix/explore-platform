import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditParentResourceAuthorizationComponent } from './edit-parent-resource-authorization.component';

describe('EditParentResourceAuthorizationComponent', () => {
  let component: EditParentResourceAuthorizationComponent;
  let fixture: ComponentFixture<EditParentResourceAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditParentResourceAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditParentResourceAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
