import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListChildResourceComponent } from './list-child-resource.component';

describe('ListChildResourceComponent', () => {
  let component: ListChildResourceComponent;
  let fixture: ComponentFixture<ListChildResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListChildResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListChildResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
