import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { take, filter } from 'rxjs/operators';
import { BehaviorSubject, Observable, of } from 'rxjs';

import { AuthService } from '../../../services/auth/auth.service';
import { MessageService } from '../../../services/message/message.service';
import { ChildResourceService } from '../../../services/child-resource/child-resource.service';
import { ChildResource, ParentResource } from '../../../models/resource.model';
import { ConfirmDialogComponent } from '../../../utils/dialogs/confirm-dialog/confirm-dialog.component';
import { AddChildResourceComponent } from '../add-child-resource/add-child-resource.component';
import { ParentResourceService } from 'src/app/services/parent-resource/parent-resource.service';
import { PermissionService } from '../../../services/permission/permission.service';

import { SettingDataSource } from '../../shared/setting.data-source';
import { UserPolicyType } from '../../../enums/user-policy-type.enum';

@Component({
  selector: 'app-list-child-resource',
  templateUrl: './list-child-resource.component.html',
  styleUrls: ['./list-child-resource.component.scss']
})
export class ListChildResourceComponent implements OnInit {
  parentResource = new BehaviorSubject<ParentResource>(null);


  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'child_resource_name', 'icon_name','label', 'reserved', 'actions'];
  childResourcesDataSource = new SettingDataSource<ChildResource>();

  applyFilter(filterValue: string) {
    this.childResourcesDataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private parentResourceService: ParentResourceService,
    private childResourceService: ChildResourceService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
    private permissionService: PermissionService,
    private authService: AuthService,
  ) {

  }

  ngOnInit() {

    this.route.params.subscribe(
      params => {
        this.parentResourceService.getParentResourceById(params['id']).subscribe(
          data => {
            this.parentResource.next(data);

          }
        );
        this.childResourcesDataSource.initData(this.childResourceService.getChildResources(params['id']));
      });

    this.childResourcesDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁資源數：';
    this.childResourcesDataSource.sort = this.sort;
    this.refreshUserPolicies();
  }

  createChildResource(): void {
    const dialogRef = this.dialog.open(
      AddChildResourceComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          update: false,
        },
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.childResourceService.createChildResource(data, this.parentResource.value.id)
            .subscribe(
              (response: ChildResource) => {
                this.childResourcesDataSource.create(response);
                this.messageService.openInfoSnackBar('已經成功建立 ' + data.child_resource_name);
                this.refreshUserPolicies();
              },
              error => {
                this.messageService.openErrorSnackBar('子資源 ' + data.child_resource_name + ' 已經存在');
              });
        }
      },
    );
  }

  editChildResource(childResource: ChildResource): void {
    const dialogRef = this.dialog.open(
      AddChildResourceComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          child_resource_obj: childResource,
          update: true,
        },
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.childResourceService.updateChildResource(data, childResource.id)
            .subscribe(
              (response: ChildResource) => {
                this.childResourcesDataSource.update(response);
                this.messageService.openInfoSnackBar('已經成功更新 ' + data.child_resource_name);
                // this.refreshUserPolicies();
              },
              error => {
                this.messageService.openErrorSnackBar('子資源 ' + data.child_resource_name + ' 不存在');
              });
        }
      },
    );
  }


  deleteChildResource(childResource: ChildResource): void {
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent,
      {
        autoFocus: false,
        data: {
          type: '子資源',
          id: childResource.id,
          name: childResource.child_resource_name,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.childResourceService.deleteChildResource(childResource.id)
            .subscribe((response: ChildResource) => {
              this.childResourcesDataSource.delete(response);
              this.messageService.openInfoSnackBar('已經刪除 ' + childResource.child_resource_name);

            });
        }
      });
  }



  validateParentResourcePermission(permissionName: string): Observable<boolean> {
    let validateResult: Observable<boolean>;
    this.parentResource.pipe(
      filter(data => data != null),
      take(1),
    ).subscribe(
      data => {
        try {
          validateResult = this.permissionService.validateResourcePolicy(data, permissionName);
        } catch (e) {
          // because some resource are reserved, they don't have some permission types like 'create_child'.
          if (data.reserved) {
            return of(false);
          } else {
            console.log(e);
          }
        }

      }
    );
    return validateResult;
  }
  private refreshUserPolicies(): void {
    this.authService.partialRefresh([
      UserPolicyType.RESOURCE_POLICY,
      UserPolicyType.AUTHORIZABLE_RESOURCE_POLICY
    ]).subscribe();
  }
  validateChildResourcePermission(childResource: ChildResource, permissionName: string): Observable<boolean> {
    return this.permissionService.validateResourcePolicy(childResource, permissionName);
  }

  validateChildResourceAuthorizableAny(childResource: ChildResource): Observable<boolean> {
    return this.permissionService.validateAuthoriableResourcePolicyAny(childResource);
  }



  editChildResourceAuthorization(childResource: ChildResource): void {
    this.router.navigate(['../../edit_child_resource_authorization', childResource.id],
      {
        relativeTo: this.route,
      });
  }


  returnResourceManagement(): void {
    this.router.navigate(['../../list_parent_resource'], { relativeTo: this.route });
  }
}
