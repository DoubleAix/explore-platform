import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChildResourceComponent } from './add-child-resource.component';

describe('AddChildResourceComponent', () => {
  let component: AddChildResourceComponent;
  let fixture: ComponentFixture<AddChildResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChildResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChildResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
