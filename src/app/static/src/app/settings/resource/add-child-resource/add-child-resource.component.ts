import { Component, OnInit, Inject, Injectable } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { validation_messages, ChildResourceValidationMessages } from './add-child-resource.validation.messages';
import { iconList } from '../resource.icons';
import { EmbeddingOption } from '../../../enums/embedding-option.enum';

@Component({
  selector: 'app-add-child-resource',
  templateUrl: './add-child-resource.component.html',
  styleUrls: ['./add-child-resource.component.scss']
})
export class AddChildResourceComponent implements OnInit {
  childResourceForm: FormGroup;
  validation_messages: ChildResourceValidationMessages = validation_messages;
  iconList: string[] = iconList;
  embeddingOption: typeof EmbeddingOption = EmbeddingOption;
  embeddingOptionList = Object.keys(EmbeddingOption).map(k => EmbeddingOption[k as string]);
  update: boolean;

  constructor(
    private dialogRef: MatDialogRef<AddChildResourceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) {
    this.update = data.update;
   }

  ngOnInit() {
    this.buildForm();
    if (this.update) {
      this.childResourceForm.patchValue(this.data.child_resource_obj);
      this.childResourceForm.controls['child_resource_name'].disable();
    }
    this.subscribeLabelChanges();
    this.setLabel(this.embeddingOption.TABLEAU);
  }

  buildForm() {
    this.childResourceForm = this.fb.group({
      child_resource_name: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]
      ],
      icon_name: ['dashboard', [
        Validators.required,
      ],
      ],
      type: ['embeddings', [
        Validators.required,
      ],
      ],
      label: ['', [
        Validators.required,
      ],
      ],
      data: this.initChildResourceDataFormGroup()
    });
  }

  initChildResourceDataFormGroup() {
    const group = this.fb.group(
      {
        tableau: this.fb.group(this.initTableauDataModel()),
        kibana: this.fb.group(this.initKibanaDataModel()),
      }
    );
    return group;
  }

  initTableauDataModel() {
    const model =
    {
      site_name: ['VL900', [
        Validators.required,
      ],
      ],
      workbook_name: ['', [
        Validators.required,
      ],
      ],
      dashboard_name: ['', [
        Validators.required,
      ],
      ],
      width: ['', [
        Validators.required,
        Validators.max(2000),
        Validators.min(500),
      ],
      ],
      height: ['', [
        Validators.required,
        Validators.max(1600),
        Validators.min(400),
      ],
      ],
    }

    return model;
  }

  initKibanaDataModel() {
    const model =
    {
      url: ['', [
        Validators.required,
      ],
      ],
    }

    return model;
  }
  setLabel(label: string) {
    // update label value
    const ctrl: FormControl = (<any>this.childResourceForm).controls.label;
    ctrl.setValue(label);
}
  subscribeLabelChanges() {
    // controls
    const dataCtrl = (<any>this.childResourceForm).controls.data;
    const tableauCtrl = dataCtrl.controls.tableau;
    const kibanaCtrl = dataCtrl.controls.kibana;

    // initialize value changes stream
    const changes$ = this.childResourceForm.controls.label.valueChanges;

    // subscribe to the stream
    changes$.subscribe(label => {
      // TABLEAU
      if (label === this.embeddingOption.TABLEAU) {
        // apply validators to each tableau fields, retrieve validators from tableau model
        Object.keys(tableauCtrl.controls).forEach(key => {
          tableauCtrl.controls[key].setValidators(this.initTableauDataModel()[key][1]);
          tableauCtrl.controls[key].updateValueAndValidity();
        });

        // remove all validators from kibana fields
        Object.keys(kibanaCtrl.controls).forEach(key => {
          kibanaCtrl.controls[key].setValidators(null);
          kibanaCtrl.controls[key].updateValueAndValidity();
        });
      }

      // KIBANA
      if (label === this.embeddingOption.KIBANA) {
        // remove all validators from tableau fields
        Object.keys(tableauCtrl.controls).forEach(key => {
          tableauCtrl.controls[key].setValidators(null);
          tableauCtrl.controls[key].updateValueAndValidity();
        });

        // apply validators to each kibana fields, retrieve validators from kibana model
        Object.keys(kibanaCtrl.controls).forEach(key => {
          kibanaCtrl.controls[key].setValidators(this.initKibanaDataModel()[key][1]);
          kibanaCtrl.controls[key].updateValueAndValidity();
        });
      }

    });
  }



  validateByControl(validation, controlName: string | string[]): boolean {
    return this.childResourceForm.get(controlName).hasError(validation.type) && (this.childResourceForm.get(controlName).dirty || this.childResourceForm.get(controlName).touched);
  }

  public closeDialog() {
    this.dialogRef.close();
  }
  public confirm() {
    this.childResourceForm.controls['child_resource_name'].enable();
    this.dialogRef.close(this.childResourceForm.value);
  }
}

