export const validation_messages = {
    'child_resource_name': [
        { type: 'required', message: '子資源名稱是必要欄位' },
        { type: 'maxlength', message: '子資源名稱最多只能輸入10個字元' },
    ],
    'icon_name': [
        { type: 'required', message: 'Icon 樣式是必要欄位' },
        // { type: 'maxlength', message: 'Icon 樣式最多只能輸入20個字元' },
    ],
    'site_name': [
        { type: 'required', message: '站台名稱是必要欄位' },
        // { type: 'maxlength', message: 'Icon 樣式最多只能輸入20個字元' },
    ],
    'workbook_name': [
        { type: 'required', message: '工作簿名稱是必要欄位' },
        // { type: 'maxlength', message: 'Icon 樣式最多只能輸入20個字元' },
    ],
    'dashboard_name': [
        { type: 'required', message: 'Dashboard 名稱是必要欄位' },
        // { type: 'maxlength', message: 'Icon 樣式最多只能輸入20個字元' },
    ],
    'width': [
        { type: 'required', message: '寬度是必要欄位' },
        { type: 'max', message: '寬度最大值為 2000px' },
        { type: 'min', message: '寬度最小值為 500px' },
    ],
    'height': [
        { type: 'required', message: '高度是必要欄位' },
        { type: 'max', message: '高度最大值為 1600px' },
        { type: 'min', message: '高度最小值為 400px' },
    ],
    
};

export interface ChildResourceValidationMessages {
    child_resource_name?: object[];
    icon_name?: object[];
    site_name?: object[];
    workbook_name?: object[];
    dashboard_name?: object[];
    width?: object[];
    height?: object[];
};