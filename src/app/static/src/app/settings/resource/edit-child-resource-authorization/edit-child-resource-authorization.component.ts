import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { take, filter } from 'rxjs/operators'
import { BehaviorSubject, Observable, of } from 'rxjs';

import { ConfigService } from '../../../services/config/config.service';
import { ChildResourceService } from '../../../services/child-resource/child-resource.service';
import { ParentResource, ChildResource } from '../../../models/resource.model';
import { Permission } from '../../../models/permission.model';
import { PermissionService } from 'src/app/services/permission/permission.service';


@Component({
  selector: 'app-edit-child-resource-authorization',
  templateUrl: './edit-child-resource-authorization.component.html',
  styleUrls: ['./edit-child-resource-authorization.component.scss']
})
export class EditChildResourceAuthorizationComponent implements OnInit {
  childResource = new BehaviorSubject<ChildResource>(null);
  childResourcePermission: Permission[];


  constructor(
    private childResourceService: ChildResourceService,
    private router: Router,
    private route: ActivatedRoute,
    private configService: ConfigService,
    private permissionService: PermissionService,
  ) {


  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.childResourceService.getChildResourceById(params['id']).subscribe(
          data => {
            this.childResource.next(data)
            this.configService.getAuthorizationOption(data.authorization_type_id).subscribe(
              data => {
                this.childResourcePermission = data;
              },
              error => console.log(error),
            );
          }
        );
      });
  }
  validateChildResourceAuthorizable(permission: string): Observable<boolean> {
    return this.permissionService.validateAuthorizableResourcePolicy(this.childResource.value, permission)
  }


  returnResourceManagement(): void {
    this.router.navigate(['../../list_child_resource', this.childResource.value.parent_id], { relativeTo: this.route });
  }

}
