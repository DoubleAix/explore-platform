import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChildResourceAuthorizationComponent } from './edit-child-resource-authorization.component';

describe('EditChildResourceAuthorizationComponent', () => {
  let component: EditChildResourceAuthorizationComponent;
  let fixture: ComponentFixture<EditChildResourceAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChildResourceAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChildResourceAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
