import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';

import { MessageService } from '../../../services/message/message.service';
import { ResourcePolicyService } from '../../../services/resource-policy/resource-policy.service';
import { Group } from '../../../models/group.model';
import { Member } from '../../../models/member.model';
import { ParentResource, ChildResource } from '../../../models/resource.model';
import { SettingDataSource } from '../../shared/setting.data-source';

@Component({
  selector: 'app-edit-resource-authorization-group',
  templateUrl: './edit-resource-authorization-group.component.html',
  styleUrls: ['./edit-resource-authorization-group.component.scss']
})
export class EditResourceAuthorizationGroupComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() resource: ParentResource | ChildResource;
  @Input() permissionName: string;

  userType = 'group';
  displayedColumns: string[] = ['id', 'group_id', 'group_name', 'reserved', 'actions'];
  groupsDataSource = new SettingDataSource<Group | Member>();

  groups: Group[] = [];

  constructor(
    private resourcePolicyService: ResourcePolicyService,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.groupsDataSource.initData(this.resourcePolicyService.getMembers(this.resource.id, this.permissionName, this.userType));
    });

    this.groupsDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁群組數：';
    this.groupsDataSource.sort = this.sort;
  }

  addMembers(event: boolean): void {
    this.resourcePolicyService.addMembers(this.resource.id, this.permissionName, this.groups, this.userType).subscribe(
      (data: Group[]) => {
        this.groups = [];
        this.groupsDataSource.data = data;
        this.messageService.openInfoSnackBar('已經新增群組至該權限');
      }
    );
  }

  deleteMember(group: Group): void {

    this.resourcePolicyService.deleteMember(this.resource.id, this.permissionName, group.id, this.userType)
      .subscribe(
        (data: Group) => {
          this.groupsDataSource.delete(data);        
          this.messageService.openInfoSnackBar('已經刪除 ' + group.group_id);
        });
  }
}
