import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditResourceAuthorizationGroupComponent } from './edit-resource-authorization-group.component';

describe('EditResourceAuthorizationGroupComponent', () => {
  let component: EditResourceAuthorizationGroupComponent;
  let fixture: ComponentFixture<EditResourceAuthorizationGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditResourceAuthorizationGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditResourceAuthorizationGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
