import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';

import { MessageService } from '../../../services/message/message.service';
import { ResourcePolicyService } from '../../../services/resource-policy/resource-policy.service';
import { Member } from '../../../models/member.model';
import { Group } from '../../../models/group.model';
import { ParentResource, ChildResource } from '../../../models/resource.model';
import { SettingDataSource } from '../../shared/setting.data-source';

@Component({
  selector: 'app-edit-resource-authorization-member',
  templateUrl: './edit-resource-authorization-member.component.html',
  styleUrls: ['./edit-resource-authorization-member.component.scss']
})
export class EditResourceAuthorizationMemberComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() resource: ParentResource | ChildResource;
  @Input() permissionName: string;
  userType = 'user';
  displayedColumns: string[] = ['user_id', 'user_name', 'email', 'reserved', 'actions'];
  membersDataSource = new SettingDataSource<Member | Group>();


  members: Member[] = [];

  constructor(
    private resourcePolicyService: ResourcePolicyService,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.membersDataSource.initData(this.resourcePolicyService.getMembers(this.resource.id, this.permissionName, this.userType));
    });

    this.membersDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁使用者數：';
    this.membersDataSource.sort = this.sort;
  }

  addMembers(event: boolean): void {
    this.resourcePolicyService.addMembers(this.resource.id, this.permissionName, this.members, this.userType).subscribe(
      (data: Member[]) => {
        this.members = [];
        this.membersDataSource.data = data;
        this.messageService.openInfoSnackBar('已經新增使用者至該權限');
      }
    );
  }

  deleteMember(member: Member): void {

    this.resourcePolicyService.deleteMember(this.resource.id, this.permissionName, member.id, this.userType)
      .subscribe(
        (data: Member) => {
          this.membersDataSource.delete(data);
          this.messageService.openInfoSnackBar('已經刪除 ' + member.user_id);
        });
  }


}
