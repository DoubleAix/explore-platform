import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditResourceAuthorizationMemberComponent } from './edit-resource-authorization-member.component';

describe('EditResourceAuthorizationMemberComponent', () => {
  let component: EditResourceAuthorizationMemberComponent;
  let fixture: ComponentFixture<EditResourceAuthorizationMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditResourceAuthorizationMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditResourceAuthorizationMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
