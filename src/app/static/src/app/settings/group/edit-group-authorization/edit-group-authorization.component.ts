import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { take, filter } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

import { Permission } from '../../../models/permission.model';
import { Group } from '../../../models/group.model';
import { ConfigService } from '../../../services/config/config.service';
import { GroupService } from 'src/app/services/group/group.service';
import { PermissionService } from 'src/app/services/permission/permission.service';

@Component({
  selector: 'app-edit-group-authorization',
  templateUrl: './edit-group-authorization.component.html',
  styleUrls: ['./edit-group-authorization.component.scss']
})
export class EditGroupAuthorizationComponent implements OnInit {

  groupPermission: Permission[];
  group = new BehaviorSubject<Group>(null);


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private configService: ConfigService,
    private groupService: GroupService,
    private permissionService: PermissionService,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {

      this.groupService.getGroupById(params['id']).subscribe(
        data => {
          this.group.next(data);

          this.configService.getAuthorizationOption(data.authorization_type_id).subscribe(
            data => {

              this.groupPermission = data;
            },
            error => console.log(error),
          );
        }
      )
    });

  }

  validateGroupAuthorizable(permission: string): Observable<boolean> {
    return this.permissionService.validateAuthorizableGroupPolicy(this.group.value, permission);
  }

  returnGroupManagement(): void {
    this.router.navigate(['../../list_group'], { relativeTo: this.route });
  }
}
