import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupAuthorizationComponent } from './edit-group-authorization.component';

describe('EditGroupAuthorizationComponent', () => {
  let component: EditGroupAuthorizationComponent;
  let fixture: ComponentFixture<EditGroupAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGroupAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGroupAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
