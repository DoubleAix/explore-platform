export const validation_messages = {
    'group_id': [
        { type: 'required', message: '群組ID是必要欄位' },
        { type: 'maxlength', message: '群組ID最多只能輸入10個字元' },
    ],
    'group_name': [
        { type: 'required', message: '群組名稱是必要欄位' },
        { type: 'maxlength', message: '群組名稱最多只能輸入20個字元' },
    ],
};

export interface GroupValidationMessages {
    group_id?: object[];
    group_name?: object[];
};