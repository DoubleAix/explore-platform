import { Component, OnInit, Inject, Injectable } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { validation_messages, GroupValidationMessages } from './add-group.validation.messages';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss']
})
export class AddGroupComponent implements OnInit {
  groupForm: FormGroup;
  validation_messages: GroupValidationMessages = validation_messages;

  constructor(
    private dialogRef: MatDialogRef<AddGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.groupForm = this.fb.group({
      group_id: ['', [
        Validators.required,
        Validators.maxLength(10)
      ]
      ],
      group_name: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]
      ],
    });
  }
  validateByControl(validation, controlName: string| string[]): boolean {
    return this.groupForm.get(controlName).hasError(validation.type) && (this.groupForm.get(controlName).dirty || this.groupForm.get(controlName).touched)
  }

  public closeDialog() {
    this.dialogRef.close();
  }
  public confirm() {

    this.dialogRef.close(this.groupForm.value);
  }
}
