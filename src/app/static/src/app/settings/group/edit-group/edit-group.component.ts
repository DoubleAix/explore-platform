import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { take, filter } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

import { MessageService } from '../../../services/message/message.service';
import { GroupService } from '../../../services/group/group.service';
import { GroupPolicyService } from '../../../services/group-policy/group-policy.service';
import { Group } from '../../../models/group.model';
import { Member } from '../../../models/member.model';
import { SettingDataSource } from '../../shared/setting.data-source'
import { PermissionOption } from '../../../enums/permission-option.enum';

@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.scss']
})
export class EditGroupComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'user_id', 'user_name', 'email', 'reserved', 'actions'];
  membersDataSource = new SettingDataSource<Member>();

  group = new BehaviorSubject<Group>(null);

  members: Member[] = [];

  constructor(
    private groupService: GroupService,
    private groupPolicyService: GroupPolicyService,
    private router: Router,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,

  ) {

  }

  ngOnInit() {

    this.route.params.subscribe(params => {

      this.groupService.getGroupById(params['id']).subscribe(
        data => {
          this.group.next(data);

        }
      )
      this.membersDataSource.initData(this.groupPolicyService.getMembers(params['id'], PermissionOption.BELONG));
    });
    this.membersDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁群組數：';
    this.membersDataSource.sort = this.sort;
  }

  addMembers(event: boolean): void {
    this.groupPolicyService.addMembers(this.group.value.id, PermissionOption.BELONG, this.members).subscribe(
      data => {
        this.members = [];
        this.membersDataSource.data = data;
        this.messageService.openInfoSnackBar('已經新增使用者至群組');

      }
    );
  }

  deleteMember(member: Member): void {

    this.groupPolicyService.deleteMember(this.group.value.id, PermissionOption.BELONG, member.id)
      .subscribe(data => {
        this.membersDataSource.delete(data);
        this.messageService.openInfoSnackBar('已經刪除 ' + member.user_id);
      });
  }

  returnGroupManagement(): void {
    this.router.navigate(['../../list_group'], { relativeTo: this.route });
  }

}
