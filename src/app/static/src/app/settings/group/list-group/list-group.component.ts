import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Observable } from 'rxjs';

import { MessageService } from '../../../services/message/message.service';
import { GroupService } from '../../../services/group/group.service';
import { Group } from '../../../models/group.model';
import { ConfirmDialogComponent } from '../../../utils/dialogs/confirm-dialog/confirm-dialog.component';
import { AddGroupComponent } from '../add-group/add-group.component';
import { PermissionService } from '../../../services/permission/permission.service';
import { SettingsResourceOption } from '../../../enums/settings-resource-option.enum';
import { SettingDataSource } from '../../shared/setting.data-source';
import { AuthService } from '../../../services/auth/auth.service';
import { UserPolicyType } from '../../../enums/user-policy-type.enum';

@Component({
  selector: 'app-list-group',
  templateUrl: './list-group.component.html',
  styleUrls: ['./list-group.component.scss'],
})
export class ListGroupComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'group_id', 'group_name', 'reserved', 'actions'];
  groupsDataSource = new SettingDataSource<Group>();

  applyFilter(filterValue: string) {
    this.groupsDataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private groupService: GroupService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
    private permissionService: PermissionService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.groupsDataSource.initData(this.groupService.getGroups());
    this.groupsDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁群組數：';
    this.groupsDataSource.sort = this.sort;
    this.refreshUserPolicies();
    // console.log(this.router.url);
  }

  createGroup(): void {
    const dialogRef = this.dialog.open(
      AddGroupComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          type: 'group',
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.groupService.createGroup(data)
            .subscribe(
              (response: Group) => {
                this.groupsDataSource.create(response);
                this.messageService.openInfoSnackBar('已經成功建立 ' + data.group_name);
                this.refreshUserPolicies();
              },
              error => {
                this.messageService.openInfoSnackBar('群組ID ' + data.group_id + ' 已經存在');
              });
        }
      },
    );
  }

  deleteGroup(group: Group): void {
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent,
      {
        autoFocus: false,
        data: {
          type: '群組',
          id: group.id,
          name: group.group_name,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.groupService.deleteGroup(group.id)
            .subscribe(response => {
              this.groupsDataSource.delete(response);
              this.messageService.openInfoSnackBar('已經刪除 ' + group.group_name);
            });
        }
      });
  }
  private refreshUserPolicies(): void {
    this.authService.partialRefresh([
      UserPolicyType.GROUP_POLICY,
      UserPolicyType.AUTHORIZABLE_GROUP_POLICY,
    ]).subscribe();
  }
  validateGroupManagementPermission(permissionName: string): Observable<boolean> {
    return this.permissionService.validateSettingsResourcePolicy(SettingsResourceOption.GROUP_MANAGEMENT, permissionName)
  }

  validateGroupPermission(group: Group, permissionName: string): Observable<boolean> {
    return this.permissionService.validateGroupPolicy(group, permissionName);
  }

  validateGroupAuthorizableAny(group: Group): Observable<boolean> {
    return this.permissionService.validateAuthoriableGroupPolicyAny(group);
  }

  editGroup(group: Group): void {
    this.router.navigate(['../edit_group', group.id],
      {
        relativeTo: this.route,
      });
  }

  editGroupAuthorization(group: Group): void {
    this.router.navigate(['../edit_group_authorization', group.id],
      {
        relativeTo: this.route,
      });
  }
}
