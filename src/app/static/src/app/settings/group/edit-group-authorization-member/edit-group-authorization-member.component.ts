import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';


import { MessageService } from '../../../services/message/message.service';
import { GroupService } from '../../../services/group/group.service';
import { GroupPolicyService } from 'src/app/services/group-policy/group-policy.service';
import { Group } from '../../../models/group.model';
import { Member } from '../../../models/member.model';
import { ConfirmDialogComponent } from '../../../utils/dialogs/confirm-dialog/confirm-dialog.component';
import { GeneralSnackbarComponent } from '../../../utils/snackbars/general-snackbar/general-snackbar.component';
import { SettingDataSource } from '../../shared/setting.data-source';

@Component({
  selector: 'app-edit-group-authorization-member',
  templateUrl: './edit-group-authorization-member.component.html',
  styleUrls: ['./edit-group-authorization-member.component.scss']
})
export class EditGroupAuthorizationMemberComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() group: Group;
  @Input() permissionName: string;

  displayedColumns: string[] = ['user_id', 'user_name', 'email', 'reserved', 'actions'];
  membersDataSource = new SettingDataSource<Member>();

  members: Member[] = [];

  constructor(
    private groupService: GroupService,
    private groupPolicyService: GroupPolicyService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.membersDataSource.initData(this.groupPolicyService.getMembers(this.group.id, this.permissionName));
    });

    this.membersDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁使用者數：';
    this.membersDataSource.sort = this.sort;
  }

  addMembers(event: boolean): void {
    this.groupPolicyService.addMembers(this.group.id, this.permissionName, this.members).subscribe(
      data => {
        this.members = [];
        this.membersDataSource.data = data;
        this.messageService.openInfoSnackBar('已經新增使用者至該權限');

      }
    );
  }

  deleteMember(member: Member): void {

    this.groupPolicyService.deleteMember(this.group.id, this.permissionName, member.id)
      .subscribe(data => {
        this.membersDataSource.delete(data);
        this.messageService.openInfoSnackBar('已經刪除 ' + member.user_id);
      });
  }
}
