import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupAuthorizationMemberComponent } from './edit-group-authorization-member.component';

describe('EditGroupAuthorizationMemberComponent', () => {
  let component: EditGroupAuthorizationMemberComponent;
  let fixture: ComponentFixture<EditGroupAuthorizationMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGroupAuthorizationMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGroupAuthorizationMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
