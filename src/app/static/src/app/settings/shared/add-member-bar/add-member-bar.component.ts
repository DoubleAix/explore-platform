import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime, switchMap, filter } from 'rxjs/operators';

import { UserService } from '../../../services/user/user.service';
import { Member } from '../../../models/member.model';

@Component({
  selector: 'app-add-member-bar',
  templateUrl: './add-member-bar.component.html',
  styleUrls: ['./add-member-bar.component.scss']
})
export class AddMemberBarComponent {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  memberCtrl = new FormControl();
  filteredMembers: Observable<Member[]>;
  
  @Input() updatedType: string;
  @Input() members: Member[];
  @Output() addMembersAction = new EventEmitter<boolean>();


  @ViewChild('memberInput', { static: true }) memberInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: true }) matAutocomplete: MatAutocomplete;

  constructor(private userService: UserService) {
    this.filteredMembers = this.memberCtrl
      .valueChanges
      .pipe(
        debounceTime(300),
        switchMap((member: string | null) => member ? this.userService.getUsersFiltered(member) : this.userService.getUsers())
      );
  }

  // add(event): void {
  //   // Add member only when MatAutocomplete is not open
  //   // To make sure this does not conflict with OptionSelected Event
  //   if (!this.matAutocomplete.isOpen) {
  //     const input = event.input;
  //     const value = event.value;

  //     // Add our member
  //     if ((value || '').trim()) {
  //       this.members.push(value.trim());
  //     }

  //     // Reset the input value
  //     if (input) {
  //         input.value = '';
  //       }

  //     this.memberCtrl.setValue(null);
  //   }
  // }

  remove(member: Member): void {
    const index = this.members.indexOf(member);

    if (index >= 0) {
      this.members.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.members.push(event.option.value);
    this.memberInput.nativeElement.value = '';
    this.memberCtrl.setValue(null);
  }
}
