import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGroupBarComponent } from './add-group-bar.component';

describe('AddGroupBarComponent', () => {
  let component: AddGroupBarComponent;
  let fixture: ComponentFixture<AddGroupBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGroupBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGroupBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
