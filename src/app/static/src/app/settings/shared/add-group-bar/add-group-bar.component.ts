import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime, switchMap, filter } from 'rxjs/operators';

import { GroupService } from '../../../services/group/group.service';
import { Group } from '../../../models/group.model';

@Component({
  selector: 'app-add-group-bar',
  templateUrl: './add-group-bar.component.html',
  styleUrls: ['./add-group-bar.component.scss']
})
export class AddGroupBarComponent {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  groupCtrl = new FormControl();
  filteredGroups: Observable<Group[]>;
  
  @Input() updatedType: string;
  @Input() groups: Group[];
  @Output() addGroupsAction = new EventEmitter<boolean>();


  @ViewChild('groupInput', { static: true }) groupInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: true }) matAutocomplete: MatAutocomplete;

  constructor(private userService: GroupService) {
    this.filteredGroups = this.groupCtrl
      .valueChanges
      .pipe(
        debounceTime(300),
        switchMap((group: string | null) => group ? this.userService.getGroupsFiltered(group) : this.userService.getGroups())
      );
  }

  // add(event): void {
  //   // Add group only when MatAutocomplete is not open
  //   // To make sure this does not conflict with OptionSelected Event
  //   if (!this.matAutocomplete.isOpen) {
  //     const input = event.input;
  //     const value = event.value;

  //     // Add our group
  //     if ((value || '').trim()) {
  //       this.groups.push(value.trim());
  //     }

  //     // Reset the input value
  //     if (input) {
  //         input.value = '';
  //       }

  //     this.groupCtrl.setValue(null);
  //   }
  // }

  remove(group: Group): void {
    const index = this.groups.indexOf(group);

    if (index >= 0) {
      this.groups.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.groups.push(event.option.value);
    this.groupInput.nativeElement.value = '';
    this.groupCtrl.setValue(null);
  }

}
