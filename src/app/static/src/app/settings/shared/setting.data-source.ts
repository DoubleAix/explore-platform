import { MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs';
import { take} from 'rxjs/operators';

export interface ModelObject {
    id: number;
}

export class SettingDataSource<T extends ModelObject> extends MatTableDataSource<T> {

    constructor() {
        super();
    }
    initData(data$: Observable<T[]>) {
        data$.subscribe((data: T[]) => {
            this.data = data;
        });
    }

    create(response: T): void {
        this.data.push(response);
        this._updateChangeSubscription();
    }

    update(response: T): void {
        const index: number = this.data.findIndex(d => d.id === response.id);
        this.data[index] = response;
        this._updateChangeSubscription();
    }

    delete(response: T): void {
        const index: number = this.data.findIndex(d => d.id === response.id);
        this.data.splice(index, 1);
        this._updateChangeSubscription();
    }


}