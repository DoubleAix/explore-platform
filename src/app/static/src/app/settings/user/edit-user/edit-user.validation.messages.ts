export const validation_messages = {
    'user_id': [
        { type: 'required', message: '使用者ID是必要欄位' },
        { type: 'maxlength', message: '使用者ID最多只能輸入10個字元' },    
    ],
    'user_name': [
        { type: 'required', message: '使用者名稱是必要欄位' },
        { type: 'maxlength', message: '使用者名稱最多只能輸入20個字元' },
    ],
    'email': [
        { type: 'required', message: '使用者信箱是必要欄位' },
        { type: 'email', message: '使用者信箱不符合信箱命名規則' },
    ],
};

export interface UserValidationMessages {
    user_id?: object[];
    user_name?: object[];
    email?: object[];
};