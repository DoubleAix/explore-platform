import { Component, OnInit, Inject, Injectable } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { validation_messages, UserValidationMessages } from './edit-user.validation.messages';
import { Member } from 'src/app/models/member.model';
import { AddMemberBarComponent } from '../../shared/add-member-bar/add-member-bar.component';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  userForm: FormGroup;
  validation_messages: UserValidationMessages = validation_messages;
  update: boolean;

  constructor(
    private dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) {
    this.update = data.update;
  }

  ngOnInit() {
    this.buildForm();
    if (this.update) {
      this.userForm.patchValue(this.data.user_obj);
      this.userForm.controls['user_id'].disable();
    } else {
      this.userForm.controls['user_name'].disable();
      this.userForm.controls['email'].disable();
    }
  }

  buildForm() {
    this.userForm = this.fb.group({
      user_id: ['', [
        Validators.required,
        Validators.maxLength(10),
       ]
      ],
      user_name: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]
      ],
      email: ['', [
        Validators.required,
        Validators.email
      ]
      ],
    });
  }
  validateByControl(validation, controlName: string| string[]): boolean {
    return this.userForm.get(controlName).hasError(validation.type) && (this.userForm.get(controlName).dirty || this.userForm.get(controlName).touched)
  }

  public closeDialog() {
    this.dialogRef.close();
  }
  public confirm() {
    if (this.update) {
      this.userForm.controls['user_id'].enable();
    } else {
      this.userForm.controls['user_name'].enable();
      this.userForm.controls['email'].enable();
    }
    this.dialogRef.close(this.userForm.value);
  }
}
