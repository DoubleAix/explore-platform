import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Observable } from 'rxjs';


import { MessageService } from '../../../services/message/message.service';
import { UserService } from '../../../services/user/user.service';
import { Member } from '../../../models/member.model';

import { ConfirmDialogComponent } from '../../../utils/dialogs/confirm-dialog/confirm-dialog.component';
import { GeneralSnackbarComponent } from '../../../utils/snackbars/general-snackbar/general-snackbar.component';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { PermissionService } from '../../../services/permission/permission.service';
// import { PermissionOption } from '../../../enums/permission-option.enum';
import { SettingsResourceOption } from '../../../enums/settings-resource-option.enum';
import { SettingDataSource } from '../../shared/setting.data-source';
import { AuthService } from '../../../services/auth/auth.service';
import { UserPolicyType } from '../../../enums/user-policy-type.enum';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
})
export class ListUserComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'user_id', 'user_name', 'email', 'reserved', 'actions'];
  usersDataSource = new SettingDataSource<Member>();

  applyFilter(filterValue: string) {
    this.usersDataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private userService: UserService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
    private permissionService: PermissionService,
    private authService: AuthService,
  ) { }


  ngOnInit() {
    this.usersDataSource.initData(this.userService.getUsers());
    this.usersDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁使用者數：';
    this.usersDataSource.sort = this.sort;
    this.refreshUserPolicies();
  }

  createUser(): void {
    const dialogRef = this.dialog.open(
      EditUserComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          update: false,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.userService.createUser(data)
            .subscribe(
              (response: Member) => {
                this.usersDataSource.create(response);
                this.messageService.openInfoSnackBar('已經成功建立 ' + response.user_name);
              },
              error => {
                this.messageService.openErrorSnackBar('使用者ID ' + data.user_id + ' 已經存在');
              });
        }
      },
    );
  }

  deleteUser(user: Member): void {
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent,
      {
        autoFocus: false,
        data: {
          type: '使用者',
          id: user.id,
          name: user.user_name,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.userService.deleteUser(user.id)
            .subscribe(response => {
              this.usersDataSource.delete(response);
              this.messageService.openInfoSnackBar('已經刪除 ' + user.user_name);
            });
        }
      });
  }

  editUser(user: Member): void {
    const dialogRef = this.dialog.open(
      EditUserComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          update: true,
          user_obj: user,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.userService.updateUser(data, user.id)
            .subscribe(
              (response: Member) => {
                this.usersDataSource.update(response);
                this.messageService.openInfoSnackBar('已經成功更新 ' + data.user_name);
              },
              error => {
                this.messageService.openErrorSnackBar('使用者ID ' + data.user_id + ' 不存在');
              });
        }
      },
    );
  }
  private refreshUserPolicies(): void {
    this.authService.partialRefresh([
      UserPolicyType.RESOURCE_POLICY,
    ]).subscribe();
  }

  validateUserManagementPermission(permissionName: string): Observable<boolean> {
    return this.permissionService.validateSettingsResourcePolicy(SettingsResourceOption.USER_MANAGEMENT, permissionName)
  }

}