import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { validation_messages, SpaceValidationMessages } from './edit-space.validation.messages';
import { themes } from '../../../core/toggle-theme-button/togge-theme-button.helper';
import {iconList} from '../../space/space.icons'

@Component({
  selector: 'app-edit-space',
  templateUrl: './edit-space.component.html',
  styleUrls: ['./edit-space.component.scss']
})
export class EditSpaceComponent implements OnInit {
  spaceForm: FormGroup;
  validation_messages: SpaceValidationMessages = validation_messages;
  update: string;
  iconList: string[] = iconList;
  themes: any = themes;

  constructor(
    private dialogRef: MatDialogRef<EditSpaceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) {
    this.update = data.update;
  }

  ngOnInit() {
    this.buildForm();
    if (this.update) {
      this.spaceForm.patchValue(this.data.space_obj);
      this.spaceForm.controls['space_id'].disable();
      this.spaceForm.controls['space_name'].disable();
    }
  }

  buildForm() {
    this.spaceForm = this.fb.group({
      space_id: ['', [
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern('[A-Z]*')
      ]
      ],
      space_name: ['', [
        Validators.required,
        Validators.maxLength(10)
      ]
      ],
      icon_name: ['', [
        Validators.required,
        // Validators.maxLength(10)
      ]
      ],
      theme_class: ['', [
        Validators.required,
        // Validators.maxLength(20)
      ]
      ],
    });
  }
  validateByControl(validation, controlName: string| string[]): boolean {
    return this.spaceForm.get(controlName).hasError(validation.type) && (this.spaceForm.get(controlName).dirty || this.spaceForm.get(controlName).touched)
  }

  public closeDialog() {
    this.dialogRef.close();
  }
  public confirm() {
    this.spaceForm.controls['space_id'].enable();
    this.spaceForm.controls['space_name'].enable();
    this.dialogRef.close(this.spaceForm.value);
  }
}
