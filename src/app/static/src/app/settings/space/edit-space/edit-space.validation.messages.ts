export const validation_messages = {
    'space_id':[
        { type: 'required', message: '空間ID是必要欄位' },
        { type: 'maxlength', message: '空間ID最多只能輸入10個字元' },
        { type: 'pattern', message: '空間ID只能為大寫英文字母' },
    ],
    'space_name': [
        { type: 'required', message: '空間名稱是必要欄位' },
        { type: 'maxlength', message: '空間名稱最多只能輸入10個字元' },
    ],
    'icon_name': [
        { type: 'required', message: 'Icon 樣式是必要欄位' },
        // { type: 'maxlength', message: '空間名稱最多只能輸入10個字元' },
    ],
    'theme_class': [
        { type: 'required', message: '背景主題是必要欄位' },
        // { type: 'maxlength', message: '背景主題最多只能輸入20個字元' },
    ],   
};

export interface SpaceValidationMessages {
    space_id?: object[];
    space_name?: object[];
    theme_class?: object[];
    icon_name?: object[];
};