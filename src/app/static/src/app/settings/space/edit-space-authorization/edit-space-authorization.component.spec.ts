import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpaceAuthorizationComponent } from './edit-space-authorization.component';

describe('EditSpaceAuthorizationComponent', () => {
  let component: EditSpaceAuthorizationComponent;
  let fixture: ComponentFixture<EditSpaceAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSpaceAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpaceAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
