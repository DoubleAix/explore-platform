import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { take, filter } from 'rxjs/operators'
import { BehaviorSubject, Observable, of } from 'rxjs';

import { ConfigService } from '../../../services/config/config.service';
// import { ParentResourceService } from '../../../services/parent-resource/parent-resource.service';
import { SpaceService } from "../../../services/space/space.service";
// import { ParentResource, ChildResource } from '../../../models/resource.model';
import { Space } from "../../../models/space.model";
import { Permission } from '../../../models/permission.model';
import { PermissionService } from 'src/app/services/permission/permission.service';


@Component({
  selector: 'app-edit-space-authorization',
  templateUrl: './edit-space-authorization.component.html',
  styleUrls: ['./edit-space-authorization.component.scss']
})
export class EditSpaceAuthorizationComponent implements OnInit {
  space = new BehaviorSubject<Space>(null);
  spacePermission: Permission[];


  constructor(
    private spaceService: SpaceService,
    private router: Router,
    private route: ActivatedRoute,
    private configService: ConfigService,
    private permissionService: PermissionService,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.spaceService.getSpaceById(params['id']).subscribe(
          data => {
            this.space.next(data);
            this.configService.getAuthorizationOption(data.authorization_type_id).subscribe(
              data => {
                this.spacePermission = data;
              },
              error => console.log(error),
            );
          }
        );

      });
  }
  ngOnDestroy() {
    this.space.complete();
  }
  validateSpaceAuthorizable(permission: string): Observable<boolean> {
    return this.permissionService.validateAuthorizableSpacePolicy(this.space.value, permission);
  }

  returnSpaceManagement(): void {
    this.router.navigate(['../../list_space'], { relativeTo: this.route });
  }
}
