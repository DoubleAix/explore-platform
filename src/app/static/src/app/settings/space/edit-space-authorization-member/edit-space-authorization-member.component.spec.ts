import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpaceAuthorizationMemberComponent } from './edit-space-authorization-member.component';

describe('EditSpaceAuthorizationMemberComponent', () => {
  let component: EditSpaceAuthorizationMemberComponent;
  let fixture: ComponentFixture<EditSpaceAuthorizationMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSpaceAuthorizationMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpaceAuthorizationMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
