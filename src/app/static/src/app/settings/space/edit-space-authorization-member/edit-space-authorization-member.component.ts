import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';

import { MessageService } from '../../../services/message/message.service';
import { ResourcePolicyService } from '../../../services/resource-policy/resource-policy.service';
import { SpacePolicyService } from '../../../services/space-policy/space-policy.service'
import { Member } from '../../../models/member.model';
import { Group } from '../../../models/group.model';
import { ParentResource, ChildResource } from '../../../models/resource.model';

import { SettingDataSource } from '../../shared/setting.data-source';
import { Space } from 'src/app/models/space.model';
import { UserType } from 'src/app/enums/user-type.enum';

@Component({
  selector: 'app-edit-space-authorization-member',
  templateUrl: './edit-space-authorization-member.component.html',
  styleUrls: ['./edit-space-authorization-member.component.scss']
})
export class EditSpaceAuthorizationMemberComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() space: Space;
  @Input() permissionName: string;
  userType = UserType.USER;
  displayedColumns: string[] = ['user_id', 'user_name', 'email', 'reserved', 'actions'];
  membersDataSource = new SettingDataSource<Member | Group>();


  members: Member[] = [];

  constructor(
    private spacePolicyService: SpacePolicyService,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.membersDataSource.initData(this.spacePolicyService.getMembers(this.space.id, this.permissionName, this.userType));
    });

    this.membersDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁使用者數：';
    this.membersDataSource.sort = this.sort;
  }

  addMembers(event: boolean): void {
    this.spacePolicyService.addMembers(this.space.id, this.permissionName, this.members, this.userType).subscribe(
      (data: Member[]) => {
        this.members = [];
        this.membersDataSource.data = data;
        this.messageService.openInfoSnackBar('已經新增使用者至該權限');
      }
    );
  }

  deleteMember(member: Member): void {

    this.spacePolicyService.deleteMember(this.space.id, this.permissionName, member.id, this.userType)
      .subscribe(
        (data: Member) => {
          this.membersDataSource.delete(data);
          this.messageService.openInfoSnackBar('已經刪除 ' + member.user_id);
        });
  }


}
