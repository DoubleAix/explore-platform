import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpaceAuthorizationGroupComponent } from './edit-space-authorization-group.component';

describe('EditSpaceAuthorizationGroupComponent', () => {
  let component: EditSpaceAuthorizationGroupComponent;
  let fixture: ComponentFixture<EditSpaceAuthorizationGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSpaceAuthorizationGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpaceAuthorizationGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
