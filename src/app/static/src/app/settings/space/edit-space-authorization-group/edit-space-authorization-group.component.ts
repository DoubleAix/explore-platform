import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';

import { MessageService } from '../../../services/message/message.service';
import { ResourcePolicyService } from '../../../services/resource-policy/resource-policy.service';
import { SpacePolicyService } from "../../../services/space-policy/space-policy.service";
import { Group } from '../../../models/group.model';
import { Member } from '../../../models/member.model';
import { ParentResource, ChildResource } from '../../../models/resource.model';
import { SettingDataSource } from '../../shared/setting.data-source';
import { Space } from 'src/app/models/space.model';
import { UserType } from "src/app/enums/user-type.enum";

@Component({
  selector: 'app-edit-space-authorization-group',
  templateUrl: './edit-space-authorization-group.component.html',
  styleUrls: ['./edit-space-authorization-group.component.scss']
})
export class EditSpaceAuthorizationGroupComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() space: Space;
  @Input() permissionName: string;

  userType = UserType.GROUP;
  displayedColumns: string[] = ['id', 'group_id', 'group_name', 'reserved', 'actions'];
  groupsDataSource = new SettingDataSource<Group | Member>();

  groups: Group[] = [];

  constructor(
    private spacePolicyService: SpacePolicyService,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.groupsDataSource.initData(this.spacePolicyService.getMembers(this.space.id, this.permissionName, this.userType));
    });

    this.groupsDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁群組數：';
    this.groupsDataSource.sort = this.sort;
  }

  addMembers(event: boolean): void {
    this.spacePolicyService.addMembers(this.space.id, this.permissionName, this.groups, this.userType).subscribe(
      (data: Group[]) => {
        this.groups = [];
        this.groupsDataSource.data = data;
        this.messageService.openInfoSnackBar('已經新增群組至該權限');
      }
    );
  }

  deleteMember(group: Group): void {

    this.spacePolicyService.deleteMember(this.space.id, this.permissionName, group.id, this.userType)
      .subscribe(
        (data: Group) => {
          this.groupsDataSource.delete(data);        
          this.messageService.openInfoSnackBar('已經刪除 ' + group.group_id);
        });
  }

}
