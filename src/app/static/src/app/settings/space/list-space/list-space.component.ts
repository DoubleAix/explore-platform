import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSort, MatPaginator, MatPaginatorIntl, MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Observable } from 'rxjs';


import { MessageService } from '../../../services/message/message.service';
// import { UserService } from '../../../services/user/user.service';
import { SpaceService } from '../../../services/space/space.service';
import { Member } from '../../../models/member.model';

import { ConfirmDialogComponent } from '../../../utils/dialogs/confirm-dialog/confirm-dialog.component';
import { GeneralSnackbarComponent } from '../../../utils/snackbars/general-snackbar/general-snackbar.component';
import { EditSpaceComponent } from '../edit-space/edit-space.component';
import { PermissionService } from '../../../services/permission/permission.service';
// import { PermissionOption } from '../../../enums/permission-option.enum';
import { SettingsResourceOption } from '../../../enums/settings-resource-option.enum';
import { SettingDataSource } from '../../shared/setting.data-source';
import { AuthService } from '../../../services/auth/auth.service';
import { UserPolicyType } from '../../../enums/user-policy-type.enum';
import { Space } from 'src/app/models/space.model';

@Component({
  selector: 'app-list-space',
  templateUrl: './list-space.component.html',
  styleUrls: ['./list-space.component.scss']
})
export class ListSpaceComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'space_id', 'space_name', 'icon_name', 'theme_class', 'reserved', 'actions'];
  spacesDataSource = new SettingDataSource<Space>();

  applyFilter(filterValue: string) {
    this.spacesDataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private spaceService: SpaceService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private matPaginatorIntl: MatPaginatorIntl,
    private messageService: MessageService,
    private permissionService: PermissionService,
    private authService: AuthService,
  ) { }


  ngOnInit() {
    this.spacesDataSource.initData(this.spaceService.getSpaces());
    this.spacesDataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = '每頁空間數：';
    this.spacesDataSource.sort = this.sort;
    this.refreshUserPolicies();
  }

  createSpace(): void {
    const dialogRef = this.dialog.open(
      EditSpaceComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          update: false,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.spaceService.createSpace(data)
            .subscribe(
              (response: Space) => {
                this.spacesDataSource.create(response);
                this.messageService.openInfoSnackBar('已經成功建立 ' + data.space_name);
                this.refreshUserPolicies();
              },
              error => {
                this.messageService.openErrorSnackBar('空間 ' + data.space_name + ' 已經存在');
              });
        }
      },
    );
  }

  deleteSpace(space: Space): void {
    const dialogRef = this.dialog.open(
      ConfirmDialogComponent,
      {
        autoFocus: false,
        data: {
          type: '空間',
          id: space.id,
          name: space.space_name,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          this.spaceService.deleteSpace(space.id)
            .subscribe(response => {
              this.spacesDataSource.delete(response);
              this.messageService.openInfoSnackBar('已經刪除 ' + space.space_name);
            });
        }
      });
  }

  editSpace(space: Space): void {
    const dialogRef = this.dialog.open(
      EditSpaceComponent,
      {
        autoFocus: false,
        minWidth: '400px',
        data: {
          update: true,
          space_obj: space,
        }
      }
    );
    dialogRef.afterClosed().subscribe(
      data => {
        if (data) {
          // console.log(data);
          this.spaceService.updateSpace(data, space.id)
            .subscribe(
              (response: Space) => {
                this.spacesDataSource.update(response);
                this.messageService.openInfoSnackBar('已經成功更新 ' + data.space_name);
              },
              error => {
                this.messageService.openErrorSnackBar('空間 ' + data.space_name + ' 不存在');
              });
        }
      },
    );
  }
  private refreshUserPolicies(): void {
    this.authService.partialRefresh([
      UserPolicyType.SPACE_POLICY,
      UserPolicyType.AUTHORIZABLE_SPACE_POLICY
    ]).subscribe();
  }


  validateSpaceManagementPermission(permissionName: string): Observable<boolean> {
    return this.permissionService.validateSettingsResourcePolicy(SettingsResourceOption.SPACE_MANAGEMENT, permissionName);
  }

  validateSpacePermission(space: Space, permissionName: string): Observable<boolean> {
    return this.permissionService.validateSpacePolicy(space, permissionName);
  }

  validateSpaceAuthorizableAny(space: Space): Observable<boolean> {
    return this.permissionService.validateAuthoriableSpacePolicyAny(space);
  }

  editSpaceAuthorization(space: Space): void {
    this.router.navigate(['../edit_space_authorization', space.id],
      {
        relativeTo: this.route,
      });
  }
  
  editSpaceResource(space: Space): void {
    this.router.navigate(['../list_space_resource', space.id],
      {
        relativeTo: this.route,
      });
  }
}
