import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSpaceResourceComponent } from './list-space-resource.component';

describe('ListSpaceResourceComponent', () => {
  let component: ListSpaceResourceComponent;
  let fixture: ComponentFixture<ListSpaceResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSpaceResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSpaceResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
