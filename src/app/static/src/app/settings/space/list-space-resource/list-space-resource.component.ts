import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { filter, take } from "rxjs/operators";
import { SpaceService } from '../../../services/space/space.service';
import { PermissionService } from "src/app/services/permission/permission.service";
import { SpaceResourceService } from '../../../services/space-resource/space-resource.service';
import { MessageService } from '../../../services/message/message.service';
import { Space } from '../../../models/space.model';
import { SpaceResource } from '../../../models/space-resource.model';
import { SettingsResourceOption } from 'src/app/enums/settings-resource-option.enum';


@Component({
  selector: 'app-list-space-resource',
  templateUrl: './list-space-resource.component.html',
  styleUrls: ['./list-space-resource.component.scss']
})
export class ListSpaceResourceComponent implements OnInit {
  space = new BehaviorSubject<Space>(null);
  embeddedResourceList: SpaceResource[];
  embeddableResourceList: SpaceResource[];

  constructor(
    private spaceService: SpaceService,
    private spaceResourceService: SpaceResourceService,
    private messageService: MessageService,
    private permissionService: PermissionService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(
      params => {
        this.spaceService.getSpaceById(params['id']).subscribe(
          data => {
            this.space.next(data);
          }
        );
        this.spaceResourceService.getSpaceResources(params['id']).subscribe(
          data => {
            this.embeddedResourceList = data.embedded;
            this.embeddableResourceList = data.embeddable;
          }
        );
      });

  }

  validateSpaceManagementPermission(permissionName: string): Observable<boolean> {
    return this.permissionService.validateSettingsResourcePolicy(SettingsResourceOption.SPACE_MANAGEMENT, permissionName);
  }
  updateSpaceResources(): void {
    this.spaceResourceService.updateSpaceResources(this.embeddedResourceList, this.space.value.id).subscribe(
      data => {
        this.embeddedResourceList = data.embedded;
        this.embeddableResourceList = data.embeddable;
        this.messageService.openInfoSnackBar('已經更新資源至空間已鑲嵌清單');
      }
    )
  }

  validateSpacePermission(permissionName: string): Observable<boolean> {
    let validateResult: Observable<boolean>;
    this.space.pipe(
      filter(data => data != null),
      take(1),
    ).subscribe(
      data => {
        try {
          validateResult = this.permissionService.validateSpacePolicy(data, permissionName);
        } catch (e) {
          console.log(e);
        }
      }
    );

    return validateResult;

  }



  drop(event: CdkDragDrop<SpaceResource[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }



  returnSpaceManagement(): void {
    this.router.navigate(['../../list_space'], { relativeTo: this.route });
  }
}
