import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUserComponent } from './user/list-user/list-user.component';
import { ListGroupComponent } from './group/list-group/list-group.component';
import { EditGroupComponent } from './group/edit-group/edit-group.component';
import { ListParentResourceComponent } from './resource/list-parent-resource/list-parent-resource.component';

import {
  EditParentResourceAuthorizationComponent,
} from './resource/edit-parent-resource-authorization/edit-parent-resource-authorization.component';
import { EditChildResourceAuthorizationComponent } from './resource/edit-child-resource-authorization/edit-child-resource-authorization.component';
import { EditGroupAuthorizationComponent } from './group/edit-group-authorization/edit-group-authorization.component';
import { ListChildResourceComponent } from './resource/list-child-resource/list-child-resource.component';
import { ListSpaceComponent } from './space/list-space/list-space.component';
import { EditSpaceAuthorizationComponent } from "./space/edit-space-authorization/edit-space-authorization.component";
import { ListSpaceResourceComponent } from './space/list-space-resource/list-space-resource.component';
import { SettingsRouteGuard } from '../utils/guards/settings-route.guard';
import { PermissionOption } from '../enums/permission-option.enum';
import { SettingsResourceOption } from '../enums/settings-resource-option.enum';

import { TestResolveService } from "src/app/utils/resolves/test-resolve/test-resolve.service";
const routes: Routes = [
  {
    path: 'user_management',
    canActivateChild: [SettingsRouteGuard],
    children: [
      {
        path: 'list_user',
        component: ListUserComponent,
        // resolve: { space: TestResolveService },

        data: {
          dashboard_title: '使用者管理',
          resource_name: SettingsResourceOption.USER_MANAGEMENT,
          permission_name: PermissionOption.READ,
        }
      },
      { path: '**', redirectTo: 'list_user' },
    ],
  },
  {
    path: 'group_management',
    canActivateChild: [SettingsRouteGuard],
    children: [
      {
        path: 'list_group',
        component: ListGroupComponent,
        data: {
          dashboard_title: '群組管理',
          resource_name: SettingsResourceOption.GROUP_MANAGEMENT,
          permission_name: PermissionOption.READ,

        }
      },
      {
        path: 'edit_group_authorization/:id',
        component: EditGroupAuthorizationComponent,
        data: {
          dashboard_title: '群組 - 編輯使用者權限',
          resource_name: SettingsResourceOption.GROUP_MANAGEMENT,
          permission_name: PermissionOption.READ,
        }
      },
      {
        path: 'edit_group/:id',
        component: EditGroupComponent,
        data: {
          dashboard_title: '編輯群組',
          resource_name: SettingsResourceOption.GROUP_MANAGEMENT,
          permission_name: PermissionOption.READ,
        }
      },
      { path: '**', redirectTo: 'list_group' },
    ],
  },

  {
    path: 'resource_management',
    canActivateChild: [SettingsRouteGuard],
    children: [
      {
        path: 'list_parent_resource',
        component: ListParentResourceComponent,

        data: {
          dashboard_title: '主要資源管理',
          resource_name: SettingsResourceOption.RESOURCE_MANAGEMENT,
          permission_name: PermissionOption.READ,
        }
      },
      {
        path: 'edit_parent_resource_authorization/:id',
        component: EditParentResourceAuthorizationComponent,

        data: {
          dashboard_title: '主要資源 - 編輯使用者權限',
          resource_name: SettingsResourceOption.RESOURCE_MANAGEMENT,
          permission_name: PermissionOption.READ,

        }
      },
      {
        path: 'list_child_resource/:id',
        component: ListChildResourceComponent,

        data: {
          dashboard_title: '主要資源 - 編輯子資源清單',
          resource_name: SettingsResourceOption.RESOURCE_MANAGEMENT,
          permission_name: PermissionOption.READ,

        }
      },
      {
        path: 'edit_child_resource_authorization/:id',
        component: EditChildResourceAuthorizationComponent,
        data: {
          dashboard_title: '子資源 - 編輯使用者權限',
          resource_name: SettingsResourceOption.RESOURCE_MANAGEMENT,
          permission_name: PermissionOption.READ,
        }
      },
      { path: '**', redirectTo: 'list_parent_resource' },
    ],
  },
  {
    path: 'space_management',
    canActivateChild: [SettingsRouteGuard],
    children: [
      {
        path: 'list_space',
        component: ListSpaceComponent,

        data: {
          dashboard_title: '空間管理',
          resource_name: SettingsResourceOption.SPACE_MANAGEMENT,
          permission_name: PermissionOption.READ,
        }
      },
      {
        path: 'edit_space_authorization/:id',
        component: EditSpaceAuthorizationComponent,

        data: {
          dashboard_title: '空間 - 編輯使用者權限',
          resource_name: SettingsResourceOption.SPACE_MANAGEMENT,
          permission_name: PermissionOption.READ,

        }
      },
      {
        path: 'list_space_resource/:id',
        component: ListSpaceResourceComponent,

        data: {
          dashboard_title: '空間 - 編輯主要資源清單',
          resource_name: SettingsResourceOption.SPACE_MANAGEMENT,
          permission_name: PermissionOption.READ,

        }
      },
      { path: '**', redirectTo: 'list_space' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
