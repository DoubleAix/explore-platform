import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { SettingsRoutingModule } from './settings-routing.module';
import { UtilsModule } from '../utils/utils.module';

import { ListGroupComponent } from './group/list-group/list-group.component';
import { EditGroupComponent } from './group/edit-group/edit-group.component';
import { ConfirmDialogComponent } from '../utils/dialogs/confirm-dialog/confirm-dialog.component';
import { AddGroupComponent } from './group/add-group/add-group.component';
import { ListParentResourceComponent } from './resource/list-parent-resource/list-parent-resource.component';
import { AddParentResourceComponent } from './resource/add-parent-resource/add-parent-resource.component';
import {
  EditParentResourceAuthorizationComponent
} from './resource/edit-parent-resource-authorization/edit-parent-resource-authorization.component';

import { AddMemberBarComponent } from './shared/add-member-bar/add-member-bar.component';
import { AddGroupBarComponent } from './shared/add-group-bar/add-group-bar.component';
import {
  EditResourceAuthorizationMemberComponent
} from './resource/edit-resource-authorization-member/edit-resource-authorization-member.component';
import {
  EditResourceAuthorizationGroupComponent
} from './resource/edit-resource-authorization-group/edit-resource-authorization-group.component';
import { ListUserComponent } from './user/list-user/list-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { EditGroupAuthorizationComponent } from './group/edit-group-authorization/edit-group-authorization.component';
import { EditGroupAuthorizationMemberComponent } from './group/edit-group-authorization-member/edit-group-authorization-member.component';
import { ListChildResourceComponent } from './resource/list-child-resource/list-child-resource.component';
import {
  EditChildResourceAuthorizationComponent
} from './resource/edit-child-resource-authorization/edit-child-resource-authorization.component';
import { AddChildResourceComponent } from './resource/add-child-resource/add-child-resource.component';
import { ListSpaceComponent } from './space/list-space/list-space.component';
import { EditSpaceComponent } from './space/edit-space/edit-space.component';
import { ListSpaceResourceComponent } from './space/list-space-resource/list-space-resource.component';
import { EditSpaceAuthorizationComponent } from './space/edit-space-authorization/edit-space-authorization.component';
import { EditSpaceAuthorizationMemberComponent } from './space/edit-space-authorization-member/edit-space-authorization-member.component';
import { EditSpaceAuthorizationGroupComponent } from './space/edit-space-authorization-group/edit-space-authorization-group.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};



@NgModule({

  imports: [
    CommonModule,
    FlexLayoutModule,
    SettingsRoutingModule,
    PerfectScrollbarModule,
    UtilsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    MatToolbarModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatSnackBarModule,
    MatChipsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatTabsModule,
    MatPaginatorModule,
    MatListModule,
    DragDropModule,
    MatSliderModule,

  ],
  declarations: [
    ListGroupComponent,
    EditGroupComponent,
    AddGroupComponent,
    ListParentResourceComponent,
    AddParentResourceComponent,
    EditParentResourceAuthorizationComponent,
    AddMemberBarComponent,
    AddGroupBarComponent,
    EditResourceAuthorizationMemberComponent,
    EditResourceAuthorizationGroupComponent,
    ListUserComponent,
    EditUserComponent,
    EditGroupAuthorizationComponent,
    EditGroupAuthorizationMemberComponent,
    ListChildResourceComponent,
    EditChildResourceAuthorizationComponent,
    AddChildResourceComponent,
    ListSpaceComponent,
    EditSpaceComponent,
    ListSpaceResourceComponent,
    EditSpaceAuthorizationComponent,
    EditSpaceAuthorizationMemberComponent,
    EditSpaceAuthorizationGroupComponent,


  ],
  entryComponents: [
    ConfirmDialogComponent,
    AddGroupComponent,
    AddParentResourceComponent,
    AddChildResourceComponent,
    EditUserComponent,
    EditSpaceComponent,
    // GeneralSnackbarComponent,
  ],
})
export class SettingsModule { }
