import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TokenRequiredGuard } from './utils/guards/token-required.guard';
import { TokenExistGuard } from './utils/guards/token-exist.guard';
import { SpaceResolveService } from 'src/app/utils/resolves/space-resolve/space-resolve.service';
import { MenuResolveService } from 'src/app/utils/resolves/menu-resolve/menu-resolve.service';
import { SpaceGuard } from 'src/app/utils/guards/space.guard';
const routes: Routes = [
    {
        path: 'space',
        loadChildren: () => import('./space-page/space-page.module').then(m => m.SpacePageModule),
        canActivate: [TokenRequiredGuard],
    },
    {
        path: 'dashboard/:spaceId',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [TokenRequiredGuard, SpaceGuard],
        resolve: {
            space: SpaceResolveService,
            menu: MenuResolveService,
        },
        // runGuardsAndResolvers: 'always',
    },
    {
        path: 'login',
        loadChildren: () => import('./login-page/login-page.module').then(m => m.LoginPageModule),
        canActivate: [TokenExistGuard],
    },
    {
        path: '**',
        redirectTo: 'space'
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        useHash: true,
        // onSameUrlNavigation: 'reload',
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }