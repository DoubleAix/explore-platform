import { MatPaginatorIntl } from '@angular/material';

export class CustomPaginatorConfig extends MatPaginatorIntl {
  constructor() {
    super();
    this.nextPageLabel = '下一頁';
    this.previousPageLabel = '上一頁';
    this.lastPageLabel = '最後一頁';
    this.firstPageLabel = '第一頁';
  }
}