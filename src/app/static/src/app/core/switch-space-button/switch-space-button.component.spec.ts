import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchSpaceButtonComponent } from './switch-space-button.component';

describe('SwitchSpaceButtonComponent', () => {
  let component: SwitchSpaceButtonComponent;
  let fixture: ComponentFixture<SwitchSpaceButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitchSpaceButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchSpaceButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
