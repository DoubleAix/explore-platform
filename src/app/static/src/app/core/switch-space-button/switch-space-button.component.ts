import { Component, OnInit, Input } from '@angular/core';
import { ConfigService } from 'src/app/services/config/config.service';
import { Observable, of, combineLatest } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Space } from 'src/app/models/space.model';
import { PermissionService } from 'src/app/services/permission/permission.service';
import { filter, map } from 'rxjs/operators';
import { SpaceGuard } from 'src/app/utils/guards/space.guard';

@Component({
  selector: 'cdk-switch-space-button',
  templateUrl: './switch-space-button.component.html',
  styleUrls: ['./switch-space-button.component.scss']
})
export class SwitchSpaceButtonComponent implements OnInit {
  spaceList: Observable<Space[]>;
  @Input() space$: Observable<Space>;

  constructor(
    private configService: ConfigService,
    private permissionService: PermissionService,
    private router: Router,
  ) { }


  ngOnInit() {
    // this.spaceList = this.configService.spaceMapping$.pipe(
    //   map(data => data.filter(data => data.id !== this.space.id)),
    // );
    this.spaceList = combineLatest(this.configService.spaceMapping$, this.space$).pipe(
      map(([spaceMapping, space]) => spaceMapping.filter(data => data.id !== space.id))
    );

    // this.spaceList = this.configService.spaceMapping$.pipe(
    //   map(data => data.filter(data => data.id !== this.space.id)),
    // );

  }

  validateSpacePermission(space: Space, permissionName: string): Observable<boolean> {
    // console.log(space);
    return this.permissionService.validateSpacePolicy(space, permissionName);
  }

  accessSpace(spaceId: string): void {
    this.router.navigate([`/dashboard/${spaceId}`]);
  }

}
