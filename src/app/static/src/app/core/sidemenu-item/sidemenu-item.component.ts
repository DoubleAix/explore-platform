import { Component, OnInit, Input } from '@angular/core';
import { PermissionService } from "../../services/permission/permission.service";
import { Observable, of } from "rxjs";
import { ParentMenu } from 'src/app/models/menu.model';

@Component({
    selector: 'cdk-sidemenu-item',
    templateUrl: './sidemenu-item.component.html',
    styleUrls: ['./sidemenu-item.component.scss'],
})
export class SidemenuItemComponent implements OnInit {

    @Input() menu: ParentMenu;
    @Input() iconOnly: boolean;
    @Input() secondaryMenu = false;

    constructor(
        private permissionService: PermissionService,
    ) { }

    ngOnInit() {
    }

    // openLink() {
    //     this.menu.open = this.menu.open;
    // }

    chechForChildMenu() {
        return (this.menu && this.menu.sub) ? true : false;
    }

    validateChildMenuItem(resourcePolicyId: number): Observable<boolean> {
        return this.permissionService.validateResourcePolicyById(resourcePolicyId)
    }

}
