import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTabsModule } from '@angular/material';
import { MatDividerModule } from '@angular/material/divider';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import {
    MatSidenavModule,
    MatSliderModule,
    MatProgressBarModule,
} from '@angular/material';

import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { SidemenuItemComponent } from './sidemenu-item/sidemenu-item.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FullscreenComponent } from './fullscreen/fullscreen.component';
import { DemoButtrnComponent } from './demo-buttrn/demo-buttrn.component';
import { FeedbackMenuComponent } from './feedback-menu/feedback-menu.component';
import { UserDropDownComponent } from './user-drop-down/user-drop-down.component';
import { ToggleThemeButtonComponent } from './toggle-theme-button/toggle-theme-button.component';
import { SwitchSpaceButtonComponent } from './switch-space-button/switch-space-button.component';




const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({

    declarations: [
        SidemenuComponent,
        SidemenuItemComponent,
        ToolbarComponent,
        FullscreenComponent,
        DemoButtrnComponent,
        FeedbackMenuComponent,
        UserDropDownComponent,
        ToggleThemeButtonComponent,
        SwitchSpaceButtonComponent,
       
    ],

    imports: [
        CommonModule,
        MatListModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatChipsModule,
        RouterModule,
        PerfectScrollbarModule,
        FlexLayoutModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatSidenavModule,
        MatTabsModule,
        MatSliderModule,
        MatProgressBarModule,
        MatDividerModule,
        MatRippleModule,
        MatTooltipModule,
        MatMenuModule,
    ],


    exports: [
        SidemenuComponent,
        SidemenuItemComponent,
        ToolbarComponent,
        FullscreenComponent,
        FeedbackMenuComponent,
        UserDropDownComponent,
        SwitchSpaceButtonComponent,
        
    ],

    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class CoreModule { }
