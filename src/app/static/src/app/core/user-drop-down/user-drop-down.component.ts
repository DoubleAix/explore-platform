import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service'
import { Router } from "@angular/router";
import { Observable } from 'rxjs';
import { Member } from 'src/app/models/member.model';
@Component({
  selector: 'cdk-user-drop-down',
  templateUrl: './user-drop-down.component.html',
  styleUrls: ['./user-drop-down.component.scss']
})
export class UserDropDownComponent implements OnInit {
  userInfo: Observable<Member>;
  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.userInfo = this.authService.userInfo$
  }

  help() {
    console.log('Designed By Aix - Only Excellence Matters. \nEmail: aix.chen@fubon.com \nLINE ID: cyes850407');
  }


  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
