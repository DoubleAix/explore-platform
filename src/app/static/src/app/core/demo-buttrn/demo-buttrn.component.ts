import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';

@Component({
  selector: 'cdk-demo-buttrn',
  templateUrl: './demo-buttrn.component.html',
  styleUrls: ['./demo-buttrn.component.scss']
})
export class DemoButtrnComponent implements OnInit {

  isDemo: boolean = false;
  interval = timer(5000, 5000)
  subscription
  urls = [
    '/dashboard/tests/httpclient',
    '/dashboard/tests/flexlayout',
    '/dashboard/tables/fixed',
    '/dashboard/tables/featured',
    '/dashboard/tables/responsive',
    '/dashboard/tables/showdetail',
    '/dashboard/tables/general',
  ];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  toggleDemo() {
    if (this.isDemo) {
      this.stopDemo();
      this.isDemo = false;
    } else {
      this.startDemo();
      this.isDemo = true;
    }
  }

  startDemo() {
    this.subscription = this.interval.subscribe(_ => this.demo())
  }
  stopDemo() {
    this.subscription.unsubscribe();
  }

  demo() {
    this.router.navigateByUrl(this.urls[Math.floor(Math.random() * this.urls.length)])
  }
}
