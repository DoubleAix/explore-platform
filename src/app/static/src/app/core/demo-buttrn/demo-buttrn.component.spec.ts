import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoButtrnComponent } from './demo-buttrn.component';

describe('DemoButtrnComponent', () => {
  let component: DemoButtrnComponent;
  let fixture: ComponentFixture<DemoButtrnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoButtrnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoButtrnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
