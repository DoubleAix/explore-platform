import { Component, OnInit, Input } from '@angular/core';
import { ConfigService } from 'src/app/services/config/config.service';
import { Observable } from 'rxjs';
import { SpaceGuard } from 'src/app/utils/guards/space.guard';
import { Space } from 'src/app/models/space.model';

@Component({
	selector: 'cdk-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

	@Input() sidenav;
	@Input() sidebar;
	@Input() drawer;
	@Input() matDrawerShow;
	@Input() space$:Observable<Space>;
	dashboardTitle: Observable<string>;

	constructor(
		private configService: ConfigService,
	) { }

	ngOnInit() {
		this.dashboardTitle = this.configService.dashboardTitle$;
	}

}
