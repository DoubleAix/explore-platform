import { Component, OnInit, Input } from '@angular/core';
import { PermissionService } from '../../services/permission/permission.service';
import { Observable } from 'rxjs';
import { ParentMenu } from 'src/app/models/menu.model';



@Component({
  selector: 'cdk-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

  @Input() iconOnly: boolean = false;
  @Input() menus: Observable<ParentMenu[]>;
  resource_policies: number[];

  constructor(
    private permissionService: PermissionService,
  ) { }

  ngOnInit() {

  }

  validateParentMenuItem(menu: ParentMenu): Observable<boolean> {
    let resourcePolicyIds: number[] = [];
    if (menu && menu.sub) {
      resourcePolicyIds = menu.sub.map(item => item.authorized_to);
    }
    return this.permissionService.validateResourcePolicyByIds(resourcePolicyIds)
  }
}
