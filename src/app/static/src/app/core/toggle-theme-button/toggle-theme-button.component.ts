import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme/theme.service';
import { themes } from './togge-theme-button.helper';

@Component({
  selector: 'cdk-toggle-theme-button',
  templateUrl: './toggle-theme-button.component.html',
  styleUrls: ['./toggle-theme-button.component.scss']
})

export class ToggleThemeButtonComponent implements OnInit {
  themes: any = themes;


  constructor(
    private themeService: ThemeService,
  ) { }

  ngOnInit() {
  }

  toggleTheme(themeClass: string) {
    this.themeService.toggleTheme(themeClass);
  }
}
