export const themes = [
    {
      'theme_name': '藍灰主題',
      'theme_class': 'basic-theme'
    },
    {
      'theme_name': '灰白主題',
      'theme_class': 'grey-theme'
    },
    {
      'theme_name': '暗紅主題',
      'theme_class': 'dark-red-theme'
    },
    {
      'theme_name': '森林主題',
      'theme_class': 'forest-theme'
    },
    {
      'theme_name': '暗藍主題',
      'theme_class': 'dark-blue-theme'
    },
    {
      'theme_name': '咖啡主題',
      'theme_class': 'brown-theme'
    },
    {
      'theme_name': '深紫主題',
      'theme_class': 'deep-purple-theme'
    },
    {
      'theme_name': '藍綠灰主題',
      'theme_class': 'teal-grey-theme'
    },
    {
      'theme_name': '暗琥珀主題',
      'theme_class': 'dark-amber-theme'
    },
    {
      'theme_name': '紅灰主題',
      'theme_class': 'red-grey-theme'
    },
    {
      'theme_name': '暗紫主題',
      'theme_class': 'dark-purple-theme'
    },
    {
      'theme_name': '暗綠主題',
      'theme_class': 'dark-green-theme'
    },
  ];