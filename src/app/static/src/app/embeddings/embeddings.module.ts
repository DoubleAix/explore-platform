import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmbeddingsRoutingModule } from './embeddings-routing.module';

import { KibanaTemplateComponent } from "./kibana-template/kibana-template.component";
import { UtilsModule } from "../utils/utils.module";
import { CoreModule } from "../core/core.module";


import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { TableauTemplateComponent } from './tableau-template/tableau-template.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    // suppressScrollX: true,
    // wheelPropagation: true,
};


@NgModule({
  declarations: [
    KibanaTemplateComponent,
    TableauTemplateComponent,
    

  ],
  imports: [
    CommonModule,
    EmbeddingsRoutingModule,
    UtilsModule,
    CoreModule,
    PerfectScrollbarModule,
    
  ],
  providers: [
    {
        provide: PERFECT_SCROLLBAR_CONFIG,
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
],
})
export class EmbeddingsModule { }
