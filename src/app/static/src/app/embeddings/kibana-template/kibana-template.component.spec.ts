import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KibanaTemplateComponent } from './kibana-template.component';

describe('KibanaTemplateComponent', () => {
  let component: KibanaTemplateComponent;
  let fixture: ComponentFixture<KibanaTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KibanaTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KibanaTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
