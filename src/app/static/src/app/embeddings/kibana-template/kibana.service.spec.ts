import { TestBed } from '@angular/core/testing';

import { KibanaService } from './kibana.service';

describe('KibanaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KibanaService = TestBed.get(KibanaService);
    expect(service).toBeTruthy();
  });
});
