import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { DashboardTitleService } from "../../utils/services/dashboard-title.service";

@Component({
  selector: 'app-kibana-template',
  templateUrl: './kibana-template.component.html',
  styleUrls: ['./kibana-template.component.scss']
})
export class KibanaTemplateComponent implements OnInit {
  kibana_url: string;
  dashboard_title: string;
  sub: any;


  constructor(
    private route: ActivatedRoute,
    // private dashboardTitleService : DashboardTitleService,
    ) { }

  ngOnInit() {
    this.sub = this.route.data.subscribe(d => {
      this.kibana_url = d.kibana_url;
      // this.dashboard_title = d.dashboard_title;
      // this.dashboardTitleService.next(this.dashboard_title);

    });
  }

  // ngOnDestroy() {
  //   this.sub.unsubscribe();
  // }
}
