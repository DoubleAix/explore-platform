import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KibanaTemplateComponent } from "./kibana-template/kibana-template.component";
import { TableauTemplateComponent } from "./tableau-template/tableau-template.component";
import { KibanaGuard } from "../utils/guards/kibana.guard";
import { TableauGuard } from '../utils/guards/tableau.guard';
import { TableauResolveService } from "../utils/resolves/tableau-resolve/tableau-resolve.service";

const routes: Routes = [
  // {
  //   path: 'kibana1',
  //   component: KibanaTemplateComponent,
  //   // canActivate: [AuthGuard],
  //   data: {
  //     kibana_url: "http://localhost:5601/app/kibana#/dashboard/722b74f0-b882-11e8-a6d9-e546fe2bba5f?embed=true&_g=(refreshInterval%3A(pause%3A!f%2Cvalue%3A900000)%2Ctime%3A(from%3Anow-7d%2Cmode%3Aquick%2Cto%3Anow))",
  //     dashboard_title: "科技研發部 / 數據暨智能發展科 / 每月人力統計",
  //     user_name: "root"
  //   }
  // },
  // {
  //   path: 'kibana2',
  //   component: KibanaTemplateComponent,
  //   data: {
  //     kibana_url: "proxy/localhost:5601/app/kibana#/dashboard/7adfa750-4c81-11e8-b3d7-01146121b73d?embed=true&_g=(refreshInterval%3A(pause%3A!f%2Cvalue%3A900000)%2Ctime%3A(from%3Anow-24h%2Cmode%3Aquick%2Cto%3Anow))",
  //     dashboard_title: "科技研發部 / 技術發展科 / 每月人力統計",
  //   }
  // },
  // {
  //   path: 'kibana3',
  //   component: KibanaTemplateComponent,
  //   data: {
  //     kibana_url: "http://localhost:5601/app/kibana#/dashboard/edf84fe0-e1a0-11e7-b6d5-4dc382ef7f5b?embed=true&_g=(refreshInterval%3A(pause%3A!f%2Cvalue%3A900000)%2Ctime%3A(from%3Anow-7d%2Cmode%3Aquick%2Cto%3Anow))",
  //     dashboard_title: "科技研發部 / 數據暨智能發展科 / 每季生產力指數"
  //   }
  // },
  {
    path: 'tableau/:id', component: TableauTemplateComponent, canActivate: [TableauGuard],
    resolve: {
      tableauDashboard:  TableauResolveService,
     },
    // data: { 
    //   dashboard_title: "新客戶數分析",
    //   dashboard_code: "NEW_CUST/Dashboard1" 
    // } 
  },
  // { path: 'tableau/1', component: TableauTemplateComponent, data: { 
  //   dashboard_title: "新客戶數分析",
  //   dashboard_code: "NEW_CUST/Dashboard1" 
  // } },
  // { path: 'tableau/2', component: TableauTemplateComponent, data: { 
  //   dashboard_title: "業務員屬性",
  //   dashboard_code: "_10/Dashboard1" 
  // } },
  // {path: '**', redirectTo: 'tableau/1'},
  // { path: 'featured', component: FeatureTableComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmbeddingsRoutingModule { }
