import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauTemplateComponent } from './tableau-template.component';

describe('TableauTemplateComponent', () => {
  let component: TableauTemplateComponent;
  let fixture: ComponentFixture<TableauTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableauTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
