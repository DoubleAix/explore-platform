import { Component, Inject, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { TableauService } from '../../services/embeddings/tableau/tableau.service';
import { ConfigService } from 'src/app/services/config/config.service';
import { Subscription, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, take, map } from 'rxjs/operators';
import { TableauData } from 'src/app/models/resource.model';
import { TableauDashboard } from 'src/app/models/tableau.model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { PerfectScrollbarConfigInterface,
  PerfectScrollbarComponent, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
// class HeightAndWidth {
//   height: number;
//   width: number;
// }

declare var tableau: any;
@Component({
  selector: 'app-tableau-template',
  templateUrl: './tableau-template.component.html',
  styleUrls: ['./tableau-template.component.scss']
})
export class TableauTemplateComponent implements OnInit, AfterViewInit, OnDestroy {
  // private tableauDashboard: BehaviorSubject<TableauDashboard> = new BehaviorSubject<TableauDashboard>(null);
  // readonly tableauDashboard$: Observable<TableauDashboard> = this.tableauDashboard.asObservable();
  viz: any;

  // dashboardWidth: number;
  // dashboardHeight: number;  
  // // detect dashboard content size 
  // private subscription: Subscription;
  // @ViewChild('divToTrackSizeChanges') divToTrackSizeChanges: ElementRef;
  
  constructor(
    private route: ActivatedRoute,
    private tableauService: TableauService,
    private configService: ConfigService,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit() {
    
    // this.route.data.subscribe(
    //   (data) => {
    //     console.log(data)
    //     this.tableauDashboard.next(data.tableauDashboard)
    //     // this.dashboardHeight = data.tableauDashboard.height
    //     // this.dashboardWidth = data.tableauDashboard.width
    //     let containerDiv = this.document.getElementById('vizContainer');
    //     containerDiv.style.height = `${data.tableauDashboard.height}px`;
    //     containerDiv.style.width = `${data.tableauDashboard.width}px`;
    //    });
      }
   

  // getHeightAndWidthObject(): HeightAndWidth {
  //   const newValues = new HeightAndWidth();
  //   newValues.height = this.divToTrackSizeChanges.nativeElement.offsetHeight;
  //   newValues.width = this.divToTrackSizeChanges.nativeElement.offsetWidth;
  //   return newValues;
  // }

  // setupHeightMutationObserver() {
  //   const observerable$ = new Observable<HeightAndWidth>(observer => {
  //     // Callback function to execute when mutations are observed
  //     // this can and will be called very often
  //     const callback = (mutationsList, observer2) => {
  //       observer.next(this.getHeightAndWidthObject());
  //     };
  //     // Create an observer instance linked to the callback function
  //     const elementObserver = new MutationObserver(callback);

  //     // Options for the observer (which mutations to observe)
  //     const config = { attributes: true, childList: true, subtree: true };
  //     // Start observing the target node for configured mutations
  //     elementObserver.observe(this.divToTrackSizeChanges.nativeElement, config);
  //   });

  //   this.subscription = observerable$
  //     .pipe(
  //       debounceTime(50),
  //       distinctUntilChanged()
  //     )
  //     .subscribe((newValues => {
  //       this.doDivHeightChange(newValues);
  //     }));
  // }
  // doDivHeightChange(newValues: HeightAndWidth) {
  //   let containerDiv = this.document.getElementById('vizContainer');
  //   let maxHeight: number = Math.max(newValues.height, this.dashboardHeight);
  //   let maxWidth: number = Math.max(newValues.width, this.dashboardWidth);
  //   containerDiv.style.height = `${maxHeight}px`;
  //   containerDiv.style.width = `${maxWidth}px`;
  //   console.log(newValues.height);
  //   console.log(newValues.width);
  // }

  ngAfterViewInit() {
    // this.setupHeightMutationObserver();
    // this.doDivHeightChange(this.getHeightAndWidthObject());

    this.route.params.subscribe(
      params => {
          this.route.data.subscribe(
          (data) => {
            // console.log(data)
            // this.tableauDashboard.next(data.tableauDashboard)
            // this.dashboardHeight = data.tableauDashboard.height
            // this.dashboardWidth = data.tableauDashboard.width
            let containerDiv = this.document.getElementById('vizContainer');
            containerDiv.style.height = `${data.tableauDashboard.height}px`;
            containerDiv.style.width = `${data.tableauDashboard.width}px`;
    let options: object = {
        width: `${data.tableauDashboard.width+27}px`,
        height: `${data.tableauDashboard.height+27}px`,
        hideTabs: true,
        hideToolbar: false,
        // toolbar:"top",
        onFirstInteractive: function () {
          console.log("Run this code when the viz has finished loading.");
        }
      };

      let viz = tableau.VizManager.getVizs()[0];
      if (viz) {
        viz.dispose();
      }
      viz = new tableau.Viz(containerDiv, data.tableauDashboard.tableau_dashboard_url, options);
    

           });
          })
        }

  //   this.tableauDashboard$.pipe(
  //     filter(data => data != null),
  //     take(1),
  //   ).subscribe(data => {
  //     this.configService.setDashboardTitle(data.dashboard_title);
  //     let containerDiv = this.document.getElementById("vizContainer");

  //     // https://onlinehelp.tableau.com/current/api/js_api/en-us/JavaScriptAPI/js_api_ref.htm#vizcreateoptions_record
  //     let options: object = {
  //       width: `${data.width}px`,
  //       height: `${data.height}px`,
  //       hideTabs: true,
  //       hideToolbar: false,
  //       // toolbar:"top",
  //       onFirstInteractive: function () {
  //         console.log("Run this code when the viz has finished loading.");
  //       }
  //     };

  //     let viz = tableau.VizManager.getVizs()[0];
  //     if (viz) {
  //       viz.dispose();
  //     }
  //     viz = new tableau.Viz(containerDiv, data.tableau_dashboard_url, options);
  //   }
  //   )
  // })
  
  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }
}