import { NgModule } from '@angular/core';
import {
    MatCardModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatToolbarModule,

    // MatDialogModule,
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginPageComponent } from './login-page.component';

import { UtilsModule } from '../utils/utils.module';
import { MessageDialogComponent } from '../utils/dialogs/message-dialog/message-dialog.component';


const routes: Routes = [
    { path: '', component: LoginPageComponent },
];
@NgModule({
    imports: [
        MatCardModule,
        CommonModule,
        FlexLayoutModule,
        
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatToolbarModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        UtilsModule,

        // MatDialogModule
    ],
    declarations: [
        LoginPageComponent,
        // MessageDialogComponent,
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ],
    entryComponents: [MessageDialogComponent]
})
export class LoginPageModule {
}
