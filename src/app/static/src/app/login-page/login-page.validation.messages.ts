export const validation_messages = {
    'user_id': [
        { type: 'required', message: '使用者ID是必要欄位' },
    ],
    'password': [
        { type: 'required', message: '密碼是必要欄位' },
        { type: 'minlength', message: '密碼最少要輸入6個字元' },
        { type: 'maxlength', message: '密碼最多只能輸入25個字元' },
    ],
};

export interface LoginInfoValidationMessages {
    user_id?: object[];
    password?: object[];
};