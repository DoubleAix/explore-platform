import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../services/auth/auth.service';
import { MessageDialogComponent } from '../utils/dialogs/message-dialog/message-dialog.component';
import { validation_messages, LoginInfoValidationMessages } from './login-page.validation.messages';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';
import { ThemeService } from "src/app/services/theme/theme.service";
import { LoginInfo } from '../models/login-info.model';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  animations: [
    trigger('enterintologinpage', [
      transition('void => *',
        animate('1000ms ease-in',
          keyframes([
            style({ transform: 'rotateY(180deg) rotateZ(90deg) scale(4)', opacity: 0 }),
            // style({ transform: 'rotateY(360deg) rotateZ(180deg)', }),
            // // style({ transform: 'rotateY(180deg) rotateZ(270deg)', }),
            style({ transform: 'rotateY(360deg) rotateZ(0deg)', opacity: 1 })
          ]))
      ),
      // transition('hide => show', animate('1000ms ease-in'))
    ])
  ]
})
export class LoginPageComponent implements OnInit {
  title = '探索平台';
  userForm: FormGroup;
  validation_messages: LoginInfoValidationMessages = validation_messages;
  returnUrl: string;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private authService: AuthService,
    private dialog: MatDialog,
    private titleService: Title,
    private themeService: ThemeService,
    ) {
  }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.themeService.toggleTheme('basic-theme');
    this.buildForm();
    this.authService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/space';
  }

  buildForm() {
    this.userForm = this.fb.group({
      user_id: ['', [
        Validators.required,
      ]
      ],
      password: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(25)
      ]
      ],
    });


  }

  login() {
    const data: LoginInfo = this.userForm.value;


    this.authService.login(data)
      .subscribe(
        () => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          console.log(error);
          this.dialog.open(MessageDialogComponent
          );
        },
      );

  }

  validateByControl(validation, controlName: string| string[]): boolean {
    return this.userForm.get(controlName).hasError(validation.type) &&
      (this.userForm.get(controlName).dirty || this.userForm.get(controlName).touched);
  }
}

