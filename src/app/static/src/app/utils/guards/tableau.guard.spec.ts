import { TestBed, async, inject } from '@angular/core/testing';

import { TableauGuard } from './tableau.guard';

describe('TableauGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TableauGuard]
    });
  });

  it('should ...', inject([TableauGuard], (guard: TableauGuard) => {
    expect(guard).toBeTruthy();
  }));
});
