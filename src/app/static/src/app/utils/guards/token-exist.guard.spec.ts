import { TestBed, async, inject } from '@angular/core/testing';

import { TokenExistGuard } from './token-exist.guard';

describe('TokenExistGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenExistGuard]
    });
  });

  it('should ...', inject([TokenExistGuard], (guard: TokenExistGuard) => {
    expect(guard).toBeTruthy();
  }));
});
