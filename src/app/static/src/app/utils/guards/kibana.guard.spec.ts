import { TestBed, async, inject } from '@angular/core/testing';

import { KibanaGuard } from './kibana.guard';

describe('KibanaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KibanaGuard]
    });
  });

  it('should ...', inject([KibanaGuard], (guard: KibanaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
