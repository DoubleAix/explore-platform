import { TestBed, async, inject } from '@angular/core/testing';

import { TokenRequiredGuard } from './token-required.guard';

describe('TokenRequiredGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenRequiredGuard]
    });
  });

  it('should ...', inject([TokenRequiredGuard], (guard: TokenRequiredGuard) => {
    expect(guard).toBeTruthy();
  }));
});
