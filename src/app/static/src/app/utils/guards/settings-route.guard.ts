import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
// import { ConfigService } from '../../services/config/config.service';
import { PermissionService } from '../../services/permission/permission.service';
@Injectable({
  providedIn: 'root'
})
export class SettingsRouteGuard implements CanActivateChild, CanActivate {
  constructor(
    // private configService: ConfigService,
    private permissionService: PermissionService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    // let resource_policy_id = this.configService.getSettingResourcePolicyId(route.data.resource_name, route.data.permission_name)
    // this.permissionService.validateSettingsResourcePolicy(route.data.resource_name, route.data.permission_name).subscribe(data => console.log(data))
    return this.permissionService.validateSettingsResourcePolicy(route.data.resource_name, route.data.permission_name)
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    return this.canActivate(route, state)
  }
}
