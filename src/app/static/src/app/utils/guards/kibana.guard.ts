import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { TableauService } from '../../services/embeddings/tableau/tableau.service';
import { PermissionService } from '../../services/permission/permission.service';
@Injectable({
  providedIn: 'root'
})
export class KibanaGuard implements CanActivate {

  constructor(
    private tableauService: TableauService,
    private router: Router,
    private permissionService: PermissionService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    // console.log();
    
    return true;
    // console.log(route.params["id"])
    // route.data['id'] = route.params["id"];
    // this.tableauService.getDashboardUrl(route.params["id"]).pipe(take(1)).subscribe(data => )
    // return this.permissionService.validateResourcePolicyById();
    // let resource_policy_id = this.configService.getSettingResourcePolicyId(route.data.resource_name, route.data.permission_name)
    // this.permissionService.validateSettingsResourcePolicy(route.data.resource_name, route.data.permission_name).subscribe(data => console.log(data))
    // return this.permissionService.validateSettingsResourcePolicy(route.data.resource_name, route.data.permission_name)
  }
}
