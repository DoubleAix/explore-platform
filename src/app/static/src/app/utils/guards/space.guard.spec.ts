import { TestBed, async, inject } from '@angular/core/testing';

import { SpaceGuard } from './space.guard';

describe('SpaceGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpaceGuard]
    });
  });

  it('should ...', inject([SpaceGuard], (guard: SpaceGuard) => {
    expect(guard).toBeTruthy();
  }));
});
