import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Router, Route, UrlSegment } from "@angular/router";
import { AuthService } from '../../services/auth/auth.service'
import { RefreshInfo } from '../../models/refresh-info.model'

@Injectable({
  providedIn: 'root'
})
export class TokenExistGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (localStorage.getItem('access_token') && localStorage.getItem('refresh_token')) {
      // console.log("Token exists.");
      const refreshInfo: RefreshInfo = { user_policy_type: [] };
      this.authService.refresh(refreshInfo).subscribe(() => {
        this.router.navigate(['/space']);
      },
        error => {
          console.log(error);
          return true
        });

    } else {
      // console.log("Token doesn't exist.");
      return true;
    }
  }
}
