import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { TableauService } from '../../services/embeddings/tableau/tableau.service';
import { PermissionService } from '../../services/permission/permission.service';
import { PermissionOption } from "src/app/enums/permission-option.enum";
@Injectable({
  providedIn: 'root'
})
export class TableauGuard implements CanActivate {
  constructor(
    private tableauService: TableauService,
    private router: Router,
    private permissionService: PermissionService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    return this.permissionService.validateResourcePolicyByResourceId(
      route.pathFromRoot[1].params['spaceId'],
      parseInt(route.params["id"]
      ));
  }
}