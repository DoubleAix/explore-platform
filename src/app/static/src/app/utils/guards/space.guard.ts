import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild, CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';
import { take, filter, switchMap } from "rxjs/operators";
import { Router } from "@angular/router";
import { PermissionService } from '../../services/permission/permission.service';
import { PermissionOption } from '../../enums/permission-option.enum';
import { ConfigService } from "../../services/config/config.service";
// import { routerNgProbeToken } from '@angular/router/src/router_module';

@Injectable({
  providedIn: 'root'
})
export class SpaceGuard implements CanActivate, CanActivateChild {
  constructor(
    private router: Router,
    private permissionService: PermissionService,
    private configService: ConfigService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    return this.permissionService.validateSpacePolicybySpaceId(route.params['spaceId'], PermissionOption.READ).pipe(
      switchMap(
        data => {
          if (data) {
            return of(true)
          } else {
            this.router.navigate(['/space']);
          }
        }
      ))
  }


  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    return this.canActivate(route, state);
  }


}
