import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDialogModule,
  MatButtonModule,
} from '@angular/material';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SafePipe } from './pipes/safe/safe.pipe';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { DashboardDialogComponent } from './dialogs/dashboard-dialog/dashboard-dialog.component';
import { MessageDialogComponent } from './dialogs/message-dialog/message-dialog.component';
import { GeneralSnackbarComponent } from './snackbars/general-snackbar/general-snackbar.component';
import { RemoveUnderscorePipe } from './pipes/remove-underscore/remove-underscore.pipe';
import { RemoveDashPipe } from './pipes/remove-dash/remove-dash.pipe';


@NgModule({
  declarations: [
    SafePipe,
    ConfirmDialogComponent,
    DashboardDialogComponent,
    MessageDialogComponent,
    GeneralSnackbarComponent,
    RemoveUnderscorePipe,
    RemoveDashPipe,


  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
  ],
  exports: [
    SafePipe,
    ConfirmDialogComponent,
    DashboardDialogComponent,
    MessageDialogComponent,
    GeneralSnackbarComponent,
    RemoveUnderscorePipe,
    RemoveDashPipe,

  ]
})
export class UtilsModule { }
