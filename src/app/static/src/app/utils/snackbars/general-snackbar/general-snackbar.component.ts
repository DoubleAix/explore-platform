import { Component, OnInit,Inject } from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material';
// import {  } from "@angular/material";
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-general-snackbar',
  templateUrl: './general-snackbar.component.html',
  styleUrls: ['./general-snackbar.component.scss']
})
export class GeneralSnackbarComponent implements OnInit {

  constructor(
    private snackBar: MatSnackBar, 
    @Inject(MAT_SNACK_BAR_DATA) public data: any,
    ) { }

  ngOnInit() {
  }
  closeSnackBar() {
    this.snackBar.dismiss();
  }
}
