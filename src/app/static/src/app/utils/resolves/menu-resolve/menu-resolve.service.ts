import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';
import { Observable } from 'rxjs';
import { ParentMenu } from 'src/app/models/menu.model';
@Injectable({
  providedIn: 'root'
})
export class MenuResolveService implements Resolve<any> {

  constructor(
    private configService:ConfigService,
  ) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ParentMenu[]> {
    return this.configService.getMenu(route.params['spaceId']);
  }

}
