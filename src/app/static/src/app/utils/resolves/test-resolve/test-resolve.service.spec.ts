import { TestBed } from '@angular/core/testing';

import { TestResolveService } from './test-resolve.service';

describe('TestResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TestResolveService = TestBed.get(TestResolveService);
    expect(service).toBeTruthy();
  });
});
