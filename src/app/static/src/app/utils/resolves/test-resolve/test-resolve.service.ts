import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { SpaceService } from 'src/app/services/space/space.service'
import { Space } from 'src/app/models/space.model';
import { Observable, of } from 'rxjs';
import { delay } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class TestResolveService implements Resolve<any> {

  constructor(
    private spaceService: SpaceService,
  ) { }
  resolve(route: ActivatedRouteSnapshot): Observable<string> {
    
    // this.spaceService.getSpaceBySpaceId(route.params['space_id']).subscribe(
      // data => console.log(data)
    // );

    return of('Hello Alligator!').pipe(delay(2000));
  }
}
