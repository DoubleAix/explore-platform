import { TestBed } from '@angular/core/testing';

import { SpaceResolveService } from './space-resolve.service';

describe('SpaceResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpaceResolveService = TestBed.get(SpaceResolveService);
    expect(service).toBeTruthy();
  });
});
