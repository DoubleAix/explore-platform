import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { SpaceService } from 'src/app/services/space/space.service'
import { Space } from 'src/app/models/space.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpaceResolveService implements Resolve<any>{

  constructor(
    private spaceService: SpaceService,
  ) { }
  resolve(route: ActivatedRouteSnapshot): Observable<Space> {
    return this.spaceService.getSpaceBySpaceId(route.params['spaceId']);
  }
}
