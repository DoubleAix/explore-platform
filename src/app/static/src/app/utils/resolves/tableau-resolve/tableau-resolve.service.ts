import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { TableauService } from '../../../services/embeddings/tableau/tableau.service';
import { Observable } from 'rxjs';

import { TableauDashboard } from 'src/app/models/tableau.model';
@Injectable({
  providedIn: 'root'
})
export class TableauResolveService {

  constructor(
    private tableauService: TableauService,
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<TableauDashboard> {
    return this.tableauService.getDashboardUrl(route.params['id']);
  }
}
