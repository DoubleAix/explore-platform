import { TestBed } from '@angular/core/testing';

import { AddTokenInterceptor } from './add-token.interceptor';

describe('AddTokenInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddTokenInterceptor = TestBed.get(AddTokenInterceptor);
    expect(service).toBeTruthy();
  });
});