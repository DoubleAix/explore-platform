import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { catchError, switchMap, filter, take,finalize } from 'rxjs/operators';
import { Router } from "@angular/router";

import { AuthService } from '../../services/auth/auth.service'
import { RefreshInfo } from "../../models/refresh-info.model";


@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  constructor(
    private authService: AuthService,
    private router: Router,


  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError(error => {
        if (
          req.url.includes("refresh") ||
          req.url.includes("login")
        ) {
          if (
            req.url.includes("refresh")
          ) {
            this.authService.logout();
            this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url } });
          }
          return throwError(error);
        }

        if (error.status != 401) {
          return throwError(error);
        }

        if (this.refreshTokenInProgress) {
          return this.refreshTokenSubject.pipe(
            filter(data => data !== null),
            take(1),
            switchMap(() => next.handle(this.addAuthenticationToken(req))),
            
          )
        } else {
          console.log("start refresh token");
          this.refreshTokenInProgress = true;
          this.refreshTokenSubject.next(null);
          const refreshInfo: RefreshInfo = { user_policy_type: [] };
          return this.authService.refresh(refreshInfo).pipe(
            switchMap(data => {
              this.refreshTokenInProgress = false;
              this.refreshTokenSubject.next(data);
              return next.handle(this.addAuthenticationToken(req));
            }),
            catchError(error => {
              this.refreshTokenInProgress = false;
              this.authService.logout();
              this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url } });
              return throwError(error);
            }),
         
          )
        }
      }),
    )
  }
  addAuthenticationToken(req: HttpRequest<any>): HttpRequest<any> {
    const accessToken = localStorage.getItem("access_token");
    if (accessToken) {
      const cloned = req.clone({
        headers: req.headers.set("Authorization",
          "Bearer " + accessToken)
      });
      return cloned
    } else {
      return req
    }
  }
}
