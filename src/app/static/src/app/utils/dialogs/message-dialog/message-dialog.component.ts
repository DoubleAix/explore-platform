import {Component, OnInit,Inject, Injectable} from  '@angular/core';

import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from  '@angular/material';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit {

  constructor(
    private  dialogRef:  MatDialogRef<MessageDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public  data:  any) { }
  
  message: string = "你所輸入的帳號或密碼錯誤，請重新登入，謝謝！！"

  ngOnInit() {

    // this.dialogRef.updateSize('40vh', '30vh');
  }
  public  closeDialog() {
    this.dialogRef.close();
}
}
