import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { trigger, state, style, animate, transition, group, stagger, query, keyframes } from '@angular/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatFooterRow } from '@angular/material';
import { timer } from 'rxjs';
// import { first } from "rxjs/operators";

@Component({
  selector: 'app-dashboard-dialog',
  templateUrl: './dashboard-dialog.component.html',
  styleUrls: ['./dashboard-dialog.component.scss'],
  })
export class DashboardDialogComponent implements OnInit {
  timer$ = timer(8000)
  autoclose
  constructor(
    private matDialog: MatDialog,
    private dialogRef: MatDialogRef<DashboardDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }
s
  message: string = "已經將您的重要訊息送至相關單位之信箱！！請等待回覆！！"

  ngOnInit() {
    const dialog_number: number = this.matDialog.openDialogs.length - 1;
    const dialog_position_offset: string = String(dialog_number * 210 + 25);
    this.dialogRef.updatePosition({ bottom: dialog_position_offset + 'px', right: '25px' });
    this.dialogRef.afterClosed().subscribe(
      _ => {
        if (!this.autoclose.closed) {
          this.autoclose.unsubscribe();
        }
       
        this.matDialog.openDialogs.forEach((mr, index) => {
          const dialog_position_offset: string = String(index * 200 + 25);
          mr.updatePosition({ bottom: dialog_position_offset + 'px', right: '25px' });
        })
      });
    this.autoclose = this.timer$.subscribe(
      _ => this.dialogRef.close()
      );


  }
  public closeDialog() {
    this.dialogRef.close();
  }
}
