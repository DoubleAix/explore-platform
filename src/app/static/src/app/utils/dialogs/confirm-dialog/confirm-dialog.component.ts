import { Component, OnInit, Inject, Injectable } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  message: string = `請問要刪除${this.data.name}嗎？`;
  type: string = this.data.type;
  ngOnInit() {

  }
  public closeDialog() {
    this.dialogRef.close();
  }
  public confirm() {
    this.dialogRef.close(this.data.id);
  }
}
