import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { Config } from 'protractor';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { ThemeService } from './services/theme/theme.service';
import { Observable } from 'rxjs';
import { iconList } from 'src/app/settings/space/space.icons';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],



})
export class AppComponent implements OnInit {
  themeClass$: Observable<string>;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private themeService: ThemeService,
  ) {
    iconList.forEach(item => {
      this.matIconRegistry.addSvgIcon(
        item,
        this.domSanitizer.bypassSecurityTrustResourceUrl(`./assets/space-icon/${item}.svg`)

      );
    });
 

  }

  ngOnInit() {
    this.themeClass$ = this.themeService.themeClass$;
  }
}
