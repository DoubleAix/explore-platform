import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
// import { TestResolveService } from '../utils/resolves/test-resolve/test-resolve.service';
import { TokenRequiredGuard } from "../utils/guards/token-required.guard";
import { HomePageComponent } from "src/app/dashboard/home-page/home-page.component";

const routes: Routes = [
    {
        path: '', component: DashboardComponent, 
        //  resolve: { items: TestResolveService },
        children: [
            { path: 'tests', loadChildren: () => import('../tests/tests.module').then(m => m.TestsModule) },  //test
            // { path: 'tables', loadChildren: '../tables/tables.module#TablesModule' }, // test
            { path: 'settings', loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule) },
            { path: 'embeddings', loadChildren: () => import('../embeddings/embeddings.module').then(m => m.EmbeddingsModule) },
            {
                path: '**',
                component: HomePageComponent,
            },
        ],
    }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }