import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { GeneralSnackbarComponent } from '../utils/snackbars/general-snackbar/general-snackbar.component';

import {
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    MatSidenavModule,
    MatProgressBarModule,

} from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardRoutingModule } from './dashboard-routing.module';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

import { CoreModule } from '../core/core.module';
import { DashboardDialogComponent } from '../utils/dialogs/dashboard-dialog/dashboard-dialog.component';
import { UtilsModule } from '../utils/utils.module';
import { HomePageComponent } from './home-page/home-page.component';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        MatTabsModule,
        CoreModule,
        MatSidenavModule,
        PerfectScrollbarModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        UtilsModule,
        FlexLayoutModule,


    ],
    declarations: [DashboardComponent, HomePageComponent],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ],
    entryComponents: [
        DashboardDialogComponent,
        GeneralSnackbarComponent
    ]
})
export class DashboardModule { }
