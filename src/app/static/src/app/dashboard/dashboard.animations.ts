import {
    trigger,
    transition,
    style,
    query,
    group,
    animateChild,
    animate,
    keyframes,
} from '@angular/animations';

export const Fader =
    trigger('routeAnimations', [
        transition('* <=> *', [
            // Set a default  style for enter and leave
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    left: 0,
                    width: '100%',
                    height: '100%',
                    opacity: 0,
                    transform: 'scale(0) translateY(100%)',
                }),
            ], { optional: true }),
            // Animate the new page in
            query(':enter', [
                animate('600ms ease', style({ opacity: 1, transform: 'scale(1) translateY(0)' })),
            ], { optional: true })
        ]),
    ]);

export const Stepper3 =
    trigger('routeAnimations', [
        // transition('isDashboard => isDashboard', changeHeight('100%')),

        transition('* => isDashboard', changeHeight('100%')),
        // transition('isDashboard => *', changeHeight('auto')),
        transition('* <=> *', changeHeight('auto')),
    ]);

function changeHeight(heightValue) {
    const optional = { optional: true };
    return [
        query(':enter, :leave', [
            style({
                position: 'absolute',
                left: 0,
                width: '100%',
                height: heightValue,
            }),
        ], optional),
        group([
            query(':enter', [
                animate('1000ms ease', keyframes([
                    style({ opacity: 0, transform: 'scale(0) translateX(50%)', offset: 0 }),
                    style({ opacity: 0.5, transform: 'scale(0.5) translateX(25%)', offset: 0.5 }),
                    style({ opacity: 1, height: heightValue, transform: 'scale(1) translateX(0%)', offset: 1 }),
                ])),
            ], optional),
            query(':leave', [
                animate('1000ms ease', keyframes([
                    style({ opacity: 1, height: heightValue, transform: 'scale(1)', offset: 0 }),
                    style({ opacity: 0.5, transform: 'scale(3) translateX(-25%) rotate(0)', offset: 0.5 }),
                    style({ opacity: 0, transform: 'scale(6) translateX(-50%) rotate(-180deg)', offset: 1 }),
                ])),
            ], optional)
        ]),
    ];
}


export const Stepper =
    trigger('routeAnimations', [
        transition('* <=> *', [
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    width: '100%',
                    height: '100%',
                    // height: 'inherit',
                }),
            ], { optional: true }),
            group([
                query(':enter', [
                    animate('1000ms ease', keyframes([
                        style({ opacity: 0, transform: 'scale(0) translateX(50%)', offset: 0 }),
                        style({ opacity: 0.5, transform: 'scale(0.5) translateX(25%)', offset: 0.5 }),
                        style({ opacity: 1, transform: 'scale(1) translateX(0%)', offset: 1 }),
                    ])),
                ], { optional: true }),
                query(':leave', [
                    animate('1000ms ease', keyframes([
                        style({ opacity: 1, transform: 'scale(1)', offset: 0 }),
                        style({ opacity: 0.5, transform: 'scale(0.5) translateX(-25%) rotate(0)', offset: 0.5 }),
                        style({ opacity: 0, transform: 'scale(6) translateX(-50%) rotate(-180deg)', offset: 1 }),
                    ])),
                ], { optional: true })
            ]),
        ])

    ]);

export const DashboardInit = trigger('enterintodashboard', [
    transition('void => *',
        animate('1000ms ease-in',
            keyframes([
                style({ transform: 'scale(0)', opacity: 0 }),
                // style({ transform: 'rotateY(360deg) rotateZ(180deg)', }),
                style({ transform: 'scale(0.5)', opacity: 0.5 }),
                style({ transform: 'scale(1)', opacity: 1 })
            ]))
    ),
    // transition('hide => show', animate('1000ms ease-in'))
]);
