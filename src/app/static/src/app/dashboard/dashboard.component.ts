import { Component, OnInit, OnDestroy } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Title } from '@angular/platform-browser';
import { RouterOutlet } from '@angular/router';
import { Stepper, Fader, DashboardInit } from './dashboard.animations';

import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  RoutesRecognized,
  Router,
  ActivatedRoute,
} from '@angular/router';

import { filter, map, mergeMap, tap } from 'rxjs/operators';
// import { menus } from './dashboard.menus';
import { ConfigService } from '../services/config/config.service';
import { LoadingService } from '../services/loading/loading.service';
import { ParentMenu } from '../models/menu.model';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Space } from '../models/space.model';
import { domainToASCII } from 'url';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    Stepper,
    DashboardInit,
  ]

})

export class DashboardComponent implements OnInit, OnDestroy {
  space_name: string;
  space_logo = './assets/logo.takoyaki.png';

  private space: BehaviorSubject<Space> = new BehaviorSubject<Space>(null);
  readonly space$: Observable<Space> = this.space.asObservable();

  sideNavOpened = true;
  matDrawerOpened = false;
  matDrawerShow = true;
  sideNavMode = 'side';

  // loading progress bar
  // loading$: Observable<boolean>;
  loading: boolean;
  // menu list
  menus: Observable<ParentMenu[]>;

  // dashboard title
  routerSub$: any;



  constructor(
    private media: MediaObserver,
    private router: Router,
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private configService: ConfigService,
    private themeService: ThemeService,
    // private loadingService: LoadingService,
  ) {
    // loading progress bar
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart && !(event.url.includes('#/login') || event.url.includes('#/space')): {
          this.loading = true;
          break;
        }
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });

    // set dashboard title
    this.routerSub$ = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map(route => { while (route.firstChild) { route = route.firstChild; } return route; }),
      filter(route => route.outlet === 'primary'),
      mergeMap(route => route.data),
    )
      .subscribe((data) => {
        this.configService.setDashboardTitle(data.dashboard_title || '');
      });
  }

  ngOnInit() {
    this.activatedRoute.data.pipe(
      tap(data => this.space.next(data.space)),
      map(data => data.space),
    ).subscribe(
      (data: Space) => {
        this.space_name = data.space_name;
        this.menus = this.configService.dashboardMenu$;
        this.titleService.setTitle(data.space_name);
        this.themeService.toggleTheme(data.theme_class);
      });


    // this.loading$ = this.loadingService.loading$;
    this.media.media$.subscribe((mediaChange: MediaChange) => {
      this.toggleView();
    });

    // this.configService.getMenu().subscribe(
    //   data => {
    //     this.menus = this.menus.concat(data);
    //   },
    //   error => console.log(error),
    // );
  }

  ngOnDestroy() {
    this.routerSub$.unsubscribe();
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
    // tslint:disable-next-line: max-line-length
    // return outlet.activatedRouteData['animation'] ? outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'] : outlet && outlet.activatedRouteData;
  }


  toggleView() {
    if (this.media.isActive('gt-md')) {
      this.sideNavMode = 'side';
      this.sideNavOpened = true;
      this.matDrawerOpened = false;
      this.matDrawerShow = true;
    } else if (this.media.isActive('gt-xs')) {
      this.sideNavMode = 'side';
      this.sideNavOpened = false;
      this.matDrawerOpened = true;
      this.matDrawerShow = true;
    } else if (this.media.isActive('lt-sm')) {
      this.sideNavMode = 'over';
      this.sideNavOpened = false;
      this.matDrawerOpened = false;
      this.matDrawerShow = false;
    }
  }
}
