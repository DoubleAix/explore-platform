import { Component, OnInit } from '@angular/core';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Space } from 'src/app/models/space.model';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  private space: BehaviorSubject<Space> = new BehaviorSubject<Space>(null);
  readonly space$: Observable<Space> = this.space.asObservable();

  constructor(
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.parent.data.pipe(
      tap(data => this.space.next(data.space))
      ).subscribe();

  }
}
