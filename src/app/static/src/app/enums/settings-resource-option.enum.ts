export enum SettingsResourceOption {
    MANAGEMENT = 'management',
    USER_MANAGEMENT = 'user_management',
    RESOURCE_MANAGEMENT = 'resource_management',
    GROUP_MANAGEMENT = 'group_management',
    SPACE_MANAGEMENT = 'space_management',
}
