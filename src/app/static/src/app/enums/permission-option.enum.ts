export enum PermissionOption {
    READ = 'read',
    DELETE = 'delete',
    UPDATE = 'update',
    NOTIFICATION = 'notification',
    AUTHORIZATION = 'authorization',
    FEEDBACK = 'feedback',
    BELONG = 'belong',
    READ_CHILD = 'read_child',
    CREATE_CHILD = 'create_child',
    DELETE_CHILD = 'delete_child',
    UPDATE_CHILD = 'update_child',
    NOTIFICATION_CHILD = 'notification_child',
    AUTHORIZATION_CHILD = 'authorization_child',
    FEEDBACK_CHILD = 'feedback_child',
    OWNER = 'owner',
    OWNER_READ = 'owner_read',
    OWNER_UPDATE = 'owner_update',
}



