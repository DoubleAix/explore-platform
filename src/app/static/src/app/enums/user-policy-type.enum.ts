export enum UserPolicyType {
    RESOURCE_POLICY = 'resource_policies',
    GROUP_POLICY = 'group_policies',
    SPACE_POLICY = 'space_policies',
    AUTHORIZABLE_RESOURCE_POLICY = 'authorizable_resource_policies',
    AUTHORIZABLE_GROUP_POLICY = 'authorizable_group_policies',
    AUTHORIZABLE_SPACE_POLICY = 'authorizable_space_policies',
}
