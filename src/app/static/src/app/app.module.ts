import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { APP_INITIALIZER } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthInterceptor } from "./utils/interceptors/auth.interceptor";
import { AddTokenInterceptor } from "./utils/interceptors/add-token.interceptor";
// import { SafePipe } from './utils/pipes/safe.pipe';
import { CustomPaginatorConfig } from "./config/custom-paginator.config";
import { ConfigService } from "./services/config/config.service";



@NgModule({
  declarations: [
    AppComponent,
    // SafePipe,
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
  ],

  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AddTokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    {
      provide: MatPaginatorIntl,
      useClass: CustomPaginatorConfig,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigService) => () => configService.initPermissionMapping(),
      deps: [ConfigService],
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigService) => () => configService.initSettingsResourcePolicyMapping(),
      deps: [ConfigService],
      multi: true
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }