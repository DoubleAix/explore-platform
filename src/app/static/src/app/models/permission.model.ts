export interface Permission {
    name: string;
    label?: string;
    id?: number;
}
