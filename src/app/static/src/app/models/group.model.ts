export interface PolicyPermission {
    name: string;
}

export interface GroupPolicy {
    id: number;
    group_id?: number;
    permission_id?: number;
}

export interface Group {
    id: number;
    group_id: string;
    group_name: string;
    reserved: boolean;
    authorization_type_id: number;
    group_policies: GroupPolicy[];
}
