export interface LoginInfo {
    user_id: string;
    password: string;
}