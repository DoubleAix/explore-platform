
export interface SpacePolicy {
    id: number;
    space_id?: number;
    permission_id?: number;
}
export interface Space {
    id: number;
    space_id: string;
    space_name: string;
    icon_name: string;
    reserved: boolean;
    authorization_type_id: number;
    theme_class: string;
    space_policies: SpacePolicy[];
}

