export interface TableauDashboard {
    dashboard_title: string;
    tableau_dashboard_url: string;
    height: number;
    width: number;
}