export interface PolicyPermission {
    name: string;
}

export interface ResourcePolicy {
    id: number;
    resource_id?: number;
    permission_id?: number;
    permission: PolicyPermission;
}

export interface TableauData {
    workbook_name: string;
    dashoboard_name: string;
    site_name?: string;
    height: number;
    width: number;

}

export interface KibanaData {
}

export interface ChildResourceData {
    tableau?: TableauData;
    kibana?: KibanaData;
}

export interface ChildResource {
    id: number;
    child_resource_name: string;
    layer: string;
    icon_name: string;
    type: string;
    label: string | null;
    reserved: boolean;
    authorization_type_id: number;
    parent_id: string;
    data?: ChildResourceData;
    resource_policies: ResourcePolicy[];
}

export interface ParentResource {
    id: number;
    parent_resource_name: string;
    layer: string;
    icon_name: string;
    tags:string[]| null;
    type: string | null;
    label: string | null;
    reserved: boolean;
    authorization_type_id: number;
    children: ChildResource[];
    resource_policies: ResourcePolicy[];
}

