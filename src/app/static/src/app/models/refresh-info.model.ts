export interface RefreshInfo {
    user_policy_type: string[];
    access_token?: string;
}
