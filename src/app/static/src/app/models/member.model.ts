// User
export interface Member {
    id: number;
    user_id: string;
    user_name: string;
    email: string;
    reserved: boolean;
    }