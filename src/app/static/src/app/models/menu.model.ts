export interface ChildMenu {
    name: string;
    icon: string;
    link: string;
    resource_id?:number,
    authorized_to: number;
    open?: boolean,
}


export interface ParentMenu {
    name: string;
    icon: string;
    link?: string | boolean;
    authorized_to?: number | null;
    resource_id?: number,
    open?: boolean,
    sub?: ChildMenu[];
}
