export interface LoginResult {
    access_token: string;
    refresh_token?: string;
}
