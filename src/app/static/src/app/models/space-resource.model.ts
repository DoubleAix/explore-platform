export interface SpaceResource {
    id: number;
    resource_name: string;
    icon_name: string;
    reserved: boolean;
}

export interface SpaceResources {
    embeddable: SpaceResource[];
    embedded: SpaceResource[];
}