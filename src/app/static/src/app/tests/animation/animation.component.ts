import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, group, stagger, query, keyframes } from '@angular/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatDialogConfig } from '@angular/material';
import { DashboardDialogComponent } from '../../utils/dialogs/dashboard-dialog/dashboard-dialog.component';


@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.scss'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [

        query(':enter', style({ opacity: 0 }), { optional: true }),

        query(':enter', stagger('20ms', [
          animate('0.5s ease-in-out', keyframes([
            style({ opacity: 1, transform: 'translateY(0)' }),
            style({ opacity: 1, transform: 'translateY(0)' }),
            style({ opacity: 1, transform: 'translateY(0)' }),
          ]))]), { optional: true }),
      ])
    ]),
  ]
})

export class AnimationComponent implements OnInit {

  menuItem: MenuItem[];

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    this.menuItem = [
      { name: 'thumb_up_alt', mouseOvered: false },
      { name: 'thumb_down_alt', mouseOvered: false },
      { name: 'keyboard_voice', mouseOvered: false },
      { name: 'live_help', mouseOvered: false },
    ];
  }

  isHovering = false;

  mouseHovering() {
    this.isHovering = true;
  }
  mouseLeaving() {
    this.isHovering = false;
  }
  clickFeedback() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.hasBackdrop = false;
    dialogConfig.panelClass = 'login-dialog';
    dialogConfig.position = {
      bottom: '20px',
      right: '20px'
    };

    this.dialog.open(DashboardDialogComponent, dialogConfig);
  }
}


interface MenuItem {
  name: string;
  mouseOvered: boolean;

}