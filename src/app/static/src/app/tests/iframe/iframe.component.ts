import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
import { KibanaService } from '../../embeddings/kibana-template/kibana.service'; 


@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss']
})
export class IframeComponent implements OnInit {
  @ViewChild('myiframe', { static: true }) iframe: ElementRef;

  constructor(
    // private kibanaService: KibanaService,
  ) { }

  ngOnInit() {
    // this.kibanaService.get("http://localhost:5601/s/test/app/kibana#/dashboard/d2c73ce0-3bdb-11e9-a3d4-1d83063306d0?embed=true&_g=()")
    // .subscribe(blob => this.iframe.nativeElement.src = blob);
  }

  ngAfterViewInit() {
   
  }
}
  
