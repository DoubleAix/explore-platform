import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
// import { decode } from "jwt-decode";
import * as moment from "moment";
import { Router } from '@angular/router';

@Component({
  selector: 'app-httpclient',
  templateUrl: './httpclient.component.html',
  styleUrls: ['./httpclient.component.scss']
})
export class HttpclientComponent implements OnInit {

  jwthelper = new JwtHelperService();

  username: string = 'root';
  password: string = '00000000';

  access_token: string;
  refresh_token: string;
  expiration_time: string;
  log_in: boolean;
  user_id: string;
  resource_policies:number[];
  resource_policy:number;


  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {

  }


  login() {
    this.authService.login({ user_id: this.username, password: this.password })
      .subscribe(
        () => {
          this.getinfo()
        }
      );

  }

  getinfo() {
    const access_token_object = this.jwthelper.decodeToken(localStorage.getItem("access_token"));
    this.access_token = JSON.stringify(access_token_object);
    const refresh_token_object = this.jwthelper.decodeToken(localStorage.getItem("refresh_token"));
    this.refresh_token = JSON.stringify(refresh_token_object);
    this.expiration_time = moment.unix(access_token_object.exp).toString();
    this.log_in = this.authService.isLoggedIn();
    this.user_id = localStorage.getItem("sub");
    this.resource_policies = JSON.parse(localStorage.getItem("resource_policies"));
    this.resource_policy = this.resource_policies[0]

  }

  clean() {
    this.authService.logout()
  }
  refresh() {
    // this.authService.refresh().subscribe(
    //   () => {
    //     this.getinfo()
    //    },
    //    error => {
    //      console.log(error);
    //     //  this.router.navigate(['/login']);
    //    },
    // );
  }



}
