import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { TestsRoutingModule } from './tests-routing.module';
import { HttpclientComponent } from './httpclient/httpclient.component';
import { FlexlayoutComponent } from './flexlayout/flexlayout.component';
import { AnimationComponent } from './animation/animation.component';
import {
  MatCardModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatInputModule,
  MatToolbarModule,
  MatDialogModule,
  MatIconModule,
  MatListModule,
  MatProgressBarModule,
} from '@angular/material';
import { MatRippleModule } from '@angular/material/core';
import { IframeComponent } from './iframe/iframe.component';
import { UploadFileComponent } from './upload-file/upload-file.component';

import { UploadDialogComponent } from './upload-file/upload-dialog/upload-dialog.component';
// import { KibanaTemplateComponent } from '../embeddings/kibana-template/kibana-template.component';



// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { HttpClientModule } from '@angular/common/http';



@NgModule({
  imports: [
    CommonModule,
    TestsRoutingModule,
    FlexLayoutModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    MatRippleModule,
    MatListModule,
    MatProgressBarModule,
    MatDialogModule,
    // BrowserAnimationsModule,

  ],
  declarations: [HttpclientComponent, FlexlayoutComponent, AnimationComponent, IframeComponent, UploadFileComponent, UploadDialogComponent],
  entryComponents: [
    UploadDialogComponent,
  ]
})
export class TestsModule { }
