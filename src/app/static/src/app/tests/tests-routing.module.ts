import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpclientComponent } from './httpclient/httpclient.component';
import { FlexlayoutComponent } from './flexlayout/flexlayout.component';
import { UploadFileComponent } from "src/app/tests/upload-file/upload-file.component";
import { AnimationComponent } from './animation/animation.component';
import { IframeComponent } from './iframe/iframe.component';

const routes: Routes = [
  { path: 'httpclient', component: HttpclientComponent,  },
  { path: 'flexlayout', component: FlexlayoutComponent,data: { animation: 'isDashboard' }},
  {path: 'uploadfile', component: UploadFileComponent},
  // { path: 'animation', component: AnimationComponent, data: { animation: 'isDashboard' } },
  // { path: 'iframe', component: IframeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestsRoutingModule { }
