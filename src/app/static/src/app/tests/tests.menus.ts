import { ParentMenu } from '../models/menu.model';
export const menus: ParentMenu[] = [
    {
        'name': '測試',
        'icon': 'dashboard',
        'link': false,
        'authorized_to': -1,
        'sub': [
            {
                'name': '測試 FlexLayout',
                'link': 'tests/flexlayout',
                'icon': 'money',
                'authorized_to': -1,
            },
            {
                'name': '測試 HttpClient',
                'link': 'tests/httpclient',
                'icon': 'money',
                'authorized_to': -1,
            },
            // {
            //     'name': '測試 Animation',
            //     'link': 'tests/animation',
            //     'icon': 'money',
            //     'authorized_to': -1,
            // },
            // {
            //     'name': '測試 Iframe',
            //     'link': 'tests/iframe',
            //     'icon': 'money',
            //     'authorized_to': -1,
            // },
            {
                'name': '測試 UploadFile',
                'link': 'tests/uploadfile',
                'icon': 'cloud_upload',
                'authorized_to': -1,
            }
        ]
    },
    // {
    //     'name': 'Table',
    //     'icon': 'dashboard',
    //     'link': false,
    //     'sub': [
    //         {
    //             'name': 'fixed',
    //             'link': 'tables/fixed',
    //             'icon': 'money',
    //             'authorized_to': -1,
    //         },
    //         {
    //             'name': 'featured',
    //             'link': 'tables/featured',
    //             'icon': 'money',
    //             'authorized_to': -1,
    //         },
    //         {
    //             'name': 'responsive',
    //             'link': 'tables/responsive',
    //             'icon': 'money',
    //             'authorized_to': -1,
    //         },
    //         {
    //             'name': 'showdetail',
    //             'link': 'tables/showdetail',
    //             'icon': 'money',
    //             'authorized_to': -1,
    //         },
    //         {
    //             'name': 'general',
    //             'link': 'tables/general',
    //             'icon': 'money',
    //             'authorized_to': -1,
    //         },
    //     ]
    // },
]; 
