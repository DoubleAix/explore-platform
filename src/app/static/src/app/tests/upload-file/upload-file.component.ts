import { Component,OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UploadDialogComponent } from './upload-dialog/upload-dialog.component';
import { UploadFileService } from 'src/app/services/tests/upload-file/upload-file.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  constructor(public dialog: MatDialog, public uploadFileService: UploadFileService) { }

  ngOnInit() {
  }
  public openUploadDialog() {
    let dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '50%',
      height: '50%',
    })
  }
}
