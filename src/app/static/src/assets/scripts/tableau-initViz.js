function initViz(url) {
    
    var containerDiv = document.getElementById("vizContainer");
    
    // url = "http://{{tableau_url}}/trusted/{{ticket}}/t/{{site}}/views/{{dashboard}}",
    
    options = {
        // width: "100%",
        // height: calc(100% - 66px),
        hideTabs: true,
        hideToolbar: false,
        // toolbar:"top",
        onFirstInteractive: function () {
            console.log("Run this code when the viz has finished loading.");
        }
    };
    // version #2 (latest)
    // https://community.tableau.com/thread/142615
    // // viz is undefined on both the first and second page loads - even though second page load throws a 'viz already present' error
    let viz = window.tableau.VizManager.getVizs()[0];
    if (viz) {
      viz.dispose();
    }
    viz = new tableau.Viz(containerDiv, url, options);
    
    // version #1 (old)
    // if(viz != null) {
    //  viz.dispose();
    // }
    // var viz = new tableau.Viz(containerDiv, url, options); 

}