from flask import Blueprint,flash,redirect,request,current_app, jsonify
from werkzeug.utils import secure_filename
import os

# from app.main.config import Config
# from flask import current_app
# from app import blueprint

test = Blueprint('test',
                      __name__,)


@test.route('/upload/', methods=['GET', 'POST'])
def upload():
    if request.method == 'GET':
        return 'OKOK'
    if request.method == 'POST':
        print(request.files)
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
            return jsonify({"message":"OK"})
         
    