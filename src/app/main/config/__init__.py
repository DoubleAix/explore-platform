import os
from pathlib import Path
import datetime
from configparser import ConfigParser

BASE_DIR = Path(os.path.dirname(__file__)).parents[2]
STATIC_BASE_DIR = Path(os.path.dirname(__file__)).parents[1]

if os.name == 'nt':
    CONFIG_FILE_PATH ='D:\ods\odsai\config.ini'
else:  ## os.name =='posix'
    CONFIG_FILE_PATH = '/ods/odsai/config.ini'

parseconfig = ConfigParser()
parseconfig.read(CONFIG_FILE_PATH)


class Config:
    ## JWT
    JWT_SECRET_KEY = "DesignByAix"
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(seconds=1800)
    JWT_REFRESH_TOKEN_EXPIRES = datetime.timedelta(seconds=86400)
    JWT_IDENTITY_CLAIM = 'sub'

    ## SQLAlchemy
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"

    ## dashboard blueprint
    STATIC_FOLDER = os.path.join(STATIC_BASE_DIR, 'static/dist/prod')
    STATIC_FOLDER_DEV = os.path.join(STATIC_BASE_DIR, 'static/dist/dev')
    UPLOAD_FOLDER = os.path.join(STATIC_BASE_DIR, 'static/tmp/upload_file')


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(
        BASE_DIR, 'web_config.db')
    TABLEAU_LOCATION = "10.240.69.20"
    TABLEAU_USERNAME = "DS_TABAP"
 
    # LDAP_HOST = parseconfig['TEST']['LDAP']
    # IE_ACCOUNT = 'B0374'
    # IE_PASSWORD = 'Aa123456'

class TestingConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(
        BASE_DIR, 'web_config.db')
    TABLEAU_LOCATION = "10.240.69.20"
    TABLEAU_USERNAME = "DS_TABAP"

    # LDAP_HOST = parseconfig['TEST']['LDAP']
    # IE_ACCOUNT = 'B0374'
    # IE_PASSWORD = 'Aa123456'
    # SQLALCHEMY_DATABASE_URI = 'sqlite:////ods/web_config.db'
    # SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost:5432/exploreplatform'


class UserTestingConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(
        BASE_DIR, 'web_config.db')
    # LDAP_HOST = parseconfig['UAT']['LDAP']

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(
        BASE_DIR, 'web_config.db')
    # LDAP_HOST = parseconfig['PROD']['LDAP']


config_by_name = dict(DEFAULT=Config,
                      DEV=DevelopmentConfig,
                      TEST=TestingConfig,
                      UAT=UserTestingConfig,
                      PROD=ProductionConfig)
