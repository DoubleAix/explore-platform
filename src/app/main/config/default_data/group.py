reserved_group_list = [
    {
        'group_id': 'group1',
        'group_name': '管理群組',
        'reserved': True
    },
]

non_reserved_group_list = []
for i in range(10):
    non_reserved_group_list.append({
        'group_id': 'G{}'.format(i),
        'group_name': '群組{}'.format(i)
    })

group_list = reserved_group_list + non_reserved_group_list
