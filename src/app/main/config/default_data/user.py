reserved_user_list = [
    {
        "user_id": "root",
        "user_name": "最高權限",
        "email": "root@chen.com",
        "reserved": True
    },
]

non_reserved_user_list = []
for i in range(20):
    non_reserved_user_list.append({
        "user_id": 'U{}'.format(i),
        "user_name": '使用者{}'.format(i),
        "email": 'U{}.chen@googoo.com'.format(i),
    })

user_list = reserved_user_list + non_reserved_user_list