manage_parent_resource_list = [{
    'parent_resource_name':
    '管理',
    'type':
    "settings",
    'icon_name':
    'settings',
    'reserved':
    True,
    'authorization_type':
    'management',
    'label':
    'management',
    'children': [
        {
            'child_resource_name': '使用者管理',
            'type': "settings",
            'icon_name': "settings",
            'label': 'user_management',
            'data': {
                "link_name": "user_management",
            },
            'reserved': True,
            'authorization_type': "user_management"
        },
        {
            'child_resource_name': '資源管理',
            'type': "settings",
            'icon_name': "settings",
            'label': 'resource_management',
            'data': {
                "link_name": "resource_management",
            },
            'reserved': True,
            'authorization_type': "resource_management"
        },
        {
            'child_resource_name': '群組管理',
            'type': "settings",
            'icon_name': "settings",
            'label': 'group_management',
            'data': {
                "link_name": "group_management",
            },
            'reserved': True,
            'authorization_type': "group_management"
        },
        {
            'child_resource_name': '空間管理',
            'type': "settings",
            'icon_name': "settings",
            'label': 'space_management',
            'data': {
                "link_name": "space_management",
            },
            'reserved': True,
            'authorization_type': "space_management"
        },
    ]
}]

web_parent_resouce_list = []
for i in range(30):
    web_parent_resouce_list.append({
        'parent_resource_name':
        '主要資源{}'.format(i),
        'icon_name':
        'money',
        'children': [
            {
                'child_resource_name': '第四張DashBoard',
                'type': "embeddings",
                'icon_name': "dashboard",
                'label': 'kibana',
                'data': {
                    "kibana": {},
                    "tableau": {},
                }
            },
            {
                'child_resource_name': '第五張DashBoard',
                'type': "embeddings",
                'icon_name': "dashboard",
                'label': 'kibana',
                'data': {
                    "kibana": {},
                    "tableau": {},
                }
            },
            {
                'child_resource_name': '第六張DashBoard',
                'type': "embeddings",
                'icon_name': "dashboard",
                'label': 'kibana',
                'data': {
                    "kibana": {},
                    "tableau": {},
                }
            },
        ]
    })

root_resource_list = [
    {
        'root_resource_name': '_manage_root',
        'children': manage_parent_resource_list,
    },
    {
        'root_resource_name': '_web_root',
        'children': web_parent_resouce_list,
    },
]