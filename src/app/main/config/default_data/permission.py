permission_list = [
    {
        "name": "read"
    },
        {
        "name": "create"
    },
    {
        "name": "delete"
    },
    {
        "name": "update"
    },
    {
        "name": "notification"
    },
    {
        "name": "authorization"
    },
    {
        "name": "feedback"
    },
    {
        "name": "belong"
    },
    {
        "name": "read_child"
    },
    {
        "name": "create_child"
    },
    {
        "name": "delete_child"
    },
    {
        "name": "update_child"
    },
    {
        "name": "authorization_child"
    },
    {
        "name": "notification_child"
    },
    {
        "name": "feedback_child"
    },
    {
        "name": "owner"
    },
    {
        "name": "owner_read"
    },
        {
        "name": "owner_create"
    },
    {
        "name": "owner_update"
    },
]