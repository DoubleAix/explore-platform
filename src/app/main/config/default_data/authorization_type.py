authorization_type_list = [
    {
        "name":
        "group",
        "authroization_type_children": [],
        "permission_list": [
            # {
            #     'name': 'create_child',
            #     'label': '新增使用者',
            # },
            # {
            #     'name': 'delete_child',
            #     'label': '刪除使用者',
            # },
            # {
            #     'name': 'authorization',
            #     'label': '授權群組',
            # },
            {
                'name': 'owner_create',
                'label': '群組擁有者(更新)',
            },
            {
                'name': 'create',
                'label': '編輯群組使用者',
            },
            # {
            #     'name': 'delete',
            #     'label': '刪除群組',
            # },
        ],
    },
    {
        "name":
        "space",
        "authroization_type_children": [],
        "permission_list": [
            {
                'name': 'read',
                'label': '讀取空間',
            },
            # {
            #     'name': 'create_child',
            #     'label': '新增主要資源',
            # },
            # {
            #     'name': 'delete_child',
            #     'label': '刪除主要資源',
            # },
            # {
            #     'name': 'authorization',
            #     'label': '授權空間',
            # },
            {
                'name': 'owner_read',
                'label': '空間擁有者(讀取)',
            },
            # {
            #     'name': 'update',
            #     'label': '更新空間',
            # },
            # {
            #     'name': 'delete',
            #     'label': '刪除空間',
            # },
        ],
    },
    {
        "name":
        "management",
        "authroization_type_children": [
            'user_management', 'group_management', 'resource_management',
            'space_management'
        ],
        "permission_list": [
            # {
            #     'name': 'read',
            #     'label': '讀取管理頁面',
            # },
            {
                'name': 'owner',
                'label': '管理擁有者',
            },
            {
                'name': 'read_child',
                'label': '讀取所有管理子資源',
            },
            # {
            #     'name': 'authorization',
            #     'label': '授權管理權限',
            # },
            # {
            #     'name': 'authorization_child',
            #     'label': '授權管理子資源權限',
            # },
        ],
    },
    {
        "name":
        "user_management",
        "authroization_type_children": [],
        "permission_list": [
            {
                'name': 'read',
                'label': '讀取使用者管理頁面',
            },
            {
                'name': 'owner',
                'label': '使用者管理擁有者',
            },
            {
                'name': 'create_child',
                'label': '新增(修改)使用者',
            },
            # {
            #     'name': 'authorization',
            #     'label': '授權使用者管理權限',
            # },
            # {
            #     'name': 'update_child',
            #     'label': '更新使用者',
            # },
            {
                'name': 'delete_child',
                'label': '刪除使用者',
            },
        ],
    },
    {
        "name":
        "group_management",
        "authroization_type_children": ['group'],
        "permission_list": [
            {
                'name': 'read',
                'label': '讀取群組管理頁面',
            },
            {
                'name': 'owner',
                'label': '群組管理擁有者',
            },
            {
                'name': 'create_child',
                'label': '新增(更新)群組',
            },
            # {
            #     'name': 'authorization',
            #     'label': '授權群組管理權限',
            # },
            # {
            #     'name': 'update_child',
            #     'label': '更新群組',
            # },
            {
                'name': 'delete_child',
                'label': '刪除群組',
            },
            # {
            #     'name': 'authorization_child',
            #     'label': '授權群組權限',
            # },
        ],
    },
    {
        "name":
        "resource_management",
        "authroization_type_children": ['parent_resource'],
        "permission_list": [
            {
                'name': 'read',
                'label': '讀取資源管理頁面',
            },
            {
                'name': 'owner',
                'label': '資源管理擁有者',
            },
            # {
            #     'name': 'authorization',
            #     'label': '授權資源管理權限',
            # },
            {
                'name': 'create_child',
                'label': '新增(更新)主要資源',
            },
            # {
            #     'name': 'update_child',
            #     'label': '更新主要資源',
            # },
            {
                'name': 'delete_child',
                'label': '刪除主要資源',
            },
            # {
            #     'name': 'authorization_child',
            #     'label': '授權主要資源權限',
            # },
        ],
    },
    {
        "name":
        "space_management",
        "authroization_type_children": ['space'],
        "permission_list": [
            {
                'name': 'read',
                'label': '讀取空間管理頁面',
            },
            {
                'name': 'owner',
                'label': '空間管理擁有者',
            },
            # {
            #     'name': 'authorization',
            #     'label': '授權空間管理權限',
            # },
            {
                'name': 'create_child',
                'label': '新增(更新)空間',
            },
            {
                'name': 'read_child',
                'label': '讀取空間',
            },
            # {
            #     'name': 'update_child',
            #     'label': '更新空間',
            # },
            {
                'name': 'delete_child',
                'label': '刪除空間',
            },
            # {
            #     'name': 'authorization_child',
            #     'label': '授權空間權限',
            # },
        ],
    },
    {
        "name":
        "parent_resource",
        "authroization_type_children": ['child_resource', 'custom_resource'],
        "permission_list": [
            # {
            #     'name': 'read',
            #     'label': '讀取主要資源頁面',
            # },
            # {
            #     'name': 'update',
            #     'label': '更新主要資源',
            # },
            # {
            #     'name': 'delete',
            #     'label': '刪除主要資源',
            # },
            {
                'name': 'owner',
                'label': '主要資源擁有者',
            },
            {
                'name': 'owner_read',
                'label': '主要資源擁有者(讀取)',
            },
            # {
            #     'name': 'authorization',
            #     'label': '授權主要資源權限',
            # },
            {
                'name': 'read_child',
                'label': '讀取子資源',
            },
            {
                'name': 'create_child',
                'label': '新增(更新)子資源',
            },
            # {
            #     'name': 'update_child',
            #     'label': '更新子資源',
            # },
            {
                'name': 'delete_child',
                'label': '刪除子資源',
            },
            # {
            #     'name': 'authorization_child',
            #     'label': '授權子資源權限',
            # },
            # {
            #     'name': 'notification_child',
            #     'label': '通知子資源',
            # },
            # {
            #     'name': 'feedback_child',
            #     'label': '回饋子資源',
            # },
        ],
    },
    {
        "name":
        "child_resource",
        "authroization_type_children": [],
        "permission_list": [
            {
                'name': 'read',
                'label': '讀取子資源頁面',
            },
            # {
            #     'name': 'owner',
            #     'label': '子資源擁有者',
            # },
            # {
            #     'name': 'update',
            #     'label': '更新子資源',
            # },
            # {
            #     'name': 'delete',
            #     'label': '刪除子資源',
            # },
            # {
            #     'name': 'authorization',
            #     'label': '授權子資源權限',
            # },
            # {
            #     'name': 'notification',
            #     'label': '通知子資源',
            # },
            # {
            #     'name': 'feedback',
            #     'label': '回饋子資源',
            # },
        ],
    },
    {
        "name": "custom_resource",
        "authroization_type_children": [],
        "permission_list": [],
    }
]
