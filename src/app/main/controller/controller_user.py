from flask_restplus import Resource
from flask import request
from app.main.service.service_auth import Auth
from app.main.service.service_user import get_all_user, get_user_by_id, get_all_user_containing_string, create_user, update_user, delete_user
from app.main.dto.dto_user import UserManagement
from app.main.utils.parsers import parser_filtered_list

from app.main.utils.decorators import SettingsResourcePolicyRequired, CleanForm
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption

api = UserManagement.api
_user_fields = UserManagement.user_fields


@api.route('/')
class UserList(Resource):
    @api.marshal_with(_user_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.USER_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        return get_all_user(), 200
    
    @api.expect(_user_fields)
    @api.marshal_with(_user_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.USER_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def post(self):
        # data = request.json
        data = Auth.validate_if_account_exists(request.json)
        if data:
            user = create_user(data)
            if user:
                return user, 200
            else:
                api.abort(409)
        else:
            api.abort(404)
        
      
@api.route('/filtered_list')
class FilteredUserList(Resource):

    @api.expect(parser_filtered_list)
    @api.marshal_with(_user_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.USER_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        args = request.args
        return get_all_user_containing_string(args), 200


@api.route('/<id>')
class User(Resource):

    @api.marshal_with(_user_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.USER_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self, id):
        user = get_user_by_id(id)
        if not user:
            api.abort(404)
        else:
            return user, 200

    @api.expect(_user_fields)
    @api.marshal_with(_user_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.USER_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def put(self, id):
        data = request.json
        user = update_user(data, id)
        if user:
            return user, 200
        else:
            api.abort(404)

    @api.marshal_with(_user_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.USER_MANAGEMENT.value,
        PermissionOption.DELETE_CHILD.value)
    def delete(self, id):
        user = delete_user(id)
        if user:
            return user, 200
        else:
            api.abort(404)