from flask_restplus import Resource
from flask import request

from app.main.service.service_resource_policy import (get_all_member,
                                                      update_member,
                                                      delete_member)
from app.main.dto.dto_resource_policy import ResourcePolicyManagement
from app.main.dto.dto_user import UserManagement
from app.main.dto.dto_group import GroupManagement
from app.main.utils.decorators import AuthorizableResourcePolicyRequired
from app.main.utils.enums import UserType

api = ResourcePolicyManagement.api
_member_fields = UserManagement.user_fields
_group_fields = GroupManagement.group_fields


@api.route('/<id>/user/<permission>/')
class MemberList(Resource):
    user_type = UserType.USER.value


    @api.marshal_with(_member_fields)
    @AuthorizableResourcePolicyRequired()
    def get(self, id, permission):
        members = get_all_member(id, permission, self.user_type)
        if not isinstance(members, type(None)):
            return members, 200
        else:
            api.abort(404)

    @api.expect(_member_fields)
    @api.marshal_with(_member_fields)
    @AuthorizableResourcePolicyRequired()
    def post(self, id, permission):
        data = request.json
        members = update_member(data, id, permission, self.user_type)
        if not isinstance(members, type(None)):
            return members, 200
        else:
            api.abort(404)


@api.route('/<id>/user/<permission>/<member_id>')
class Member(Resource):
    user_type = UserType.USER.value

    @api.marshal_with(_member_fields)
    @AuthorizableResourcePolicyRequired()
    def delete(self, id, permission, member_id):
        member = delete_member(id, permission, member_id, self.user_type)
        if member:
            return member, 200
        else:
            api.abort(404)


@api.route('/<id>/group/<permission>/')
class GroupList(Resource):
    user_type = UserType.GROUP.value

    @api.marshal_with(_group_fields)
    # @AuthorizableResourcePolicyRequired()
    def get(self, id, permission):
        groups = get_all_member(id, permission, self.user_type)
        if not isinstance(groups, type(None)):
            return groups, 200
        else:
            api.abort(404)

    @api.expect(_group_fields)
    @api.marshal_with(_group_fields)
    # @AuthorizableResourcePolicyRequired()
    def post(self, id, permission):
        data = request.json
        groups = update_member(data, id, permission, self.user_type)
        if not isinstance(groups, type(None)):
            return groups, 200
        else:
            api.abort(404)


@api.route('/<id>/group/<permission>/<group_id>')
class Group(Resource):
    user_type = UserType.GROUP.value

    @api.marshal_with(_group_fields)
    # @AuthorizableResourcePolicyRequired()
    def delete(self, id, permission, group_id):
        group = delete_member(id, permission, group_id, self.user_type)
        if group:
            return group, 200
        else:
            api.abort(404)