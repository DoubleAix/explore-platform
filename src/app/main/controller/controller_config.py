from flask_restplus import Resource
from flask import request, current_app
from flask_jwt_extended import jwt_required

from app.main.dto.dto_config import ConfigDto
from app.main.dto.dto_permission import PermissionDto
from app.main.service.service_authorization_type import get_all_aatp_by_authorization_type_id
from app.main.service.service_permission import get_all_permission
from app.main.service.service_root_resource import get_all_root_resource
from app.main.service.service_space_resource import get_all_resource_by_space_id
from app.main.service.service_resource import get_all_settings_resource
from app.main.utils.decorators import SettingsResourcePolicyRequired

api = ConfigDto.api
_menu_fields = ConfigDto.menu_fields
_authorization_option_fields = ConfigDto.authorization_option_fields
_settings_resource_policy_mapping = ConfigDto.settings_resource_policy_mapping
_permission_fields = PermissionDto.permission_fields


@api.route('/permission_list')
class PermissionList(Resource):
    @api.marshal_with(_permission_fields)
    def get(self):
        return get_all_permission(), 200


@api.route('/menu_list/<space_id>')
class MenuList(Resource):
    @jwt_required
    @api.marshal_with(_menu_fields)
    def get(self,space_id):
        return get_all_resource_by_space_id(space_id), 200


@api.route('/authorization_option_list/<id>')
class AuthorizationOptionList(Resource):
    @jwt_required
    @api.marshal_with(_authorization_option_fields)
    def get(self, id):
        return get_all_aatp_by_authorization_type_id(id), 200


@api.route('/settings_resource_policy_mapping')
class SettingsResourcePolicyMapping(Resource):
    @api.marshal_with(_settings_resource_policy_mapping)
    def get(self):

        return get_all_settings_resource(), 200
