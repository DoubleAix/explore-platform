from flask_restplus import Resource
from flask import request

from app.main.service.service_child_resource import (get_all_child_resource,
                                                     get_child_resource_by_id,
                                                     create_child_resource,
                                                     update_child_resource,
                                                     delete_child_resource)
from app.main.service.service_resource_policy import (get_all_member,
                                                      update_member,
                                                      delete_member)
from app.main.utils.parsers import parser_parent_resource_id
from app.main.dto.dto_child_resource import ChildResourceManagement
from app.main.dto.dto_user import UserManagement
from app.main.dto.dto_group import GroupManagement

from app.main.utils.decorators import SettingsResourcePolicyRequired,ParentResourcePolicyRequired, ResourcePolicyRequired, CleanForm
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption

api = ChildResourceManagement.api
_child_resource_fields = ChildResourceManagement.child_resource_fields
_member_fields = UserManagement.user_fields
_group_fields = GroupManagement.group_fields


@api.route('/')
class ChildResourceList(Resource):
   
    @api.expect(parser_parent_resource_id)
    @api.marshal_with(_child_resource_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.RESOURCE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        args = request.args
        return get_all_child_resource(args), 200

    @api.expect(_child_resource_fields, parser_parent_resource_id)
    @api.marshal_with(_child_resource_fields)
    @ResourcePolicyRequired(PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def post(self):
        data = request.json
        args = request.args
        child_resource = create_child_resource(data, args)
        if child_resource:
            return child_resource, 200
        else:
            api.abort(409)


@api.route('/<id>')
class ChildResource(Resource):
   
    @api.marshal_with(_child_resource_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.RESOURCE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self, id):
        resource = get_child_resource_by_id(id)
        if not resource:
            api.abort(404)
        else:
            return resource, 200


    @api.expect(_child_resource_fields)
    @api.marshal_with(_child_resource_fields)
    # @ResourcePolicyRequired(PermissionOption.UPDATE.value)
    @ParentResourcePolicyRequired(PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def put(self, id):
        data = request.json
        resource = update_child_resource(data, id)
        if resource:
            return resource, 200
        else:
            api.abort(404)

    @api.marshal_with(_child_resource_fields)
    # @ResourcePolicyRequired(PermissionOption.DELETE.value)
    @ParentResourcePolicyRequired(PermissionOption.DELETE_CHILD.value)
    def delete(self, id):
        resource = delete_child_resource(id)
        if resource:
            return resource, 200
        else:
            api.abort(404)
