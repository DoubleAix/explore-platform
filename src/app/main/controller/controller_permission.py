from app.main.dto.dto_permission import PermissionDto
from app.main.service.service_permission import get_all_permission
from flask_restplus import Resource

api = PermissionDto.api
