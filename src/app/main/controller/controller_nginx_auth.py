from flask import Blueprint, request, jsonify
from app import blueprint

nginx_auth = Blueprint('nginx_auth', __name__)


@nginx_auth.route('/auth_kibana', methods=['GET'])
def auth_kibana():
    print(request.args)
    print(request.headers)
    return jsonify({"message": "OK"}), 200

# @nginx_auth.route('/test')
# def test():
#     print(request.args)
#     print(request.headers)
#     return jsonify({"message":"test"}), 200
