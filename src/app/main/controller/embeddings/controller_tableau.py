from flask import request
from flask_restplus import Resource

from app.main.service.embeddings.service_tableau import get_dashboard_url
from app.main.dto.embeddings.dto_tableau import TableauDto
from app.main.utils.decorators import ResourcePolicyRequired
from app.main.utils.enums import PermissionOption

api = TableauDto.api
_tableau_fields = TableauDto.tableau_fields

@api.route('/<id>')
class TableauDashboard(Resource):


    @api.marshal_with(_tableau_fields)
    @ResourcePolicyRequired(PermissionOption.READ.value)
    def get(self, id):
        response = get_dashboard_url(id)
        if response:
             return response, 200
        else:
            api.abort(404)