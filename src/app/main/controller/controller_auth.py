from flask import request, jsonify
from flask_restplus import Resource
from flask_jwt_extended import get_jwt_identity, get_jwt_claims, jwt_refresh_token_required, jwt_required
import time

from app.main import jwt_manager

from app.main.service.service_auth import Auth
from app.main.dto.dto_auth import AuthDto
from app.main.utils.parsers import parser_jwt, parser_user_policy_type

api = AuthDto.api
_request_body = AuthDto.request_body


@api.route('/login')
class Login(Resource):
    @api.expect(parser_jwt)
    @jwt_required
    def get(self):
        response = {
            'current_user_id': get_jwt_identity(),
            'current_claims': get_jwt_claims()
        }
        return response, 200

    @api.response(200, 'Login Success')
    @api.response(401, 'Login Failure: Bad Username or Password')
    @api.expect(_request_body)
    def post(self):
        data = request.json

        if Auth.authetication(data):
            response = Auth.login(data)
            return response, 200
        else:
            api.abort(401)


@api.route('/refresh')
class Refresh(Resource):
    @api.response(200, 'Refresh Success')
    @api.expect(parser_jwt, parser_user_policy_type)
    # @api.expect(parser_user_policy_type)
    @jwt_refresh_token_required
    def post(self):
        data = request.json
        response = Auth.refresh(data)
        
        return response, 200


# @api.route('/user_policies')
# class UserPolicies(Resource):
#     @jwt_required
#     @api.expect(parser_user_policy_type)
#     def get(self):
#         args = request.args
#         response = Auth.get_user_policies(args)
#         if response:
#             return response, 200
#         else:
#             api.abort(404)
