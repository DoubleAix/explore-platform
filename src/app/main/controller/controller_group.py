from flask_restplus import Resource
from flask import request

from app.main.service.service_group import (
    get_all_group,
    get_group_by_id,
    create_group,
    update_group,
    delete_group,
    get_all_group_containing_string,
)
from app.main.service.service_group_policy import (delete_member,
                                                   update_member, get_all_member)
from app.main.dto.dto_group import GroupManagement
from app.main.utils.parsers import parser_filtered_list

from app.main.utils.decorators import SettingsResourcePolicyRequired, GroupPolicyRequired, CleanForm
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption

api = GroupManagement.api
_group_fields = GroupManagement.group_fields



@api.route('/')
class GroupList(Resource):

    @api.marshal_with(_group_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.GROUP_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        return get_all_group(), 200


    @api.expect(_group_fields)
    @api.marshal_with(_group_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.GROUP_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def post(self):
        data = request.json
        group = create_group(data)
        if group:
            return group, 200
        else:
            api.abort(409)


@api.route('/filtered_list')
class FilteredGroupList(Resource):

    @api.expect(parser_filtered_list)
    @api.marshal_with(_group_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.GROUP_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        args = request.args
        return get_all_group_containing_string(args), 200


@api.route('/<id>')
class Group(Resource):

    @api.marshal_with(_group_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.GROUP_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self, id):
        group = get_group_by_id(id)
        if not group:
            api.abort(404)
        else:
            return group, 200


    @api.expect(_group_fields)
    @api.marshal_with(_group_fields)
    # @GroupPolicyRequired(PermissionOption.UPDATE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.GROUP_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def put(self, id):
        data = request.json
        group = update_group(data, id)
        if group:
            return group, 200
        else:
            api.abort(404)


    @api.marshal_with(_group_fields)
    # @GroupPolicyRequired(PermissionOption.DELETE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.GROUP_MANAGEMENT.value,
        PermissionOption.DELETE_CHILD.value)
    def delete(self, id):
        group = delete_group(id)
        if group:
            return group, 200
        else:
            api.abort(404)
