from flask_restplus import Resource
from flask import request

from app.main.dto.dto_group_policy import GroupPolicyManagement
from app.main.dto.dto_group import GroupManagement
from app.main.dto.dto_user import UserManagement
from app.main.utils.decorators import AuthorizableGroupPolicyRequired

from app.main.service.service_group_policy import (delete_member,
                                                   update_member, get_all_member)

api = GroupPolicyManagement.api
_group_fields = GroupManagement.group_fields
_member_fields = UserManagement.user_fields


@api.route('/<id>/<permission>/')
class MemberList(Resource):

    @api.marshal_with(_member_fields)
    @AuthorizableGroupPolicyRequired()
    def get(self, id, permission):
        members = get_all_member(id, permission)
        if not isinstance(members, type(None)):
            return members, 200
        else:
            api.abort(404)


    @api.expect(_member_fields)
    @api.marshal_with(_member_fields)
    @AuthorizableGroupPolicyRequired()
    def post(self, id, permission):
        data = request.json
        members = update_member(data, id, permission)
        if not isinstance(members, type(None)):
            return members, 200
        else:
            api.abort(404)


@api.route('/<id>/<permission>/<member_id>')
class Member(Resource):

    @api.marshal_with(_member_fields)
    @AuthorizableGroupPolicyRequired()
    def delete(self, id, permission, member_id):
        member = delete_member(id, permission, member_id)
        if member:
            return member, 200
        else:
            api.abort(404)