from flask_restplus import Resource
from flask import request

from app.main.service.service_parent_resource import (
    get_all_parent_resource, get_parent_resource_by_id, create_parent_resource,
    update_parent_resource, delete_parent_resource)
from app.main.service.service_resource_policy import (get_all_member,
                                                      update_member,
                                                      delete_member)
from app.main.dto.dto_parent_resource import ParentResourceManagement
from app.main.dto.dto_user import UserManagement
from app.main.dto.dto_group import GroupManagement

from app.main.utils.decorators import SettingsResourcePolicyRequired, ResourcePolicyRequired, CleanForm
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption

api = ParentResourceManagement.api
_parent_resource_fields = ParentResourceManagement.parent_resource_fields
_member_fields = UserManagement.user_fields
_group_fields = GroupManagement.group_fields


@api.route('/')
class ParentResourceList(Resource):

    @api.marshal_with(_parent_resource_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.RESOURCE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        return get_all_parent_resource(), 200


    @api.expect(_parent_resource_fields)
    @api.marshal_with(_parent_resource_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.RESOURCE_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def post(self):
        data = request.json
        parent_resource = create_parent_resource(data)
        if parent_resource:
            return parent_resource, 200
        else:
            api.abort(409)


@api.route('/<id>')
class ParentResource(Resource):

    @api.marshal_with(_parent_resource_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.RESOURCE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self, id):
        resource = get_parent_resource_by_id(id)
        if not resource:
            api.abort(404)
        else:
            return resource, 200


    @api.expect(_parent_resource_fields)
    @api.marshal_with(_parent_resource_fields)
    # @ResourcePolicyRequired(PermissionOption.UPDATE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.RESOURCE_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def put(self, id):
        data = request.json
        resource = update_parent_resource(data, id)
        if resource:
            return resource, 200
        else:
            api.abort(404)

    @api.marshal_with(_parent_resource_fields)
    # @ResourcePolicyRequired(PermissionOption.DELETE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.RESOURCE_MANAGEMENT.value,
        PermissionOption.DELETE_CHILD.value)
    def delete(self, id):
        parent_resource = delete_parent_resource(id)
        if parent_resource:
            return parent_resource, 200
        else:
            api.abort(404)
