from flask_restplus import Resource
from flask import request

from app.main.dto.dto_space_resource import SpaceResourceManagement
from app.main.utils.decorators import SpacePolicyRequired, SettingsResourcePolicyRequired
from app.main.service.service_space_resource import (
    get_all_resource_by_id, update_all_resource)
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption

api = SpaceResourceManagement.api
_space_resources_fields = SpaceResourceManagement.space_resources_fields
_space_resource_fields = SpaceResourceManagement.space_resource_fields


@api.route('/<id>')
class SpaceResourceList(Resource):
    
    @api.marshal_with(_space_resources_fields)
    # @SpacePolicyRequired(PermissionOption.UPDATE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    def get(self, id):
        response = get_all_resource_by_id(id)
        if not isinstance(response, type(None)):
            return response, 200
        else:
            api.abort(404)

    @api.expect(_space_resource_fields)
    @api.marshal_with(_space_resources_fields)
    # @SpacePolicyRequired(PermissionOption.UPDATE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    def post(self, id):
        data = request.json
        response = update_all_resource(data, id)
        if not isinstance(response, type(None)):
            return response, 200
        else:
            api.abort(404)
