from flask_restplus import Resource
from flask import request

from app.main.service.service_space import (
    get_all_space,
    get_space_by_id,
    create_space,
    update_space,
    delete_space,
    get_all_space_containing_string,
    get_space_by_space_id,
)

from app.main.dto.dto_space import SpaceManagement
from app.main.dto.dto_user import UserManagement
from app.main.utils.parsers import parser_filtered_list

from app.main.utils.decorators import SettingsResourcePolicyRequired, SpacePolicyRequired
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption
from app.main.utils.decorators import CleanForm

api = SpaceManagement.api
_space_fields = SpaceManagement.space_fields


@api.route('/')
class SpaceList(Resource):
    @api.marshal_with(_space_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        return get_all_space(), 200

    @api.expect(_space_fields)
    @api.marshal_with(_space_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def post(self):
        data = request.json
        space = create_space(data)
        if space:
            return space, 200
        else:
            api.abort(409)


@api.route('/filtered_list')
class FilteredSpaceList(Resource):
    @api.expect(parser_filtered_list)
    @api.marshal_with(_space_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self):
        args = request.args
        return get_all_space_containing_string(args), 200


@api.route('/space_by_space_id/<space_id>')
class SpaceBySpaceId(Resource):
    @api.marshal_with(_space_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self, space_id):
        space = get_space_by_space_id(space_id)
        if not space:
            api.abort(404)
        else:
            return space, 200


@api.route('/<id>')
class Space(Resource):
    @api.marshal_with(_space_fields)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.READ.value)
    def get(self, id):
        space = get_space_by_id(id)
        if not space:
            api.abort(404)
        else:
            return space, 200

    @api.expect(_space_fields)
    @api.marshal_with(_space_fields)
    # @SpacePolicyRequired(PermissionOption.UPDATE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.CREATE_CHILD.value)
    @CleanForm()
    def put(self, id):
        data = request.json
        space = update_space(data, id)
        if space:
            return space, 200
        else:
            api.abort(404)

    @api.marshal_with(_space_fields)
    # @SpacePolicyRequired(PermissionOption.DELETE.value)
    @SettingsResourcePolicyRequired(
        AuthorizationTypeOption.SPACE_MANAGEMENT.value,
        PermissionOption.DELETE_CHILD.value)
    def delete(self, id):
        space = delete_space(id)
        if space:
            return space, 200
        else:
            api.abort(404)