from enum import Enum


class PermissionOption(Enum):
    UNDERSCORE = '_'
    CHILD_SUFFIX =  UNDERSCORE + 'child'
    READ = 'read'
    CREATE = 'create'
    DELETE = 'delete'
    UPDATE = 'update'
    NOTIFICATION = 'notification'
    AUTHORIZATION = 'authorization'
    FEEDBACK = 'feedback'
    BELONG = 'belong'
    OWNER = 'owner'
    READ_CHILD = READ + CHILD_SUFFIX
    CREATE_CHILD = CREATE + CHILD_SUFFIX
    DELETE_CHILD = DELETE + CHILD_SUFFIX
    UPDATE_CHILD = UPDATE + CHILD_SUFFIX
    NOTIFICATION_CHILD = NOTIFICATION + CHILD_SUFFIX
    AUTHORIZATION_CHILD = AUTHORIZATION + CHILD_SUFFIX
    FEEDBACK_CHILD = FEEDBACK + CHILD_SUFFIX
    OWNER_READ = OWNER + UNDERSCORE + READ
    OWNER_UPDATE = OWNER + UNDERSCORE + UPDATE
    OWNER_CREATE = OWNER + UNDERSCORE + CREATE

class UserPolicyType(Enum):
    RESOURCE_POLICY = 'resource_policies'
    GROUP_POLICY = 'group_policies'
    SPACE_POLICY = 'space_policies'
    AUTHORIZABLE_RESOURCE_POLICY = 'authorizable_resource_policies'
    AUTHORIZABLE_GROUP_POLICY = 'authorizable_group_policies'
    AUTHORIZABLE_SPACE_POLICY = 'authorizable_space_policies'

class UserType(Enum):
    USER = 'user'
    GROUP = 'group'


class ResourceLayer(Enum):
    FILE = 'file'
    FOLDER = 'folder'
    ROOT = 'root'


class ResourceType(Enum):
    SETTINGS = 'settings'
    EMBEDDINGS = 'embeddings'


class AuthorizationTypeOption(Enum):
    GROUP = 'group'
    SPACE = 'space'
    MANAGEMENT = 'management'
    USER_MANAGEMENT = 'user_management'
    RESOURCE_MANAGEMENT = 'resource_management'
    GROUP_MANAGEMENT = 'group_management'
    SPACE_MANAGEMENT = 'space_management'
    PARENT_RESOURCE = 'parent_resource'
    CHILD_RESOURCE = 'child_resource'
    CUSTOM_RESOURCE = "custom_resource"
