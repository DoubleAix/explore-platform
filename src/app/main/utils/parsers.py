## to verify other info of the request (rather than JSON body)

from flask_restplus import reqparse

## parser for JWT header
parser_jwt = reqparse.RequestParser(bundle_errors=True)
parser_jwt.add_argument('Authorization',
                        required=True,
                        type=str,
                        location='headers',
                        help='Bearer [token]')

## filtered users list parser
parser_filtered_list = reqparse.RequestParser(bundle_errors=True)
parser_filtered_list.add_argument('search_string', type=str, location='args')

## authorization option type parser
parser_authorization_type = reqparse.RequestParser(bundle_errors=True)
parser_authorization_type.add_argument('authorization_type',
                                       type=str,
                                       location='args')

## parent resource id parser
parser_parent_resource_id = reqparse.RequestParser(bundle_errors=True)
parser_parent_resource_id.add_argument('parent_resource_id',
                                       type=str,
                                       location='args')
## user policy type parser
parser_user_policy_type = reqparse.RequestParser(bundle_errors=True)
parser_user_policy_type.add_argument('user_policy_type',
                                     required=False,
                                     type=list,
                                     location='json')


