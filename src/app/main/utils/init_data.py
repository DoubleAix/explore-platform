from app.main.service.service_group import create_group
from app.main.service.service_group_policy import update_member as update_member_group
from app.main.service.service_permission import create_permission
from app.main.service.service_user import create_user
from app.main.service.service_root_resource import create_root_resource
from app.main.service.service_parent_resource import create_parent_resource, get_parent_resource_by_name
from app.main.service.service_child_resource import create_child_resource
from app.main.service.service_resource_policy import update_member as update_member_resource_policy
from app.main.service.service_authorization_type import (
    create_authorization_type_authorizable_hierarchy,
    create_authorization_type, create_authorization_type_ownable_hierarchy,
    get_all_aatp_name_by_authorization_type_name)
from app.main.service.service_space import create_space
from app.main.service.service_space_policy import update_member as update_member_space_policy
from app.main.service.service_space_resource import update_all_resource
from app.main.config.default_data.group import group_list
from app.main.config.default_data.user import user_list
from app.main.config.default_data.permission import permission_list
from app.main.config.default_data.resource import root_resource_list
from app.main.config.default_data.authorization_type import authorization_type_list
from app.main.config.default_data.space import space_list

from app.main.utils.enums import UserType, PermissionOption, AuthorizationTypeOption
from app.main import db


def init_config(app):
    app.config['DEBUG'] = True
    reserved_user_id_list = []
    reserved_group_id_list = []

    try:
        for permission in permission_list:
            permission_obj = create_permission(permission)
            app.logger.info("權限 " + permission_obj.name + " 已經被建立")

        for authorization_type in authorization_type_list:
            create_authorization_type(authorization_type)
            app.logger.info("授權類別 " + authorization_type['name'] + " 已經被建立")

        ## create hierarchy
        for authorization_type in authorization_type_list:
            create_authorization_type_ownable_hierarchy(authorization_type)
            create_authorization_type_authorizable_hierarchy(
                authorization_type)

        for user in user_list:
            user_obj = create_user(user)
            app.logger.info("使用者 " + user_obj.user_name + " 已經被建立")
            if user_obj.reserved:
                reserved_user_id_list.append({"id": user_obj.id})

        group_permission_options = get_all_aatp_name_by_authorization_type_name(
            AuthorizationTypeOption.GROUP.value)
        group_permission_options.append(PermissionOption.BELONG.value)
        for group in group_list:
            group_obj = create_group(group)
            app.logger.info("群組 " + group_obj.user_name + " 已經被建立")
            if group_obj.reserved:

                for permission in group_permission_options:
                    update_member_group(reserved_user_id_list, group_obj.id,
                                        permission)
                reserved_group_id_list.append({"id": group_obj.id})

        for root_resource in root_resource_list:
            root_obj = create_root_resource(root_resource)
            for parent_resource in root_resource['children']:
                if root_resource['root_resource_name'] == '_manage_root':
                    parent_resource['parent_id'] = root_obj.id

                parent_obj = create_parent_resource(parent_resource)
                app.logger.info("主要資源 " + parent_obj.name + " 已經被建立")
                if parent_obj.reserved:
                    for permission in get_all_aatp_name_by_authorization_type_name(
                            parent_obj.authorization_type.name):
                        update_member_resource_policy(reserved_user_id_list,
                                                      parent_obj.id,
                                                      permission,
                                                      UserType.USER.value)
                        update_member_resource_policy(reserved_group_id_list,
                                                      parent_obj.id,
                                                      permission,
                                                      UserType.GROUP.value)
                for child_resource in parent_resource['children']:
                    child_resource['parent_resource_id'] = parent_obj.id
                    child_obj = create_child_resource(child_resource)
                    app.logger.info("子資源 " + child_obj.name + " 已經被建立")
                    if child_obj.reserved:
                        for permission in get_all_aatp_name_by_authorization_type_name(
                                child_obj.authorization_type.name):
                            update_member_resource_policy(
                                reserved_user_id_list, child_obj.id,
                                permission, UserType.USER.value)
                            update_member_resource_policy(
                                reserved_group_id_list, child_obj.id,
                                permission, UserType.GROUP.value)

        for space in space_list:
            space_obj = create_space(space)
            app.logger.info("空間 " + space_obj.name + " 已經被建立")
            if space_obj.reserved:
                for permission in get_all_aatp_name_by_authorization_type_name(
                        AuthorizationTypeOption.SPACE.value):
                    update_member_space_policy(reserved_user_id_list,
                                               space_obj.id, permission,
                                               UserType.USER.value)
                    update_member_space_policy(reserved_group_id_list,
                                               space_obj.id, permission,
                                               UserType.GROUP.value)
                resource_list = []
                for resource in space['resources']:
                    resource_id = get_parent_resource_by_name(
                        resource['resource_name']).id
                    resource_list.append({'id': resource_id})
                    update_all_resource(resource_list, space_obj.id)

    except Exception as e:
        app.logger.exception(e)
        db.session.rollback()
