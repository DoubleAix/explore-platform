# from functools import wraps
import re
import flask
from flask import request
## https://github.com/vimalloc/flask-jwt-extended/blob/ea98fccf65981ac0516329c0b78bb9a25d7011e3/flask_jwt_extended/default_callbacks.py
## The error handling using flask-jwt-extended only returns  status code 401 or 422.
## So, we just need to define status code 403 in our custom decorator.
from flask_jwt_extended.view_decorators import verify_jwt_in_request
from flask_jwt_extended import get_jwt_claims

from app.main.utils.enums import PermissionOption, AuthorizationTypeOption
from app.main.service.service_resource_policy import get_resource_policy_id_by_label, get_parent_resource_policy_id_by_resource_id,get_resource_policy_id_by_resource_id
from app.main.service.service_group_policy import get_group_policy_by_group_id_and_permission_name
from app.main.service.service_space_policy import get_space_policy_by_space_id_and_permission_name

MESSAGE_403 = "You have no authorization for this route."
MESSAGE_422 = "Something necessary is required to set."


class SettingsResourcePolicyRequired(object):
    def __init__(self, label_name, permission_name):
        self.label_name = label_name
        self.permission_name = permission_name
        self.resource_policy_id = None

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()

            if self.resource_policy_id is None:
                self.resource_policy_id = get_resource_policy_id_by_label(
                    self.label_name, self.permission_name).id

            if _validate_policies(_get_resource_policies(),
                                  self.resource_policy_id):
                return f(*args, **kargs)
            else:
                _error_handling_403()

        return wrapper


class GroupPolicyRequired(object):
    def __init__(self, permission_name):
        self.permission_name = permission_name
        # self.group_policy_id = None

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()

            if 'id' in kargs:
                group_policy_id = get_group_policy_by_group_id_and_permission_name(
                    kargs['id'], self.permission_name).id
            else:
                _error_handling_422()

            if _validate_policies(_get_group_policies(), group_policy_id):
                return f(*args, **kargs)
            else:
                _error_handling_403()

        return wrapper


class ResourcePolicyRequired(object):
    def __init__(self, permission_name):
        self.permission_name = permission_name

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()

            if 'id' in kargs:
                resource_policy_id = get_resource_policy_id_by_resource_id(
                    kargs['id'], self.permission_name).id
            elif 'parent_resource_id' in request.args:

                resource_policy_id = get_resource_policy_id_by_resource_id(
                    request.args['parent_resource_id'],
                    self.permission_name).id
            else:
                _error_handling_422()

            if _validate_policies(_get_resource_policies(),
                                  resource_policy_id):
                return f(*args, **kargs)
            else:
                _error_handling_403()

        return wrapper

class ParentResourcePolicyRequired(object):
    def __init__(self, permission_name):
        self.permission_name = permission_name

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()

            if 'id' in kargs:
                resource_policy_id = get_parent_resource_policy_id_by_resource_id(
                    kargs['id'], self.permission_name).id
                print(resource_policy_id)
            else:
                _error_handling_422()

            if _validate_policies(_get_resource_policies(),
                                  resource_policy_id):
                return f(*args, **kargs)
            else:
                _error_handling_403()

        return wrapper

class SpacePolicyRequired(object):
    def __init__(self, permission_name):
        self.permission_name = permission_name
        # self.group_policy_id = None

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()

            if 'id' in kargs:
                space_policy_id = get_space_policy_by_space_id_and_permission_name(
                    kargs['id'], self.permission_name).id
            else:
                _error_handling_422()

            if _validate_policies(_get_space_policies(), space_policy_id):
                return f(*args, **kargs)
            else:
                _error_handling_403()

        return wrapper

class AuthorizableGroupPolicyRequired(object):
    ## cache issue
    # def __init__(self):
    #     pass

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()

            ## the authorization of updating group member is create (not belong).
            ## 20190811 update => create 
            if 'permission' in kargs and 'id' in kargs and kargs[
                    'permission'] == PermissionOption.BELONG.value:
                group_policy_id = get_group_policy_by_group_id_and_permission_name(
                    kargs['id'], PermissionOption.CREATE.value).id

                if _validate_policies(_get_group_policies(), group_policy_id):
                    return f(*args, **kargs)
                else:
                    _error_handling_403()

            if 'id' in kargs and 'permission' in kargs:
                group_policy_id = get_group_policy_by_group_id_and_permission_name(
                    kargs['id'], kargs['permission']).id
            else:
                _error_handling_422()

            if _validate_policies(_get_authorizable_group_policies(),
                                  group_policy_id):
                return f(*args, **kargs)

            else:
                _error_handling_403()

        return wrapper


class AuthorizableResourcePolicyRequired(object):
    ## cache issue
    # def __init__(self):
    #     pass

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()
            if 'id' in kargs and 'permission' in kargs:
                resource_policy_id = get_resource_policy_id_by_resource_id(
                    kargs['id'], kargs['permission']).id
            else:
                _error_handling_422()

            if _validate_policies(_get_authorizable_resource_policies(),
                                  resource_policy_id):
                return f(*args, **kargs)

            else:
                _error_handling_403()

        return wrapper

class AuthorizableSpacePolicyRequired(object):
    ## cache issue
    # def __init__(self):
    #     pass

    def __call__(self, f):
        def wrapper(*args, **kargs):
            verify_jwt_in_request()
            if 'id' in kargs and 'permission' in kargs:
                space_policy_id = get_space_policy_by_space_id_and_permission_name(
                    kargs['id'], kargs['permission']).id
            else:
                _error_handling_422()

            if _validate_policies(_get_authorizable_space_policies(),
                                  space_policy_id):
                return f(*args, **kargs)

            else:
                _error_handling_403()

        return wrapper

def _validate_policies(policy_list, policy_id):
    return policy_id in policy_list


def _get_resource_policies():
    return get_jwt_claims()['resource_policies']


def _get_group_policies():
    return get_jwt_claims()['group_policies']

def _get_space_policies():
    return get_jwt_claims()['space_policies']

def _get_authorizable_resource_policies():
    return get_jwt_claims()['authorizable_resource_policies']

def _get_authorizable_group_policies():
    return get_jwt_claims()['authorizable_group_policies']

def _get_authorizable_space_policies():
    return get_jwt_claims()['authorizable_space_policies']

def _error_handling_403():
    return flask.abort(403, MESSAGE_403)


def _error_handling_422():
    return flask.abort(422, MESSAGE_422)


## trim string & remove multiple whitespaces & convert fullwidth to halfwidth whitespace
class CleanForm(object):
    def __call__(self, f):
        def wrapper(*args, **kargs):
            if request.json:
                for k, v in request.json.items():
                    if type(v) == str:
                        request.json[k] = re.sub(' +', ' ',
                                                 v.replace('　', ' ')).strip()
            return f(*args, **kargs)

        return wrapper