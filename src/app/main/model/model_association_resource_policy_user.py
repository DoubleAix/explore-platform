from app.main import db

resource_policy_user = db.Table(
    'association_resource_policy_user',
    db.Column('policy_id', db.Integer, db.ForeignKey('resource_policy.id'), index=True),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), index=True),
)