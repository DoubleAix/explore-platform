from app.main.model.model_association_auto_demo_resource import auto_demo_resource
from app.main.model.model_association_group_policy_user import group_policy_user
from app.main.model.model_association_resource_policy_user import resource_policy_user
from app.main.model.model_association_space_policy_user import space_policy_user
from app.main.model.model_association_space_resource import AssociationSpaceResource
from app.main.model.model_association_authorization_type_permission import AssociationAuthorizationTypePermission
from app.main.model.model_association_authorizable_permission import authorizable_permission
from app.main.model.model_association_resource_tag import resource_tag

from app.main.model.model_group_policy import GroupPolicy
from app.main.model.model_permission import Permission
from app.main.model.model_resource import Resource
from app.main.model.model_resource_policy import ResourcePolicy
from app.main.model.model_space import Space
from app.main.model.model_space_policy import SpacePolicy
from app.main.model.model_user import User
from app.main.model.model_tag import Tag
from app.main.model.model_authorization_type import AuthorizationType