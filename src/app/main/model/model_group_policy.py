from app.main import db
from sqlalchemy.orm import validates

# from app.main.model.model_association_group_policy_user import group_policy_user


class GroupPolicy(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    user = db.relationship("User",
                           secondary='association_group_policy_user',
                           backref=db.backref("group_policies_user_own",
                                              lazy="dynamic")
                                              )

    group = db.relationship('User',
                            backref=db.backref("group_policies_group_own",
                                               lazy="dynamic",
                                               cascade="all, delete-orphan"))

    permission = db.relationship('Permission',
                                 backref=db.backref("group_policies",
                                                    lazy="dynamic"))

    group_id = db.Column('group_id',
                         db.Integer,
                         db.ForeignKey('user.id'),
                         nullable=False, index=True)

    permission_id = db.Column('permission_id',
                              db.Integer,
                              db.ForeignKey('permission.id'),
                              nullable=False, index=True)

    __table_args__ = (db.UniqueConstraint(
        'group_id', 'permission_id',
        name='unique_constraint_group_permission'),  {'sqlite_autoincrement': True} )

    @validates('user', include_backrefs=False)
    def validate_user(self, key, user):
        if user.type != 'user':
            raise AssertionError('This user type should be user.')
        else:
            return user

    @validates('group', include_backrefs=False)
    def validate_group(self, key, user):
        if user.type != 'group':
            raise AssertionError('This user type should be group.')
        else:
            return user