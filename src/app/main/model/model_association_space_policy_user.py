from app.main import db

space_policy_user = db.Table(
    'association_space_policy_user',
    db.Column('policy_id', db.Integer, db.ForeignKey('space_policy.id'), index=True),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), index=True),
)