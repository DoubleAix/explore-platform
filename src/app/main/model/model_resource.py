from app.main import db
from sqlalchemy.orm import validates


class Resource(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(80), nullable=False)
    label = db.Column(db.String(80))
    # "root" or "folder" or "file"
    layer = db.Column(db.String(50), nullable=False,index=True)
    icon = db.Column(db.String(50))
    type = db.Column(
        db.String(80))  # "settings" or "embeddings" (for frontend display)
    reserved = db.Column(db.Boolean, nullable=False)
    authorization_type_id = db.Column(
        db.Integer,
        db.ForeignKey('authorization_type.id'), index=True)  ## for authrizaition
    authorization_type = db.relationship('AuthorizationType')
    data = db.Column(db.JSON)  # (for frontend display)

    parent_id = db.Column(db.Integer, db.ForeignKey('resource.id'), index=True)
    children = db.relationship("Resource",
                               cascade="all, delete-orphan",
                               lazy="dynamic",
                               backref=db.backref('parent', remote_side=[id]), foreign_keys=[parent_id])

    

    tags = db.relationship("Tag",
                           lazy="dynamic",
                           secondary='association_resource_tag',
                           backref=db.backref("resources", lazy="dynamic"))

    __table_args__ = (db.UniqueConstraint(
        'name', 'parent_id', name='unique_constraint_name_parent_id'), {
            'sqlite_autoincrement': True
        })

    @validates('parent', include_backrefs=False)
    def validate_parent(self, key, resource):
        if resource.layer != 'folder' and self.layer == 'file':
            raise AssertionError(
                "The parent of Type file should be Type folder.")
        else:
            return resource