from app.main import db
from sqlalchemy.orm import validates


class AssociationSpaceResource(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sequence_number = db.Column(db.Integer, nullable=False)
    space_id = db.Column('space_id', db.Integer, db.ForeignKey('space.id'), index=True)
    resource_id = db.Column('resource_id', db.Integer,
                            db.ForeignKey('resource.id'), index=True)

    space = db.relationship("Space",
                            backref=db.backref("space_associations",
                                               cascade="all, delete-orphan"))
    resource = db.relationship("Resource",
                               backref=db.backref(
                                   "resource_associations",
                                   cascade="all, delete-orphan"))

    demo_resources = db.relationship(
        "Resource", secondary="association_auto_demo_resource")

    __table_args__ = (
        db.UniqueConstraint('space_id',
                            'resource_id',
                            name='unique_constraint_space_resource'),
        {
            'sqlite_autoincrement': True
        })

    @validates('demo_resources')
    def validate_demo_resource(self, key, resource):
        if resource.layer != 'file':
            raise AssertionError('This resource is not a child resource.')
        elif resource.parent != self.resource:
            raise AssertionError(
                "This resource is not a child resource of the association's resource."
            )
        else:
            return resource