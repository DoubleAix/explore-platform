from app.main import db
# from app.main.model.model_association_resource_policy_user import resource_policy_user


class ResourcePolicy(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    user = db.relationship("User",
                           lazy="dynamic",
                           secondary='association_resource_policy_user',
                           backref=db.backref("resource_policies",
                                              lazy="dynamic"))
    resource = db.relationship('Resource',
                               backref=db.backref(
                                   "resource_policies",
                                   lazy="dynamic",
                                   cascade="all, delete-orphan"))
    permission = db.relationship('Permission',
                                 backref=db.backref("resource_policies",
                                                    lazy="dynamic"))
    resource_id = db.Column('resource_id',
                            db.Integer,
                            db.ForeignKey('resource.id'),
                            nullable=False,
                            index=True)
    permission_id = db.Column('permission_id',
                              db.Integer,
                              db.ForeignKey('permission.id'),
                              nullable=False, index=True)

    __table_args__ = (db.UniqueConstraint(
        'resource_id',
        'permission_id',
        name='unique_constraint_resource_permission'), {
            'sqlite_autoincrement': True
        })
