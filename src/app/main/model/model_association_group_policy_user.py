from app.main import db

group_policy_user = db.Table(
    'association_group_policy_user',
    db.Column('policy_id', db.Integer, db.ForeignKey('group_policy.id'), index=True),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), index=True),
)