from app.main import db

authorizable_permission = db.Table(
    'association_authorizable_permission',
    db.Column('authorizable_permission_parent_id', db.Integer, db.ForeignKey('association_authorization_type_permission.id'), index=True),
    db.Column('authorizable_permission_child_id', db.Integer, db.ForeignKey('association_authorization_type_permission.id'), index=True),
)