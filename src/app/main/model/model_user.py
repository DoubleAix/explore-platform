from app.main import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(20), unique=True,
                        nullable=False)  # AD account
    user_name = db.Column(db.String(50))
    email = db.Column(db.String(250))
    type = db.Column(db.String(20), nullable=False, index=True)  # group or user
    reserved = db.Column(db.Boolean, nullable=False)
    authorization_type_id = db.Column(
        db.Integer,
        db.ForeignKey('authorization_type.id'), index=True)  ## for authrizaition
    authorization_type = db.relationship('AuthorizationType')

    __table_args__ = ({'sqlite_autoincrement': True}, )
