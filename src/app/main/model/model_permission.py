from app.main import db


class Permission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)

    __table_args__ = ({'sqlite_autoincrement': True}, )
