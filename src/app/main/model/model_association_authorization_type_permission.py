from app.main import db
from sqlalchemy.orm import validates


class AssociationAuthorizationTypePermission(db.Model):
    ## this entity defines all permissions of the authorization type & the hierarchy of all permissions of the authorization type.
    id = db.Column(db.Integer, primary_key=True)
    authorization_type_id = db.Column('authorization_type_id', db.Integer,
                                      db.ForeignKey('authorization_type.id'), index=True)
    permission_id = db.Column('permission_id', db.Integer,
                              db.ForeignKey('permission.id'), index=True)
    label = db.Column('label', db.String(60))
    authorization_type = db.relationship("AuthorizationType",
                                         backref=db.backref(
                                             "authorization_type_associations",
                                             cascade="all, delete-orphan"))
    permission = db.relationship("Permission",
                                 backref=db.backref(
                                     "permission_associations",
                                     cascade="all, delete-orphan"))

    parent_id = db.Column(
        db.Integer,
        db.ForeignKey('association_authorization_type_permission.id'), index=True)
    parent = db.relationship("AssociationAuthorizationTypePermission",
                             remote_side=[id],
                             foreign_keys=[parent_id],
                             backref=db.backref('children',
                                                lazy="dynamic",
                                                cascade="all, delete-orphan"))

    child_authorizable_permission = db.relationship(
        'AssociationAuthorizationTypePermission',
        lazy="dynamic",
        secondary='association_authorizable_permission',
        primaryjoin=
        'AssociationAuthorizationTypePermission.id==association_authorizable_permission.c.authorizable_permission_parent_id',
        secondaryjoin=
        'AssociationAuthorizationTypePermission.id==association_authorizable_permission.c.authorizable_permission_child_id',
        backref=db.backref('parent_authorizatble_permission'))

    __table_args__ = (db.UniqueConstraint(
        'authorization_type_id',
        'permission_id',
        name='unique_constraint_authorization_type_permission'), {
            'sqlite_autoincrement': True
        })

    @validates('parent', include_backrefs=False)
    def validate_parent(self, key, authorization_type):
        if "_child" not in authorization_type.name:
            raise AssertionError(
                'The prefix of permission\'name should be "_child".')
        else:
            return authorization_type

    @validates('authorizable_parent', include_backrefs=False)
    def validate_authorizable_parent(self, key, authorization_type):
        if "authorization" not in authorization_type.name:
            raise AssertionError(
                'The permission\'name should contain sub-string "authorization"'
            )
        else:
            return authorization_type
