from app.main import db
from sqlalchemy.orm import validates


class SpacePolicy(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship("User",
                           secondary='association_space_policy_user',
                           backref=db.backref("space_policies",
                                              lazy="dynamic"))
    space = db.relationship('Space',
                            backref=db.backref("space_policies",
                                               lazy="dynamic",
                                               cascade="all, delete-orphan"))
    permission = db.relationship('Permission',
                                 backref=db.backref("space_policies",
                                                    lazy="dynamic"))
    space_id = db.Column('space_id',
                         db.Integer,
                         db.ForeignKey('space.id'),
                         nullable=False, index=True)
    permission_id = db.Column('permission_id',
                              db.Integer,
                              db.ForeignKey('permission.id'),
                              nullable=False, index=True)

    __table_args__ = (db.UniqueConstraint(
        'space_id', 'permission_id',
        name='unique_constraint_space_permission'), )