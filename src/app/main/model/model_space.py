from app.main import db
from sqlalchemy.orm import validates


class Space(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    space_id = db.Column(db.String(10), unique=True, nullable=False)
    name = db.Column(db.String(80), unique=True, nullable=False)
    reserved = db.Column(db.Boolean, nullable=False)
    data = db.Column(db.JSON)
    theme_class = db.Column(db.String(50), nullable=False)
    icon = db.Column(db.String(50))
    authorization_type_id = db.Column(
        db.Integer,
        db.ForeignKey('authorization_type.id'), index=True)  ## for authrizaition
    authorization_type = db.relationship('AuthorizationType')

    resources = db.relationship(
        "Resource",
        secondary="association_space_resource",
        order_by="association_space_resource.c.sequence_number",
        backref=db.backref('spaces', lazy="dynamic"))

    __table_args__ = ({'sqlite_autoincrement': True}, )

    @validates('resources', include_backrefs=False)
    def validate_parent_resource(self, key, resource):
        if resource.layer == 'folder':
            return resource
        else:
            raise AssertionError('This resource is not a parent resource.')