from app.main import db

resource_tag = db.Table(
    'association_resource_tag',
    db.Column('resource_id', db.Integer, db.ForeignKey('resource.id'), index=True),
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'), index=True),
)