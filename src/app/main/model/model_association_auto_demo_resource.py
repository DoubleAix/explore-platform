from app.main import db

auto_demo_resource = db.Table(
    'association_auto_demo_resource',
    db.Column('association_space_resource_id', db.Integer,
              db.ForeignKey('association_space_resource.id'), index=True),
    db.Column('resource_id', db.Integer, db.ForeignKey('resource.id'), index=True),
)