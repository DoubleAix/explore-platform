from flask_restplus import Namespace, fields
from app.main.dto.dto_child_resource import ChildResourceManagement
from app.main.dto.dto_resource_policy import ResourcePolicyManagement

from app.main.model.model_tag import Tag
from app.main.model.model_resource import Resource


class ParentResourceManagement:
    api = Namespace('parent_resource',
                    description='parent resource management related')

    class GetTagNameList(fields.Raw):
        def output(self, key, obj, **kwargs):
            tags = Tag.query.filter(Tag.resources.any(Resource.id == obj.id)).all()
            if tags:
                return [tag.name for tag in tags]
            else:
                None

    parent_resource_fields = api.model(
        'Parent Resource',
        {
            'id':
            fields.Integer,
            'parent_resource_name':
            fields.String(attribute='name'),
            'layer':
            fields.String,
            'icon_name':
            fields.String(attribute='icon'),
            'type':
            fields.String,
            'label':
            fields.String,
            'reserved':
            fields.Boolean,
            'authorization_type_id':
            fields.Integer,
            'children':
            fields.Nested(ChildResourceManagement.child_resource_fields),
            'resource_policies':
            fields.List(
                fields.Nested(
                    ResourcePolicyManagement.resource_policy_fields)),
            'tags': GetTagNameList,
        },
        mask=
        '{id,parent_resource_name,layer,icon_name,type,label,reserved,authorization_type_id,tags,resource_policies{id,permission_id}}'
    )
