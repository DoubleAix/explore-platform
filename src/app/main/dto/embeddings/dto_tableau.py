from flask_restplus import Namespace, fields

class TableauDto:
    api = Namespace('tableau',description='to get tableau dashboard')

    tableau_fields = api.model("Tableau", {
        'dashboard_title': fields.String,
        'tableau_dashboard_url': fields.String,
        'height': fields.Integer,
        'width': fields.Integer,
         })