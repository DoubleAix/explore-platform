from flask_restplus import Namespace, fields
from app.main.dto.dto_space_policy import SpacePolicyManagement


class SpaceManagement:
    api = Namespace('space', description='space management related')

    space_fields = api.model(
        'Space', {
            'id':
            fields.Integer,
            'space_id':
            fields.String,
            'space_name':
            fields.String(attribute='name'),
            'theme_class':
            fields.String,
            'icon_name':
            fields.String(attribute='icon'),
            'authorization_type_id':
            fields.Integer,
            'reserved':
            fields.Boolean,
            'space_policies':
            fields.List(
                fields.Nested(SpacePolicyManagement.space_policy_fields)),
        },
        mask=
        '{id,space_id,space_name,icon_name,theme_class,reserved,authorization_type_id,space_policies{id,permission_id}}'
    )
