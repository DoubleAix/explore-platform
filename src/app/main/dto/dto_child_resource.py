from flask_restplus import Namespace, fields
from app.main.dto.dto_resource_policy import ResourcePolicyManagement


class ChildResourceManagement:
    api = Namespace('child_resource',
                    description='child resource management related')

    tableau_data_fields = api.model(
        'Tableau Data', {
            'site_name': fields.String,
            'workbook_name': fields.String,
            'dashboard_name': fields.String,
            'width': fields.Integer,
            'height': fields.Integer,
        })

    kibana_data_fields = api.model('Kibana Data', {
        'url': fields.String,
    })

    resource_data_fileds = api.model(
        'Resource Data', {
            'tableau': fields.Nested(tableau_data_fields),
            'kibana': fields.Nested(kibana_data_fields),
        })

    child_resource_fields = api.model(
        'Child Resource', {
            'id':
            fields.Integer,
            'child_resource_name':
            fields.String(attribute='name'),
            'layer':
            fields.String,
            'icon_name':
            fields.String(attribute='icon'),
            'type':
            fields.String,
            'label':
            fields.String,
            'reserved':
            fields.Boolean,
            'authorization_type_id':
            fields.Integer,
            'parent_id':
            fields.Integer,
            'data':
            fields.Nested(resource_data_fileds, skip_none=True),
            'resource_policies':
            fields.List(
                fields.Nested(
                    ResourcePolicyManagement.resource_policy_fields)),
        },
        mask=
        '{id,child_resource_name,layer,icon_name,type,label,reserved,authorization_type_id,parent_id,data,resource_policies{id,permission_id}}'
    )
