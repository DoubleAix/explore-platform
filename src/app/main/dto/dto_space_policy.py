from flask_restplus import Namespace,fields
# from app.main.dto.dto_permission import  PermissionDto

class SpacePolicyManagement:
    api = Namespace('space_policy', description='space policy management related')
    
    space_policy_fields = api.model('Space Policy', {
        'id': fields.Integer,
        'space_id': fields.Integer,
        'permission_id': fields.Integer,
        # 'permission': fields.Nested(PermissionDto.permission_fields),
    })