from flask_restplus import Namespace, fields


class SpaceResourceManagement:
    api = Namespace('space_resource',
                    description='space resource management related')

    space_resource_fields = api.model(
        'Space Resource', {
            'id': fields.Integer,
            'resource_name': fields.String(attribute='name'),
            'icon_name': fields.String(attribute='icon'),
            'reserved': fields.Boolean,
        })

    space_resources_fields = api.model(
        'Space Resources', {
            'embeddable': fields.List(fields.Nested(space_resource_fields)),
            'embedded': fields.List(fields.Nested(space_resource_fields))
        }
    )
    
