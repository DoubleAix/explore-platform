from flask_restplus import Namespace,fields



class PermissionDto:
    api = Namespace('permission', description='permission related')

    permission_fields = api.model("Permission", {
        'id': fields.Integer,
        "name": fields.String,
    })