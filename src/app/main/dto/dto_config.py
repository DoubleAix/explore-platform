from flask_restplus import Namespace, fields
from app.main.model.model_resource_policy import ResourcePolicy
from app.main.model.model_permission import Permission
from app.main.utils.enums import PermissionOption


class ConfigDto:

    api = Namespace('config', description='WEB config related')

    ## menu list
    class GetLink(fields.Raw):
        def output(self, key, obj, **kwargs):
            if obj.type == "embeddings":
                return obj.type + "/" + obj.label + "/" + str(obj.id)
            elif obj.type == "settings":
                return obj.type + "/" + obj.label
            else:
                return False

    class GetAuthorizedTo(fields.Raw):
        def output(self, key, obj, **kwargs):
            rp = ResourcePolicy.query.filter(
                ResourcePolicy.resource_id == obj.id,
                ResourcePolicy.permission.has(
                    Permission.name == PermissionOption.READ.value)).first()
            if rp:
                return rp.id
            else:
                False

    sub_menu_fields = api.model(
        'Sub Meau', {
            'name': fields.String,
            'icon': fields.String,
            'link': GetLink,
            'resource_id': fields.Integer(attribute="id"),
            'authorized_to': GetAuthorizedTo,
        })

    menu_fields = api.model(
        'Menu', {
            'name':
            fields.String,
            'icon':
            fields.String,
            'authorized_to':
            GetAuthorizedTo,
            'link':
            GetLink,
            'resource_id':
            fields.Integer(attribute="id"),
            'sub':
            fields.List(fields.Nested(sub_menu_fields), attribute="children"),
        })

    ## authorization option list
    class GetPermissionName(fields.Raw):
        def output(self, key, obj, **kwargs):
            return obj.permission.name

    authorization_option_fields = api.model('Authorization Option', {
        'name': GetPermissionName,
        'label': fields.String,
    })

    settings_resource_policy_fields = api.model(
        "Settings Resource Policy", {
            "resource_policies_id": fields.Integer(attribute="id"),
            "permission_id": fields.Integer(),
        })

    settings_resource_policy_mapping = api.model(
        'Settings Resource Policy Mapping', {
            'name':
            fields.String(attribute="label"),
            'resource_policies':
            fields.List(fields.Nested(settings_resource_policy_fields)),
        })
