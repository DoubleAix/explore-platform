from flask_restplus import Namespace, fields


class UserManagement:
    api = Namespace('user', description='user management related')

    user_fields = api.model('User', {
        'id': fields.Integer,
        'user_id': fields.String,
        'user_name': fields.String,
        'email': fields.String,
        'reserved': fields.Boolean,
    })
