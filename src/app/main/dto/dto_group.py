from flask_restplus import Namespace, fields
from app.main.dto.dto_group_policy import GroupPolicyManagement


class GroupManagement:
    api = Namespace('group', description='group management related')

    ## authorization option list
    class GetPermissionName(fields.Raw):
        def output(self, key, obj, **kwargs):
            return obj.permission.name

    authorization_option_fields = api.model('Authorization Option', {
        'name': GetPermissionName,
        'label': fields.String,
    })
    member_fields = api.model(
        'Member', {
            'id': fields.Integer,
            'user_id': fields.String,
            'user_name': fields.String,
            'email': fields.String,
            'reserved': fields.Boolean,
        })

    group_fields = api.model(
        'Group', {
            'id':
            fields.Integer,
            'group_id':
            fields.String(attribute='user_id'),
            'group_name':
            fields.String(attribute='user_name'),
            'reserved':
            fields.Boolean,
            'authorization_type_id':
            fields.Integer,
            'group_policies':
            fields.List(fields.Nested(
                GroupPolicyManagement.group_policy_fields, ),
                        attribute='group_policies_group_own')
        },
        mask=
        '{id,group_id,group_name,reserved,authorization_type_id,group_policies{id,permission_id}}'
    )
