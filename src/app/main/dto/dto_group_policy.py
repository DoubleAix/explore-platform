from flask_restplus import Namespace,fields
from app.main.dto.dto_permission import  PermissionDto

class GroupPolicyManagement:
    api = Namespace('group_policy', description='group policy management related')
    
    group_policy_fields = api.model('Group Policy', {
        'id': fields.Integer,
        'group_id': fields.Integer,
        'permission_id': fields.Integer,
        'permission': fields.Nested(PermissionDto.permission_fields),
    })