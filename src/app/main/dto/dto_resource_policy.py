from flask_restplus import Namespace,fields
from app.main.dto.dto_permission import  PermissionDto


class ResourcePolicyManagement:
    api = Namespace('resource_policy', description='resource policy management related')

    resource_policy_fields = api.model('Resource Policy', {
        'id': fields.Integer,
        'resource_id': fields.Integer,
        'permission_id': fields.Integer,      
    })