from flask_restplus import Namespace, fields


class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    request_body = api.model('Login Request Body', {
        'user_id': fields.String(required=True, description='user ID', example='root'),
        'password': fields.String(required=True, description='password', example='00000000')
    })


class ProtectedDto:
    api = Namespace('protected', description='proetected (for test)')
