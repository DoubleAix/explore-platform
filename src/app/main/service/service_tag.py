from app.main.model.model_tag import Tag
from app.main import db

def create_or_update_all_tag_by_name(data):
    tags = []
    for name in data:
        tag = Tag.query.filter(Tag.name == name).first()
        if tag:
            tags.append(tag)
        else:
            tags.append(Tag(name=name))
    return tags