from flask_restplus import marshal
from flask import current_app
from flask_jwt_extended import create_access_token, create_refresh_token, get_jwt_identity, get_jwt_claims
from flask_jwt_extended import decode_token
from ldap3 import Server, Connection, ALL

from app.main import jwt_manager
from app.main.model.model_user import User
from app.main.dto.dto_user import UserManagement
from app.main.utils.enums import UserType, UserPolicyType

from app.main.service.service_resource_policy import get_all_resource_policy_id, get_all_authorizable_resource_policy_id
from app.main.service.service_group_policy import get_all_authorizable_group_policy_id, get_all_group_policy_id
from app.main.service.service_space_policy import get_all_authorizable_space_policy_id, get_all_space_policy_id_by_user_id
_user_fields = UserManagement.user_fields


## this can be used for state management to decide which data should be into the access token.
@jwt_manager.user_claims_loader
def add_claims_to_access_token(user):
    return user.claims


@jwt_manager.user_identity_loader
def user_identity_lookup(user):
    return user.user_id


class Auth(object):
    class UserObject(object):
        def __init__(self, user_id, claims):
            self.user_id = user_id
            self.claims = claims

    @staticmethod
    def authetication(data):
        # return True
        ## tempararily
        if data.get('user_id') in ['root','U0'
                                   ] and data.get('password') == '00000000':
            return True
        else:
            try:
                ad_server_host = current_app.config['LDAP_HOST']
                server = Server(host=ad_server_host)
                user = data.get('user_id')
                password = data.get('password')
                search_base = 'dc={},dc={}'.format(*ad_server_host.split('.'))
                search_filter = '(&(objectClass=user)(sAMAccountName={}))'.format(
                    user)
                search_controls = [
                    "cn", "displayName", "givenName", "mail", "description"
                ]

                conn = Connection(server,
                                  user='{}@{}'.format(user, ad_server_host),
                                  password=password,
                                  auto_bind=True)
                conn.search(search_base,
                            search_filter,
                            attributes=search_controls)

                
                return conn.entries
            except:
                return False

    @staticmethod
    def validate_if_account_exists(data):
        try:
            ad_server_host = current_app.config['LDAP_HOST']
            server = Server(host=ad_server_host)
            user = current_app.config['IE_ACCOUNT']
            password = current_app.config['IE_PASSWORD']
            search_base = 'dc={},dc={}'.format(*ad_server_host.split('.'))
            search_filter = '(&(objectClass=user)(sAMAccountName={}))'.format(
                data['user_id'])
            search_controls = [
                "cn", "displayName", "givenName", "mail", "description"
            ]

            conn = Connection(server,
                              user='{}@{}'.format(user, ad_server_host),
                              password=password,
                              auto_bind=True)
            conn.search(search_base, search_filter, attributes=search_controls)
            if conn.entries:
                if 'displayName' in conn.entries[0]:
                    data['user_name'] = conn.entries[0]['displayName'].value
                if 'mail' in conn.entries[0]:
                    data['email'] = conn.entries[0]['mail'].value
                return data
            else:
                return False

        except:
            return False

    @classmethod
    def login(cls, data):

        user_id = data.get("user_id")

        claims = cls._create_claims(user_id)

        user = Auth.UserObject(
            user_id=user_id,
            claims=claims,
        )
        access_token = create_access_token(identity=user)
        refresh_token = create_refresh_token(identity=user)
        response = {
            'access_token': access_token,
            'refresh_token': refresh_token,
        }
        return response

    @classmethod
    def refresh(cls, data):
        user_id = get_jwt_identity()
        if data and 'user_policy_type' in data and 'access_token' in data and len(
                data['user_policy_type']) > 0:
            try:
                claims = decode_token(data['access_token'],
                                      None)['user_claims']
                tmp = {}
                for user_policy_type in data['user_policy_type']:
                    policies = cls._get_user_policies(user_id,
                                                      user_policy_type)
                    if policies:
                        tmp[user_policy_type] = policies
                for  k,v in tmp.items():
                    if 'authorizable_' not in k:
                        if 'authorizable_'+ k in tmp:
                            v.update(tmp['authorizable_'+ k])
                        else:
                            v.update(cls._get_user_policies(user_id, 'authorizable_'+ k))
                    claims[k] = list(v)
                    claims[k].sort()
                    
                    # if policies:
                    #     claims[user_policy_type] = policies
            except:
                claims = cls._create_claims(user_id)

        else:
            claims = cls._create_claims(user_id)

        user = Auth.UserObject(
            user_id=user_id,
            claims=claims,
        )

        response = {
            'access_token': create_access_token(identity=user),
        }
        return response

    @classmethod
    def _create_claims(cls, user_id):
        claims = {}
        user_obj = User.query.filter(User.type == UserType.USER.value,
                                     User.user_id == user_id).first()
        claims['user_info'] = marshal(user_obj, _user_fields)
        tmp = {}
        for user_policy_type in [t.value for t in UserPolicyType]:
            policies = cls._get_user_policies(user_id, user_policy_type)
            if policies:
                tmp[user_policy_type] = policies
            else:
                tmp[user_policy_type] = {}

        for  k,v in tmp.items():
            if 'authorizable_' not in k:
                if 'authorizable_'+ k in tmp:
                    v.update(tmp['authorizable_'+ k])
                else:
                    v.update(cls._get_user_policies(user_id, 'authorizable_'+ k))
            claims[k] = list(v)
            claims[k].sort()
                    

            # if policies:
            #     claims[user_policy_type] = policies
            # else:
            #     claims[user_policy_type] = []
        return claims

    @staticmethod
    def _get_user_policies(user_id, user_policy_type):
        if user_policy_type == UserPolicyType.RESOURCE_POLICY.value:
            return get_all_resource_policy_id(user_id)

        elif user_policy_type == UserPolicyType.GROUP_POLICY.value:
            return get_all_group_policy_id(user_id)

        elif user_policy_type == UserPolicyType.SPACE_POLICY.value:
            return get_all_space_policy_id_by_user_id(user_id)

        elif user_policy_type == UserPolicyType.AUTHORIZABLE_RESOURCE_POLICY.value:
            return get_all_authorizable_resource_policy_id(user_id)

        elif user_policy_type == UserPolicyType.AUTHORIZABLE_GROUP_POLICY.value:
            return get_all_authorizable_group_policy_id(user_id)

        elif user_policy_type == UserPolicyType.AUTHORIZABLE_SPACE_POLICY.value:
            return get_all_authorizable_space_policy_id(user_id)

        else:
            return None
