from app.main import create_app, db
from app.main.model.model_space import Space
from app.main.model.model_resource import Resource
from app.main.model.model_association_space_resource import AssociationSpaceResource

## space resource related
def get_all_resource_by_id(id):
    space = Space.query.filter(Space.id == id).first()
    if space:
        resources = Resource.query.filter(
            Resource.layer == 'folder',
            ~Resource.id.in_([r.id for r in space.resources])).all()
        response = {'embeddable': resources, 'embedded': space.resources}
        return response


def get_all_resource_by_space_id(space_id):
    space = Space.query.filter(Space.space_id == space_id).first()
    if space:
        return space.resources


def update_all_resource(data, id):

    space = Space.query.filter(Space.id == id).first()
    new = [r['id'] for r in data]

    if space.reserved:
        old = []
        for resource in space.resources:
            if resource.reserved:
                new.insert(0, resource.id)
            else:
                old.append(resource.id)
    else:
        old = [resource.id for resource in space.resources]

    new_set = set(new)
    delete = [r for r in old if r not in new_set]

    asps_delete = AssociationSpaceResource.query.filter(
        AssociationSpaceResource.space_id == id,
        AssociationSpaceResource.resource_id.in_(delete)).all()
    for asp in asps_delete:
        db.session.delete(asp)

    for sequence_number, resource_id in enumerate(new):
        asp = AssociationSpaceResource.query.filter(
            AssociationSpaceResource.space_id == id,
            AssociationSpaceResource.resource_id == resource_id,
        ).first()
        if asp:
            asp.sequence_number = sequence_number
            db.session.merge(asp)
        else:
            asp = AssociationSpaceResource(space_id=id,
                                           resource_id=resource_id,
                                           sequence_number=sequence_number)
            db.session.add(asp)

    db.session.commit()

    return get_all_resource_by_id(id)
