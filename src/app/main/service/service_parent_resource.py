from app.main.model.model_permission import Permission
from app.main.model.model_resource_policy import ResourcePolicy
from app.main.model.model_resource import Resource
from app.main.model.model_user import User
from app.main.model.model_authorization_type import AuthorizationType
from app.main import db
from app.main.utils.enums import ResourceLayer, AuthorizationTypeOption, UserType

from app.main.service.service_tag import create_or_update_all_tag_by_name
from app.main.service.service_authorization_type import get_all_aatp_name_by_authorization_type_name
from app.main.service.service_resource_policy import create_all_resource_policy_by_resource
from flask_jwt_extended import get_jwt_identity


def get_all_parent_resource():
    return Resource.query.filter(
        Resource.layer == ResourceLayer.FOLDER.value).all()


def get_parent_resource_by_id(id):
    resource = Resource.query.filter(
        Resource.layer == ResourceLayer.FOLDER.value,
        Resource.id == id).first()
    return resource


def get_parent_resource_by_name(name):
    resource = Resource.query.filter(
        Resource.layer == ResourceLayer.FOLDER.value,
        Resource.name == name).first()
    return resource


def create_parent_resource(data):
    parent_resource = Resource.query.filter(
        Resource.layer == ResourceLayer.FOLDER.value,
        Resource.name == data['parent_resource_name']).first()
    if not parent_resource:
        ## Some default parent resources have different authorization types.
        if 'authorization_type' in data:
            authorization_type = AuthorizationType.query.filter(
                AuthorizationType.name == data['authorization_type']).first()
        else:
            authorization_type = AuthorizationType.query.filter(
                AuthorizationType.name ==
                AuthorizationTypeOption.PARENT_RESOURCE.value).first()
        ## Some default parent resources have differnt parent ids.
        if 'parent_id' in data:
            parent_id = data['parent_id']
        else:
            parent_id = Resource.query.filter(
                Resource.label ==
                AuthorizationTypeOption.RESOURCE_MANAGEMENT.value).first().id

        ## Create or update tags.
        tags = []
        if 'tags' in data:
            tags = create_or_update_all_tag_by_name(data['tags'])

        new_parent_resource = Resource(
            name=data['parent_resource_name'],
            layer=ResourceLayer.FOLDER.value,
            label=data['label'] if 'label' in data else None,
            type=data['type'] if 'type' in data else None,
            icon=data['icon_name'],
            reserved=data['reserved'] if 'reserved' in data else False,
            authorization_type=authorization_type,
            data=data['data'] if 'data' in data else None,
            parent_id=parent_id,
            tags=tags,
        )

        ## Set related permission types into policies.
        policies = create_all_resource_policy_by_resource(new_parent_resource)

        db.session.add_all(policies)

        ## Set the user has all permissions to oeperate this resource.
        # user_id = get_jwt_identity()
        # user_obj = User.query.filter(User.type == UserType.USER.value,
        #                              User.user_id == user_id).first()
        # resourcepolicies = ResourcePolicy.query.filter(
        #     ResourcePolicy.resource == new_parent_resource).all()
        # for resourcepolicy in resourcepolicies:
        #     member_object_list = User.query.filter(
        #         User.id.in_([user_obj.id]),
        #         User.type == UserType.USER.value).all()
        #     resourcepolicy.user.extend(member_object_list)

        db.session.commit()

        return new_parent_resource


def update_parent_resource(data, id):
    resource = Resource.query.filter(
        Resource.layer == ResourceLayer.FOLDER.value, Resource.id == id,
        Resource.reserved == False).first()
    if resource:
        if 'tags' in data:
            tags = create_or_update_all_tag_by_name(data['tags'])
        resource.name = data['parent_resource_name']
        resource.tags = tags
        db.session.commit()
        return resource


def delete_parent_resource(id):
    parent_resource = Resource.query.filter(
        Resource.reserved == False,
        Resource.layer == ResourceLayer.FOLDER.value,
        Resource.id == id).first()
    if parent_resource:
        db.session.delete(parent_resource)
        db.session.commit()
        return parent_resource
