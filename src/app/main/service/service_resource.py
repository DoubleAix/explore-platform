from app.main.model.model_resource import Resource
from app.main.utils.enums import ResourceType

def get_all_resource_by_type(resource_type):
    return Resource.query.filter(Resource.type == resource_type).all()

def get_all_settings_resource():
    return get_all_resource_by_type(ResourceType.SETTINGS.value)