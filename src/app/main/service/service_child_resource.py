from app.main.model.model_permission import Permission
from app.main.model.model_resource_policy import ResourcePolicy
from app.main.model.model_resource import Resource
from app.main.model.model_user import User
from app.main.model.model_authorization_type import AuthorizationType
from app.main import db
from app.main.utils.enums import ResourceLayer, AuthorizationTypeOption
from app.main.service.service_authorization_type import get_all_aatp_name_by_authorization_type_name
from app.main.service.service_resource_policy import create_all_resource_policy_by_resource

def get_all_child_resource(args):
    parent_resource_id = args.get('parent_resource_id')

    return Resource.query.filter(
        Resource.layer == ResourceLayer.FILE.value,
        Resource.parent_id == int(parent_resource_id)).all()


def get_child_resource_by_id(id):
    resource = Resource.query.filter(Resource.layer == ResourceLayer.FILE.value,
                                     Resource.id == id).first()
    return resource


def create_child_resource(data, args={}):
    if 'parent_resource_id' in args:
        parent_resource_id = args.get('parent_resource_id')
    else:
        parent_resource_id = data['parent_resource_id']

    child_resource = Resource.query.filter(
        Resource.layer == ResourceLayer.FILE.value, Resource.name == data['child_resource_name'],
        Resource.parent_id == int(parent_resource_id)).first()
    
    if not child_resource:
        if 'authorization_type' in data:
            authorization_type = AuthorizationType.query.filter(
                AuthorizationType.name == data['authorization_type']).first()
        else:
            authorization_type = AuthorizationType.query.filter(
                AuthorizationType.name == AuthorizationTypeOption.CHILD_RESOURCE.value).first()

        new_child_resource = Resource(
            name=data['child_resource_name'],
            layer=ResourceLayer.FILE.value,
            label=data['label'] if 'label' in data else None,
            type=data['type'] if 'type' in data else None,
            icon=data['icon_name'],
            reserved=data['reserved'] if 'reserved' in data else False,
            authorization_type=authorization_type,
            data=data['data'] if 'data' in data else None,
            parent_id=parent_resource_id,
        )
        policies = create_all_resource_policy_by_resource(new_child_resource)

        db.session.add_all(policies)
        db.session.commit()

        return new_child_resource


def update_child_resource(data, id):
    resource = Resource.query.filter(Resource.layer == ResourceLayer.FILE.value,
                                     Resource.id == id,
                                     Resource.reserved == False).first()
    if resource:
        resource.label=data['label']
        resource.type=data['type']
        resource.icon=data['icon_name']
        resource.data=data['data']
        db.session.commit()
        return resource


def delete_child_resource(id):
    child_resource = Resource.query.filter(Resource.reserved == False,
                                           Resource.layer == ResourceLayer.FILE.value,
                                           Resource.id == id).first()
    if child_resource:
        db.session.delete(child_resource)
        db.session.commit()
        return child_resource
