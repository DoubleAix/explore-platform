from app.main.model.model_space import Space
from app.main.model.model_permission import Permission
from app.main.model.model_space_policy import SpacePolicy
from app.main.model.model_authorization_type import AuthorizationType
from app.main import db
from app.main.utils.enums import AuthorizationTypeOption
from app.main.service.service_space_policy import create_all_space_policy_by_space

def get_all_space():
    return Space.query.all()


def get_space_by_id(id):
    space = Space.query.filter(Space.id == id).first()
    return space

def get_space_by_space_id(space_id):
    space = Space.query.filter(Space.space_id == space_id).first()
    return space

def get_all_space_containing_string(args):
    search_string = args.get('search_string')
    if search_string:
        return Space.query.filter(Space.name.contains(search_string)).all()
    else:
        return get_all_space()


def create_space(data):
    space = Space.query.filter(Space.name == data['space_name']).first()
    if not space:
        authorization_type = AuthorizationType.query.filter(
            AuthorizationType.name ==
            AuthorizationTypeOption.SPACE.value).first()
        
        new_space = Space(
            space_id= data['space_id'],
            icon=data['icon_name'],
            name=data['space_name'],
            authorization_type=authorization_type,
            theme_class=data['theme_class'] if 'theme_class' in data else False,
            reserved=data['reserved'] if 'reserved' in data else False,
        )

        policies = create_all_space_policy_by_space(new_space)
        db.session.add_all(policies)
        db.session.commit()

        return new_space


def update_space(data, id):
    space = Space.query.filter(Space.id == id, Space.reserved == False).first()
    if space:
        space.icon=data['icon_name']
        space.theme_class = data['theme_class']
        db.session.commit()
        return space


def delete_space(id):
    space = Space.query.filter(Space.id == id, Space.reserved == False).first()
    if space:
        db.session.delete(space)
        db.session.commit()
        return space



