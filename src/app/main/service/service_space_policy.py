from app.main.model.model_permission import Permission
from app.main.model.model_space import Space
from app.main.model.model_user import User
from app.main.model.model_resource import Resource
from app.main.model.model_authorization_type import AuthorizationType
from app.main.model.model_space_policy import SpacePolicy
from app.main.model.model_resource_policy import ResourcePolicy
from app.main.model.model_association_space_resource import AssociationSpaceResource
from app.main.model.model_association_authorization_type_permission import AssociationAuthorizationTypePermission

from app.main.service.service_user import get_all_member_by_user_id
from app.main.service.service_authorization_type import get_aatp_by_resource_policy, get_all_aatp_name_by_authorization_type_name
from app.main import db
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption


## get policy id
def get_space_policy_by_space_id_and_permission_name(space_id,
                                                     permission_name):
    space_policy = SpacePolicy.query.filter(
        SpacePolicy.space.has(Space.id == space_id),
        SpacePolicy.permission.has(
            Permission.name == permission_name)).first()
    return space_policy


## policy
def get_all_member(id, permission, user_type):
    space = Space.query.filter(Space.id == id).first()
    if space:
        permission = Permission.query.filter(
            Permission.name == permission).first()
        spacepolicy = SpacePolicy.query.filter(
            SpacePolicy.space == space,
            SpacePolicy.permission == permission).first()
        members = User.query.filter(User.space_policies.contains(spacepolicy),
                                    User.type == user_type).all()
        return members


def update_member(data, id, permission, user_type):
    space = Space.query.filter(Space.id == id).first()
    if space:
        permission = Permission.query.filter(
            Permission.name == permission).first()
        spacepolicy = SpacePolicy.query.filter(
            SpacePolicy.space == space,
            SpacePolicy.permission == permission).first()
        member_id_list = [m['id'] for m in data]
        member_object_list = User.query.filter(User.id.in_(member_id_list),
                                               User.type == user_type).all()
        spacepolicy.user.extend(member_object_list)
        db.session.commit()
        members = User.query.filter(User.space_policies.contains(spacepolicy),
                                    User.type == user_type).all()
        return members


def delete_member(id, permission, member_id, user_type):
    space = Space.query.filter(Space.id == id).first()
    permission = Permission.query.filter(Permission.name == permission).first()
    spacepolicy = SpacePolicy.query.filter(
        SpacePolicy.space == space,
        SpacePolicy.permission == permission).first()
    if space.reserved == True:
        member_object = User.query.filter(User.id == member_id,
                                          User.type == user_type,
                                          User.reserved == False).first()
    else:
        member_object = User.query.filter(
            User.id == member_id,
            User.type == user_type,
        ).first()
    if member_object in spacepolicy.user:
        spacepolicy.user.remove(member_object)
        db.session.commit()
        return member_object


## create space policies
def create_all_space_policy_by_space(space_obj):
    space_permission_options = get_all_aatp_name_by_authorization_type_name(
        AuthorizationTypeOption.SPACE.value)

    space_permission_obj = Permission.query.filter(
        Permission.name.in_(space_permission_options)).all()
    policies = [
        SpacePolicy(space=space_obj, permission_id=p.id)
        for p in space_permission_obj
    ]
    return policies


## get space policy id list
def get_all_space_policy_id_by_user_id(user_id):
    space_policies_set = set()
    member_id_list = get_all_member_by_user_id(user_id)

    resource_policies = ResourcePolicy.query.filter(
        ResourcePolicy.resource.has(
            Resource.authorization_type.has(
                AuthorizationType.name ==
                AuthorizationTypeOption.SPACE_MANAGEMENT.value)),
        ResourcePolicy.user.any(User.id.in_(member_id_list))).all()
    for resource_policy in resource_policies:

        aatps = get_aatp_by_resource_policy(resource_policy).children.all()
        for aatp in aatps:
            sp = SpacePolicy.query.filter(
                SpacePolicy.space.has(
                    Space.authorization_type_id == aatp.authorization_type_id),
                SpacePolicy.permission == aatp.permission).all()
            if sp:
                space_policies_set.update(sp)

    space_plicies_by_user = SpacePolicy.query.filter(
        SpacePolicy.user.any(User.id.in_(member_id_list))).all()

    space_policies_set.update(space_plicies_by_user)

    all_space_policy_id = {
        space_policy.id for space_policy in list(space_policies_set)
    }
    # all_space_policy_id.sort()
    # return all_space_policy_id
    return all_space_policy_id

## get authorizable space policy id list
def get_all_authorizable_space_policy_id(user_id):
    authorizable_space_policies_set = set()
    member_id_list = get_all_member_by_user_id(user_id)

    resource_policies = ResourcePolicy.query.filter(
        ResourcePolicy.user.any(User.id.in_(member_id_list)),
        ResourcePolicy.resource.has(
            Resource.authorization_type.has(
                AuthorizationType.name ==
                AuthorizationTypeOption.SPACE_MANAGEMENT.value)),
        ResourcePolicy.permission.has(
            Permission.name == PermissionOption.OWNER.value)).all()

    for resource_policy in resource_policies:
        aatps = get_aatp_by_resource_policy(
            resource_policy).child_authorizable_permission.all()
        for aatp in aatps:
            sp = SpacePolicy.query.filter(
                SpacePolicy.permission == aatp.permission).all()
            if sp:
                authorizable_space_policies_set.update(sp)

    space_policies = SpacePolicy.query.filter(
        SpacePolicy.user.any(User.id.in_(member_id_list)),
        SpacePolicy.permission.has(
            Permission.name == PermissionOption.OWNER.value +
            PermissionOption.UNDERSCORE.value +
            PermissionOption.READ.value)).all()

    for space_policy in space_policies:
        aatps = AssociationAuthorizationTypePermission.query.filter(
            AssociationAuthorizationTypePermission.authorization_type.has(
                AuthorizationType.id ==
                space_policy.space.authorization_type_id),
            AssociationAuthorizationTypePermission.permission.has(
                Permission.id == space_policy.permission.id)).first(
                ).child_authorizable_permission.all()
        for aatp in aatps:
            sp = SpacePolicy.query.filter(
                SpacePolicy.space.has(Space.id == space_policy.space.id),
                SpacePolicy.permission.has(
                    Permission.id == aatp.permission.id)).all()
            if sp:
                authorizable_space_policies_set.update(sp)
    all_authorizable_space_policy_id = {
        space_policy.id
        for space_policy in list(authorizable_space_policies_set)
    }
    # all_authorizable_space_policy_id.sort()
    return all_authorizable_space_policy_id
    # return authorizable_space_policies_set

    # authorizable_space_policies_set = set()
    # member_id_list = get_all_member_by_user_id(user_id)

    # resource_policies = ResourcePolicy.query.filter(
    #     ResourcePolicy.user.any(User.id.in_(member_id_list)),
    #     ResourcePolicy.resource.has(
    #         Resource.authorization_type.has(
    #             AuthorizationType.name ==
    #             AuthorizationTypeOption.SPACE_MANAGEMENT.value)),
    #     ResourcePolicy.permission.has(
    #         Permission.name.contains(
    #             PermissionOption.AUTHORIZATION_CHILD.value))).all()

    # for resource_policy in resource_policies:
    #     aatps = get_aatp_by_resource_policy(
    #         resource_policy).child_authorizable_permission.all()
    #     for aatp in aatps:
    #         sp = SpacePolicy.query.filter(
    #             SpacePolicy.permission == aatp.permission).all()
    #         if sp:
    #             authorizable_space_policies_set.update(sp)

    # space_policies = SpacePolicy.query.filter(
    #     SpacePolicy.user.any(User.id.in_(member_id_list)),
    #     SpacePolicy.permission.has(
    #         Permission.name == PermissionOption.AUTHORIZATION.value)).all()

    # for space_policy in space_policies:
    #     aatps = AssociationAuthorizationTypePermission.query.filter(
    #         AssociationAuthorizationTypePermission.authorization_type.has(
    #             AuthorizationType.id ==
    #             space_policy.space.authorization_type_id),
    #         AssociationAuthorizationTypePermission.permission.has(
    #             Permission.id == space_policy.permission.id)).first(
    #             ).child_authorizable_permission.all()
    #     for aatp in aatps:
    #         sp = SpacePolicy.query.filter(
    #             SpacePolicy.space.has(Space.id == space_policy.space.id),
    #             SpacePolicy.permission.has(
    #                 Permission.id == aatp.permission.id)).all()
    #         if sp:
    #             authorizable_space_policies_set.update(sp)
    # all_authorizable_space_policy_id = [
    #     space_policy.id
    #     for space_policy in list(authorizable_space_policies_set)
    # ]
    # all_authorizable_space_policy_id.sort()
    # return all_authorizable_space_policy_id