from app.main import db

from app.main.model.model_user import User
from app.main.model.model_permission import Permission
from app.main.model.model_group_policy import GroupPolicy
from app.main.utils.enums import UserType, PermissionOption
from itertools import chain
from sqlalchemy import or_, and_


def get_all_user():
    return User.query.filter(User.type == UserType.USER.value).all()


def get_user_by_id(id):
    user = User.query.filter(User.type == UserType.USER.value, User.id == id).first()
    return user

def create_user(data):
    user = User.query.filter(User.type == UserType.USER.value,
                             User.user_id == data['user_id']).first()
    if not user:
        new_user = User(
            user_id=data['user_id'],
            user_name=data['user_name'],
            email=data['email'],
            type=UserType.USER.value,
            reserved=data['reserved'] if 'reserved' in data else False,
        )
        db.session.add(new_user)
        db.session.commit()

        return new_user


def update_user(data, id):
    user = User.query.filter(User.type == UserType.USER.value, User.id == id, User.reserved==False).first()
    if user:
        user.user_name = data['user_name']
        user.email = data['email']
        db.session.commit()
        return user


def delete_user(id):
    user = User.query.filter(User.type == UserType.USER.value, User.id == id, User.reserved==False).first()
    if user:
        db.session.delete(user)
        db.session.commit()
        return user


def get_all_user_containing_string(args):
    search_string = args.get('search_string')
    if search_string:
        return User.query.filter(User.type == UserType.USER.value).filter(
            User.user_name.contains(search_string)
            | User.user_id.contains(search_string)).all()
    else:
        return get_all_user()

def get_all_member_by_user_id(user_id):
    member_list = User.query.with_entities(User.id).filter(
        or_(
            User.user_id == user_id,
            and_(
                User.type == UserType.GROUP.value,
                User.group_policies_group_own.any(
                    GroupPolicy.user.any(User.user_id == user_id)),
                User.group_policies_group_own.any(
                    GroupPolicy.permission.has(
                        Permission.name == PermissionOption.BELONG.value))))).all()
    member_id_list = list(chain(*member_list))
    return member_id_list
