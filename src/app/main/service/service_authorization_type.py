from flask import current_app

from app.main.model.model_authorization_type import AuthorizationType
from app.main.model.model_permission import Permission
from app.main.model.model_association_authorization_type_permission import AssociationAuthorizationTypePermission

from app.main import db

from app.main.config.default_data.authorization_type import authorization_type_list
from app.main.utils.enums import PermissionOption


def get_all_aatp_name_by_authorization_type_name(authorization_type_name):
    aatps = AssociationAuthorizationTypePermission.query.filter(
        AssociationAuthorizationTypePermission.authorization_type.has(
            AuthorizationType.name == authorization_type_name)).all()
    all_permission_name = [aatp.permission.name for aatp in aatps]
    return all_permission_name


def get_all_aatp_by_authorization_type_id(id):
    aatps = AssociationAuthorizationTypePermission.query.filter(
        AssociationAuthorizationTypePermission.authorization_type.has(
            AuthorizationType.id == id)).all()
    return aatps


def get_aatp_by_resource_policy(resource_policy):
    aatp = AssociationAuthorizationTypePermission.query.filter(
        AssociationAuthorizationTypePermission.authorization_type.has(
            AuthorizationType.id ==
            resource_policy.resource.authorization_type_id),
        AssociationAuthorizationTypePermission.permission.has(
            Permission.id == resource_policy.permission.id)).first()
    return aatp


def create_authorization_type(data):
    new_authorization_type = AuthorizationType(name=data['name'])
    associations = [
        AssociationAuthorizationTypePermission(
            authorization_type=new_authorization_type,
            permission=Permission.query.filter(
                Permission.name == p['name']).first(),
            label=p['label']) for p in data['permission_list']
    ]

    db.session.add_all(associations)
    db.session.commit()

    return new_authorization_type


def create_authorization_type_ownable_hierarchy(data):
    ## because some authorization type objects have not defined yet.
    if len(data['permission_list']) != 0:
        for p in data['permission_list']:

            parent_association = _get_association(data['name'], p['name'])

            if PermissionOption.OWNER.value not in p[
                    'name'] and PermissionOption.CHILD_SUFFIX.value in p[
                        'name']:
                current_app.logger.info('授權類別 {} 之權限 {} 可擁有權限之階層已經被定義'.format(
                    data['name'], p['name']))

                child_permission_name = p['name'].replace(
                    PermissionOption.CHILD_SUFFIX.value, '')

                for authroization_type_children in data[
                        "authroization_type_children"]:

                    child_association_obj = _get_association(
                        authroization_type_children, child_permission_name)

                    if child_association_obj:
                        parent_association.children.extend(
                            [child_association_obj])
                        db.session.commit()

            else:
                continue


def create_authorization_type_authorizable_hierarchy(data):
    ## because some authorization type objects have not defined yet.
    if len(data['permission_list']) != 0:
        for p in data['permission_list']:

            parent_association = _get_association(data['name'], p['name'])

            if PermissionOption.OWNER.value in p[
                    'name'] and PermissionOption.UNDERSCORE.value not in p[
                        'name']:

                current_app.logger.info('授權類別 {} 之權限 {} 可授權之權限階層已經被定義'.format(
                    data['name'], p['name']))

                ## child layer
                for authroization_type_children in data[
                        "authroization_type_children"]:

                    child_association_obj_list = _get_all_association(
                        authroization_type_children)

                    parent_association.child_authorizable_permission.extend(
                        child_association_obj_list)

                    db.session.commit()

                ## same layer (exclude itself)
                for permission in data['permission_list']:
                    ## exclude itself
                    if PermissionOption.OWNER.value not in permission[
                            'name'] or PermissionOption.UNDERSCORE.value in permission[
                                'name']:

                        child_association_obj = _get_association(
                            parent_association.authorization_type.name,
                            permission['name'])

                        if child_association_obj:
                            parent_association.child_authorizable_permission.extend(
                                [child_association_obj])
                            db.session.commit()

            elif PermissionOption.OWNER.value in p[
                    'name'] and PermissionOption.UNDERSCORE.value in p['name']:

                current_app.logger.info('授權類別 {} 之權限 {} 可授權之權限階層已經被定義'.format(
                    data['name'], p['name']))

                child_permission_name = p['name'].replace(
                    PermissionOption.OWNER.value +
                    PermissionOption.UNDERSCORE.value, '')

                for authroization_type_children in data[
                        "authroization_type_children"]:

                    child_association_obj = _get_association(
                        authroization_type_children, child_permission_name)

                    if child_association_obj:
                        parent_association.child_authorizable_permission.extend(
                            [child_association_obj])
                        db.session.commit()

                for permission in data['permission_list']:
                    ## exclude itself
                    if child_permission_name in permission[
                            'name'] and PermissionOption.OWNER.value not in permission[
                                'name']:

                        child_association_obj = _get_association(
                            parent_association.authorization_type.name,
                            permission['name'])

                        if child_association_obj:
                            parent_association.child_authorizable_permission.extend(
                                [child_association_obj])
                            db.session.commit()

            else:
                continue


def _get_association(authorization_type_name, permission_name):
    association = AssociationAuthorizationTypePermission.query.filter(
        AssociationAuthorizationTypePermission.authorization_type.has(
            AuthorizationType.name == authorization_type_name),
        AssociationAuthorizationTypePermission.permission.has(
            Permission.name == permission_name)).first()
    return association


def _get_all_association(authorization_type_name):
    all_association = AssociationAuthorizationTypePermission.query.filter(
        AssociationAuthorizationTypePermission.authorization_type.has(
            AuthorizationType.name == authorization_type_name)).all()
    return all_association
