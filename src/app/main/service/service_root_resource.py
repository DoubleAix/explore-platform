from app.main.model.model_resource import Resource
from app.main import db
from app.main.utils.enums import ResourceLayer

def get_all_root_resource():
    return Resource.query.filter(Resource.parent.has(layer=ResourceLayer.ROOT.value)).all()


def create_root_resource(data):
    root_resource = Resource.query.filter(
        Resource.layer == ResourceLayer.ROOT.value,
        Resource.name == data['root_resource_name']).first()
    if not root_resource:
        new_root_resource = Resource(
            name=data['root_resource_name'],
            layer=ResourceLayer.ROOT.value,
            reserved=True,
        )

        db.session.add(new_root_resource)
        db.session.commit()

        return new_root_resource