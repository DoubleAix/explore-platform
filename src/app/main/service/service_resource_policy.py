from app.main.model.model_permission import Permission
from app.main.model.model_resource_policy import ResourcePolicy
from app.main.model.model_authorization_type import AuthorizationType
from app.main.model.model_association_authorization_type_permission import AssociationAuthorizationTypePermission
from app.main.model.model_resource import Resource
from app.main.model.model_user import User
from app.main import db

from app.main.service.service_user import get_all_member_by_user_id
from app.main.service.service_authorization_type import get_aatp_by_resource_policy, get_all_aatp_name_by_authorization_type_name
from app.main.utils.enums import PermissionOption, AuthorizationTypeOption

from sqlalchemy import or_, and_


## get policy id
def get_resource_policy_id_by_label(label_name, permission_name):
    resource_policy = ResourcePolicy.query.filter(
        ResourcePolicy.resource.has(Resource.label == label_name),
        ResourcePolicy.permission.has(
            Permission.name == permission_name)).first()
    return resource_policy


def get_resource_policy_id_by_resource_id(resource_id, permission_name):
    resource_policy = ResourcePolicy.query.filter(
        ResourcePolicy.resource.has(Resource.id == resource_id),
        ResourcePolicy.permission.has(
            Permission.name == permission_name)).first()
    return resource_policy

def get_parent_resource_policy_id_by_resource_id(resource_id, permission_name):
    resource_policy = ResourcePolicy.query.filter(
        ResourcePolicy.resource.has(Resource.children.any(Resource.id == resource_id)),        
        ResourcePolicy.permission.has(
            Permission.name == permission_name)).first()
    return resource_policy

## policy
def get_all_member(id, permission, user_type):
    resource = Resource.query.filter(Resource.id == id).first()
    if resource:
        permission = Permission.query.filter(
            Permission.name == permission).first()
        resourcepolicy = ResourcePolicy.query.filter(
            ResourcePolicy.resource == resource,
            ResourcePolicy.permission == permission).first()
        members = User.query.filter(
            User.resource_policies.contains(resourcepolicy),
            User.type == user_type).all()
        return members


def update_member(data, id, permission, user_type):
    resource = Resource.query.filter(Resource.id == id).first()
    if resource:
        permission = Permission.query.filter(
            Permission.name == permission).first()
        resourcepolicy = ResourcePolicy.query.filter(
            ResourcePolicy.resource == resource,
            ResourcePolicy.permission == permission).first()
        member_id_list = [m['id'] for m in data]
        member_object_list = User.query.filter(User.id.in_(member_id_list),
                                               User.type == user_type).all()
        resourcepolicy.user.extend(member_object_list)
        db.session.commit()
        members = User.query.filter(
            User.resource_policies.contains(resourcepolicy),
            User.type == user_type).all()
        return members


def delete_member(id, permission, member_id, user_type):
    resource = Resource.query.filter(Resource.id == id).first()
    permission = Permission.query.filter(Permission.name == permission).first()
    resourcepolicy = ResourcePolicy.query.filter(
        ResourcePolicy.resource == resource,
        ResourcePolicy.permission == permission).first()
    if resource.reserved == True:
        member_object = User.query.filter(User.id == member_id,
                                          User.type == user_type,
                                          User.reserved == False).first()
    else:
        member_object = User.query.filter(
            User.id == member_id,
            User.type == user_type,
        ).first()
    if member_object in resourcepolicy.user:
        resourcepolicy.user.remove(member_object)
        db.session.commit()
        return member_object


## create resource policies
def create_all_resource_policy_by_resource(resource_obj):
    resource_permission_obj = Permission.query.filter(
        Permission.name.in_(
            get_all_aatp_name_by_authorization_type_name(
                resource_obj.authorization_type.name))).all()
    policies = [
        ResourcePolicy(resource=resource_obj, permission=p)
        for p in resource_permission_obj
    ]
    return policies


## get resource policy id list
def get_all_resource_policy_id(user_id):

    resource_policies_set = set()

    member_id_list = get_all_member_by_user_id(user_id)

    resource_policies = ResourcePolicy.query.filter(
        ResourcePolicy.user.any(User.id.in_(member_id_list))).all()

    for resource_policy in resource_policies:

        resource_policies_set.add(resource_policy)
        aatps = get_aatp_by_resource_policy(resource_policy).children.all()
        for aatp in aatps:
            rp = ResourcePolicy.query.filter(
                or_(
                    and_(
                        ResourcePolicy.resource.has(
                            Resource.parent_id == resource_policy.resource.id),
                        ResourcePolicy.permission_id == aatp.permission_id),
                )).all()
            ## because the resource authorization type has no "group" & "space" types.
            if rp:
                resource_policies_set.update(rp)

    all_resource_policy_id = {
        resource_policy.id for resource_policy in list(resource_policies_set)
    }
    # all_resource_policy_id.sort()
    # return all_resource_policy_id
    return all_resource_policy_id


## get all authorizable resource policy id
def get_all_authorizable_resource_policy_id(user_id):
    authorizable_resource_policies_set = set()

    member_id_list = get_all_member_by_user_id(user_id)

    resource_policies = ResourcePolicy.query.filter(
        ResourcePolicy.user.any(User.id.in_(member_id_list)),
        ResourcePolicy.permission.has(
            Permission.name.contains(
                PermissionOption.OWNER.value))).all()

    for resource_policy in resource_policies:

        aatps = get_aatp_by_resource_policy(
            resource_policy).child_authorizable_permission.all()

        for aatp in aatps:
            rp = ResourcePolicy.query.filter(
                or_(
                    and_(
                        resource_policy.resource.authorization_type_id ==
                        aatp.authorization_type_id,
                        ResourcePolicy.resource.has(
                            Resource.id == resource_policy.resource.id),
                        ResourcePolicy.permission == aatp.permission),
                    and_(
                        resource_policy.resource.authorization_type_id !=
                        aatp.authorization_type_id,
                        ResourcePolicy.resource.has(
                            # or_(
                            # Resource.parent == resource_policy.resource,
                            # Resource.parent.parent == resource_policy.resource,
                            # # Resource.parent.parent.parent == resource_policy.resource
                            # )
                            Resource.parent == resource_policy.resource
                            ),
                        ResourcePolicy.permission == aatp.permission))).all()
            if rp:
                # print(resource_policy)
                # print(rp)
                authorizable_resource_policies_set.update(rp)

    all_authorizable_resource_policy_id = {
        resource_policy.id
        for resource_policy in list(authorizable_resource_policies_set)
    }
    # all_authorizable_resource_policy_id.sort()

    return all_authorizable_resource_policy_id
    # return authorizable_resource_policies_set

# ## get resource policy id list
# def get_all_resource_policy_id(user_id):

#     resource_policies_set = set()

#     member_id_list = get_all_member_by_user_id(user_id)

#     resource_policies = ResourcePolicy.query.filter(
#         ResourcePolicy.user.any(User.id.in_(member_id_list))).all()

#     for resource_policy in resource_policies:

#         resource_policies_set.add(resource_policy)
#         aatps = get_aatp_by_resource_policy(resource_policy).children.all()
#         for aatp in aatps:
#             rp = ResourcePolicy.query.filter(
#                 or_(
#                     and_(
#                         ResourcePolicy.resource.has(
#                             Resource.parent_id == resource_policy.resource.id),
#                         ResourcePolicy.permission_id == aatp.permission_id),
#                 )).all()
#             ## because the resource authorization type has no "group" & "space" types.
#             if rp:
#                 resource_policies_set.update(rp)

#     all_resource_policy_id = [
#         resource_policy.id for resource_policy in list(resource_policies_set)
#     ]
#     all_resource_policy_id.sort()
#     return all_resource_policy_id


# ## get all authorizable resource policy id
# def get_all_authorizable_resource_policy_id(user_id):
#     authorizable_resource_policies_set = set()

#     member_id_list = get_all_member_by_user_id(user_id)

#     resource_policies = ResourcePolicy.query.filter(
#         ResourcePolicy.user.any(User.id.in_(member_id_list)),
#         ResourcePolicy.permission.has(
#             Permission.name.contains(
#                 PermissionOption.AUTHORIZATION.value))).all()

#     for resource_policy in resource_policies:

#         aatps = get_aatp_by_resource_policy(
#             resource_policy).child_authorizable_permission.all()

#         for aatp in aatps:
#             rp = ResourcePolicy.query.filter(
#                 or_(
#                     and_(
#                         resource_policy.permission.name ==
#                         PermissionOption.AUTHORIZATION.value,
#                         ResourcePolicy.resource.has(
#                             Resource.id == resource_policy.resource.id),
#                         ResourcePolicy.permission == aatp.permission),
#                     and_(
#                         resource_policy.permission.name ==
#                         PermissionOption.AUTHORIZATION_CHILD.value,
#                         ResourcePolicy.resource.has(
#                             Resource.parent == resource_policy.resource),
#                         ResourcePolicy.permission == aatp.permission))).all()
#             if rp:
#                 authorizable_resource_policies_set.update(rp)

#     a                            # or_(
                            # Resource.parent == resource_policy.resource,
                            # Resource.parent.parent == resource_policy.resource,
                            # # Resource.parent.parent.parent == resource_policy.resource
                            # )
#                                  # or_(
                            # Resource.parent == resource_policy.resource,
                            # Resource.parent.parent == resource_policy.resource,
                            # # Resource.parent.parent.parent == resource_policy.resource
                            # )
#                                  # or_(
                            # Resource.parent == resource_policy.resource,
                            # Resource.parent.parent == resource_policy.resource,
                            # # Resource.parent.parent.parent == resource_policy.resource
                            # )
#     ]                            # or_(
                            # Resource.parent == resource_policy.resource,
                            # Resource.parent.parent == resource_policy.resource,
                            # # Resource.parent.parent.parent == resource_policy.resource
                            # )
#     all_authorizable_resource_policy_id.sort()

#     return all_authorizable_resource_policy_id