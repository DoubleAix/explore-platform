import requests
from flask import current_app
from app.main.service.service_child_resource import get_child_resource_by_id


def get_dashboard_url(id):
    # tableau_username: Tableau account
    # tableau_location: Tableau server location (PROD or TEST)
    # tableau_site: Talbeau site
    # tableau_url: Tableau API url to get dashboard url
    # tableau_dashboard_code: [Workbook Name]/[Dashboard Name]

    resource = get_child_resource_by_id(id)

    tableau_data = resource.data['tableau']
    tableau_location = current_app.config['TABLEAU_LOCATION']
    tableau_username = current_app.config['TABLEAU_USERNAME']

    tableau_site = tableau_data['site_name']
    tableau_dashboard_code = "{}/{}".format(tableau_data['workbook_name'],
                                            tableau_data['dashboard_name'])
    tableau_url = "http://" + tableau_location + "/trusted/"

    r = requests.post(tableau_url,
                      data={
                          'username': tableau_username,
                          'target_site': tableau_site
                      })

    if r.status_code == 200:
        ticket = r.text
        if ticket != '-1':
            dashboard_url = "{}{}/t/{}/views/{}?:toolbar=top".format(
                tableau_url, ticket, tableau_site, tableau_dashboard_code)
            print(dashboard_url)
            response = {
                'dashboard_title':
                "{} - {}".format(resource.parent.name, resource.name),
                'tableau_dashboard_url':
                dashboard_url,
                'height':
                tableau_data['height'] if 'height' in tableau_data else None,
                'width':
                tableau_data['width'] if 'width' in tableau_data else None,
            }
            return response
