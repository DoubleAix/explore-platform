from app.main.model.model_permission import Permission
from app.main import db


def get_all_permission():
    return Permission.query.all()


def create_permission(data):
    permission = Permission.query.filter(
        Permission.name == data['name']).first()
    if not permission:
        new_permission = Permission(name=data['name'])
        db.session.add(new_permission)
        db.session.commit()

        return new_permission