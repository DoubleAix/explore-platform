from app.main import create_app, db
from app.main.model.model_user import User
from app.main.model.model_resource import Resource
from app.main.model.model_permission import Permission
from app.main.model.model_group_policy import GroupPolicy
from app.main.model.model_resource_policy import ResourcePolicy
from app.main.model.model_authorization_type import AuthorizationType
from app.main.model.model_association_authorization_type_permission import AssociationAuthorizationTypePermission

from app.main.service.service_user import get_all_member_by_user_id
from app.main.service.service_authorization_type import get_aatp_by_resource_policy, get_all_aatp_name_by_authorization_type_name
from app.main.utils.enums import PermissionOption, UserType, AuthorizationTypeOption

# from itertools import chain
from sqlalchemy import or_, and_


## get policy id
def get_group_policy_by_group_id_and_permission_name(group_id,
                                                     permission_name):
    group_policy = GroupPolicy.query.filter(
        GroupPolicy.group.has(User.id == group_id),
        GroupPolicy.permission.has(
            Permission.name == permission_name)).first()
    return group_policy


## policy
def get_all_member(id, permission):
    group = User.query.filter(User.type == UserType.GROUP.value,
                              User.id == id).first()
    permission = Permission.query.filter(Permission.name == permission).first()
    if group:
        grouppolicy = GroupPolicy.query.filter(
            GroupPolicy.group == group,
            GroupPolicy.permission == permission).first()
        return grouppolicy.user


def update_member(data, id, permission):
    group = User.query.filter(User.type == UserType.GROUP.value,
                              User.id == id).first()
    if group:
        permission = Permission.query.filter(
            Permission.name == permission).first()
        grouppolicy = GroupPolicy.query.filter(
            GroupPolicy.group == group,
            GroupPolicy.permission == permission).first()
        member_id_list = [m['id'] for m in data]
        member_object_list = User.query.filter(
            User.id.in_(member_id_list)).all()
        grouppolicy.user.extend(member_object_list)
        db.session.commit()
        return grouppolicy.user


def delete_member(id, permission, member_id):
    group = User.query.filter(User.type == UserType.GROUP.value,
                              User.id == id).first()
    permission = Permission.query.filter(Permission.name == permission).first()
    grouppolicy = GroupPolicy.query.filter(
        GroupPolicy.group == group,
        GroupPolicy.permission == permission).first()
    if group.reserved == True:
        member_object = User.query.filter(User.id == member_id,
                                          User.reserved == False).first()
    else:
        member_object = User.query.filter(User.id == member_id).first()
    if member_object in grouppolicy.user:
        grouppolicy.user.remove(member_object)
        db.session.commit()
        return member_object


## create group policies
def create_all_group_policy_by_group(group_obj):
    group_permission_options = get_all_aatp_name_by_authorization_type_name(
        AuthorizationTypeOption.GROUP.value)
    group_permission_options.append(PermissionOption.BELONG.value)

    group_permission_obj = Permission.query.filter(
        Permission.name.in_(group_permission_options)).all()
    policies = [
        GroupPolicy(group=group_obj, permission_id=p.id)
        for p in group_permission_obj
    ]
    return policies


## get group policy id list
def get_all_group_policy_id(user_id):
    group_policies_set = set()
    member_id_list = get_all_member_by_user_id(user_id)

    resource_policies = ResourcePolicy.query.filter(
        ResourcePolicy.resource.has(
            Resource.authorization_type.has(
                AuthorizationType.name ==
                AuthorizationTypeOption.GROUP_MANAGEMENT.value)),
        ResourcePolicy.user.any(User.id.in_(member_id_list))).all()
    for resource_policy in resource_policies:

        aatps = get_aatp_by_resource_policy(resource_policy).children.all()
        for aatp in aatps:
            gp = GroupPolicy.query.filter(
                GroupPolicy.group.has(
                    User.authorization_type_id == aatp.authorization_type_id),
                GroupPolicy.permission == aatp.permission).all()
            if gp:
                group_policies_set.update(gp)

    group_policies_by_user = GroupPolicy.query.filter(
        GroupPolicy.user.any(User.user_id == user_id)).all()
    group_policies_set.update(group_policies_by_user)

    all_group_policy_id = {
        group_policy.id for group_policy in list(group_policies_set)
    }
    # all_group_policy_id.sort()
    # return all_group_policy_id
    return all_group_policy_id

## get authorizable group policy id list
def get_all_authorizable_group_policy_id(user_id):
    authorizable_group_policies_set = set()
    member_id_list = get_all_member_by_user_id(user_id)

    resource_policies = ResourcePolicy.query.filter(
        ResourcePolicy.user.any(User.id.in_(member_id_list)),
        ResourcePolicy.resource.has(
            Resource.authorization_type.has(
                AuthorizationType.name ==
                AuthorizationTypeOption.GROUP_MANAGEMENT.value)),
        ResourcePolicy.permission.has(
            Permission.name.contains(PermissionOption.OWNER.value))).all()
    for resource_policy in resource_policies:
        aatps = get_aatp_by_resource_policy(
            resource_policy).child_authorizable_permission.all()
        for aatp in aatps:
            gp = GroupPolicy.query.filter(
                GroupPolicy.permission == aatp.permission).all()
            if gp:
                authorizable_group_policies_set.update(gp)

    group_policies = GroupPolicy.query.filter(
        GroupPolicy.user.any(User.user_id == user_id),
        GroupPolicy.permission.has(
            Permission.name == PermissionOption.OWNER.value +
            PermissionOption.UNDERSCORE.value +
            PermissionOption.CREATE.value)).all()

    for group_policy in group_policies:
        aatps = AssociationAuthorizationTypePermission.query.filter(
            AssociationAuthorizationTypePermission.authorization_type.has(
                AuthorizationType.id ==
                group_policy.group.authorization_type_id),
            AssociationAuthorizationTypePermission.permission.has(
                Permission.id == group_policy.permission.id)).first(
                ).child_authorizable_permission.all()
        for aatp in aatps:
            gp = GroupPolicy.query.filter(
                GroupPolicy.group.has(User.id == group_policy.group.id),
                GroupPolicy.permission.has(
                    Permission.id == aatp.permission.id)).all()
            if gp:
                authorizable_group_policies_set.update(gp)
    all_authorizable_group_policy_id = {
        group_policy.id
        for group_policy in list(authorizable_group_policies_set)
    }
    # all_authorizable_group_policy_id.sort()
    # return all_authorizable_group_policy_id
    return all_authorizable_group_policy_id


# authorizable_group_policies_set = set()
#     member_id_list = get_all_member_by_user_id(user_id)

#     resource_policies = ResourcePolicy.query.filter(
#         ResourcePolicy.user.any(User.id.in_(member_id_list)),
#         ResourcePolicy.resource.has(
#             Resource.authorization_type.has(
#                 AuthorizationType.name ==
#                 AuthorizationTypeOption.GROUP_MANAGEMENT.value)),
#         ResourcePolicy.permission.has(
#             Permission.name.contains(
#                 PermissionOption.AUTHORIZATION_CHILD.value))).all()
#     for resource_policy in resource_policies:
#         aatps = get_aatp_by_resource_policy(
#             resource_policy).child_authorizable_permission.all()
#         for aatp in aatps:
#             gp = GroupPolicy.query.filter(
#                 GroupPolicy.permission == aatp.permission).all()
#             if gp:
#                 authorizable_group_policies_set.update(gp)

#     group_policies = GroupPolicy.query.filter(
#         GroupPolicy.user.any(User.user_id == user_id),
#         GroupPolicy.permission.has(
#             Permission.name == PermissionOption.AUTHORIZATION.value)).all()

#     for group_policy in group_policies:
#         aatps = AssociationAuthorizationTypePermission.query.filter(
#             AssociationAuthorizationTypePermission.authorization_type.has(
#                 AuthorizationType.id ==
#                 group_policy.group.authorization_type_id),
#             AssociationAuthorizationTypePermission.permission.has(
#                 Permission.id == group_policy.permission.id)).first(
#                 ).child_authorizable_permission.all()
#         for aatp in aatps:
#             gp = GroupPolicy.query.filter(
#                 GroupPolicy.group.has(User.id == group_policy.group.id),
#                 GroupPolicy.permission.has(
#                     Permission.id == aatp.permission.id)).all()
#             if gp:
#                 authorizable_group_policies_set.update(gp)
#     all_authorizable_group_policy_id = [
#         group_policy.id
#         for group_policy in list(authorizable_group_policies_set)
#     ]
#     all_authorizable_group_policy_id.sort()
#     return all_authorizable_group_policy_id