from app.main.model.model_user import User
from app.main.model.model_permission import Permission
from app.main.model.model_group_policy import GroupPolicy
from app.main.model.model_authorization_type import AuthorizationType
from app.main import db
from app.main.utils.enums import UserType, AuthorizationTypeOption, PermissionOption
from app.main.service.service_group_policy import create_all_group_policy_by_group


def get_all_group():
    return User.query.filter(User.type == UserType.GROUP.value).all()


def get_group_by_id(id):
    group = User.query.filter(User.type == UserType.GROUP.value, User.id == id).first()
    return group


def get_all_group_containing_string(args):
    search_string = args.get('search_string')
    if search_string:
        return User.query.filter(User.type == UserType.GROUP.value).filter(
            User.user_name.contains(search_string)
            | User.user_id.contains(search_string)).all()
    else:
        return get_all_group()


def create_group(data):
    group = User.query.filter(User.type == UserType.GROUP.value,
                              User.user_id == data['group_id']).first()
    if not group:
        authorization_type = AuthorizationType.query.filter(
            AuthorizationType.name == AuthorizationTypeOption.GROUP.value).first()
        new_group = User(
            user_id=data['group_id'],
            user_name=data['group_name'],
            type=UserType.GROUP.value,
            authorization_type=authorization_type,
            reserved=data['reserved'] if 'reserved' in data else False,
        )

        policies = create_all_group_policy_by_group(new_group)
        db.session.add_all(policies)
        db.session.commit()

        return new_group


def update_group(data, id):
    group = User.query.filter(User.type == UserType.GROUP.value, User.id == id,
                              User.reserved == False).first()
    if group:
        group.user_name = data['group_name']
        db.session.commit()
        return group


def delete_group(id):
    group = User.query.filter(User.type == UserType.GROUP.value, User.id == id,
                              User.reserved == False).first()
    if group:
        db.session.delete(group)
        db.session.commit()
        return group
